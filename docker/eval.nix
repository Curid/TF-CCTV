let
  # Tensorflow 2.13.0
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/b44bcba394c3c8c11bf262921c92144f42506028.tar.gz"){};
  tf-slim = pkgs.python3Packages.callPackage ./nix/tf-slim.nix {};
  tf-models-official = pkgs.python3Packages.callPackage ./nix/tf-models-official.nix {};
  tf-keras = pkgs.python3Packages.callPackage ./nix/tf-keras.nix { };
  tensorflow-estimatorx = pkgs.python3Packages.callPackage ./nix/tensorflow-estimator.nix { };
  tensorflowx = pkgs.python3Packages.callPackage ./nix/tensorflow.nix {
    tensorflow-estimatorx=tensorflow-estimatorx;
  };
  libedgetpu = pkgs.callPackage ./nix/libedgetpu.nix { };
  tflite-runtime = pkgs.callPackage ./nix/tflite-runtime.nix {};
  tflite-runtime-py = pkgs.python3Packages.callPackage ./nix/tflite-runtime-py.nix {
    tflite-runtime = tflite-runtime;
  };
  tflite = pkgs.callPackage ./nix/tflite.nix {};
in pkgs.mkShell {
  nativeBuildInputs = [
    libedgetpu tflite pkgs.libusb1
  ];
  LD_LIBRARY_PATH = "${libedgetpu}/lib:${tflite}/lib";

  packages = with pkgs; [
    (python3.withPackages (python-pkgs: with python-pkgs; [
      tensorflowx
      tensorflow-estimatorx
      tf-slim
      tf-models-official
      pycocotools
      keras
      tf-keras
      gin-config
      tabulate
    ]))
    tflite-runtime-py
    pyright
    ruff-lsp
  ];

  shellHook = ''
    PYTHONPATH="$PYTHONPATH:$(pwd)"
    export PYTHONPATH
  '';
}
