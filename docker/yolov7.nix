let
  # TF 2.13.0
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/59e7944d2a7d49264525dd6d9a91a3d86b0f0526.tar.gz"){};
  tf-keras = pkgs.python3Packages.callPackage ./nix/tf-keras.nix { };
  onnx-graphsurgeon = pkgs.python3Packages.callPackage ./nix/onnx-graphsurgeon.nix { };
  sng4onnx = pkgs.python3Packages.callPackage ./nix/sng4onnx.nix { onnx-graphsurgeon=onnx-graphsurgeon; };
  onnx2tf = pkgs.python3Packages.callPackage ./nix/onnx2tf.nix {
    tf-keras=tf-keras;
    onnx-graphsurgeon=onnx-graphsurgeon;
    sng4onnx=sng4onnx;
  };
  onnxsim = pkgs.python3Packages.callPackage ./nix/onnxsim.nix { };
  onnxslim = pkgs.python3Packages.callPackage ./nix/onnxslim.nix { };
  tflite-support = pkgs.python3Packages.callPackage ./nix/tflite-support.nix { };
  kerasx = pkgs.python3Packages.callPackage ./nix/kerasx.nix { };
  edgetpu-compilerx = pkgs.callPackage ./nix/edgetpu-compilerx.nix {};
  ncnnx = pkgs.python3Packages.callPackage ./nix/ncnnx.nix {};
  tf-slim = pkgs.python3Packages.callPackage ./nix/tf-slim.nix {};
  netron = pkgs.python3Packages.callPackage ./nix/netron.nix {};
  onnxscript = pkgs.python3Packages.callPackage ./nix/onnxscript.nix {};
  #torch-xla = pkgs.python3Packages.callPackage ./nix/torch-xla.nix {};
  #ai-edge-torch = pkgs.python3Packages.callPackage ./nix/ai-edge-torch.nix { torch-xla=torch-xla };
  ultralytics = pkgs.python3Packages.callPackage ./nix/ultralytics.nix {};
in pkgs.mkShell {
  # BROKEN
  # TODO: minimize deps
  packages = with pkgs; [
    edgetpu-compilerx
    pyright
    ruff-lsp
    (python3.withPackages (python-pkgs: with python-pkgs; [
      ultralytics
      numpy
      onnx
      onnx-graphsurgeon
      tensorflow-bin
      tf-keras
      torch
      onnx2tf
      sng4onnx
      psutil
      opencv4
      matplotlib
      torchvision
      onnxruntime
      onnxslim
      tflite-support

      # yolov7.
      pandas
      onnxsim
      rich
      #kerasx
      #onnxscript
      #torch-xla
      #ai-edge-torch

      #ncnnx
      #tf-slim
      #pycocotools


      netron
    ]))
  ];
 
  shellHook = ''
    PYTHONPATH="$PYTHONPATH:$(pwd)/yolo/:$PYTHONPATH:$(pwd)/yolo/yolov7/:$(pwd)"
    export PYTHONPATH
  '';
}
