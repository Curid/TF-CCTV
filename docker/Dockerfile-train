# Last good container.
FROM nvcr.io/nvidia/tensorflow:22.02-tf1-py3 

ARG DEBIAN_FRONTEND=noninteractive

# Install apt dependencies
RUN apt-get update && \
	apt-get install -y git gpg-agent protobuf-compiler mesa-utils \
	wget rsync nano python3-cairocffi python3-pil python3-lxml \
	python3-tk python3-scipy python3-pandas && \
	rm -rf /var/lib/apt/lists/* 


RUN mkdir /app
WORKDIR /app


# Last commit compatible with pillow.
RUN cd /app && \
	wget https://github.com/tensorflow/models/archive/8095e92c6b2fae80e7378d07aa1d010c20ea83d6.zip -O models.zip && \
	unzip models.zip && \
	mv models-* models && \
	rm models.zip


# Compile protobuf configs
RUN (cd /app/models/research/ && protoc object_detection/protos/*.proto --python_out=.)
WORKDIR /app/models/research/

RUN cp object_detection/packages/tf1/setup.py ./
ENV PATH="/app/.local/bin:${PATH}"

RUN apt-get update && \
	apt-get install -y python3-dev python3-pip && \
	rm -rf /var/lib/apt/lists/* 

RUN python3 -m pip install -U pip
RUN python3 -m pip install .

RUN pip3 install numpy==1.17.4
RUN pip3 install Pillow==9.5

# Uninstall pip packages to make python start faster.
RUN pip3 uninstall -y notebook nbformat nbconvert nbclient ipykernel \
	jupytext jupyter-core jupyter-client jupyter-tensorboard jupyterlab \
	jupyterlab-pygments jupyterlab-server scikit-learn tensorrt \
	prometheus-client

ENV TF_CPP_MIN_LOG_LEVEL 3

ENV PYTHONPATH="/app/models/research/content"

WORKDIR /app/models/research/content
