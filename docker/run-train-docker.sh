#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")
home_dir="$script_dir/.."

docker run \
	-p 0.0.0.0:6006:6006 \
	-p 5151:5151 \
	-p 3000:3000 \
	-it -v "$home_dir/:/app/models/research/content" \
	codeberg.org/curid/tfcctv:train_v1 /bin/bash
