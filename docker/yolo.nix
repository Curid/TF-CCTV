let
  # nixos-24.05
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/b134951a4c9f3c995fd7be05f3243f8ecd65d798.tar.gz"){};
  tf-keras_2-18 = pkgs.python3Packages.callPackage ./nix/tf-keras_2-18.nix { };
  onnx-graphsurgeon = pkgs.python3Packages.callPackage ./nix/onnx-graphsurgeon.nix { };
  sng4onnx = pkgs.python3Packages.callPackage ./nix/sng4onnx.nix { onnx-graphsurgeon=onnx-graphsurgeon; };
  onnx2tf_1-22 = pkgs.python3Packages.callPackage ./nix/onnx2tf_1-22.nix {
    tf-keras=tf-keras_2-18;
    onnx-graphsurgeon=onnx-graphsurgeon;
    sng4onnx=sng4onnx;
  };
  onnxslim = pkgs.python3Packages.callPackage ./nix/onnxslim.nix { };
  tflite-support = pkgs.python3Packages.callPackage ./nix/tflite-support.nix { };
  edgetpu-compiler_16 = pkgs.callPackage ./nix/edgetpu-compiler_16.nix {};
  netron = pkgs.python3Packages.callPackage ./nix/netron.nix {};
  torch_2-0 = pkgs.python3Packages.callPackage ./nix/torch_2-0.nix {};
  torchvision_0-9 = pkgs.python3Packages.callPackage ./nix/torchvision_0-9.nix {};
  ultralytics = pkgs.python3Packages.callPackage ./nix/ultralytics.nix {};
  libedgetpu2 = pkgs.callPackage ./nix/libedgetpu2.nix { };
  python3 = pkgs.python3.override {
    packageOverrides = prev: final: {
      tf-keras=tf-keras_2-18;
    };
  };
in pkgs.mkShell {
  LD_LIBRARY_PATH = "${libedgetpu2}/lib";

  packages = with pkgs; [
    edgetpu-compiler_16
    pyright
    ruff-lsp
    meld
    xxd
    (python3.withPackages (python-pkgs: with python-pkgs; [
      ultralytics
      numpy
      torch
      opencv4
      matplotlib
      tqdm
      torchvision
      psutil
      tensorflow-bin
      onnx2tf_1-22
      tf-keras
      onnx
      onnx-graphsurgeon
      sng4onnx
      onnxslim
      onnxruntime
      tflite-support
      pandas
      seaborn

      netron
    ]))
  ];
 
  shellHook = ''
    PYTHONPATH="$PYTHONPATH:$(pwd)/yolo/:$PYTHONPATH:$(pwd)"
    export PYTHONPATH
  '';
}
