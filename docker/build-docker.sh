#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")

cd "$script_dir" || exit

docker image build -t codeberg.org/curid/tfcctv:v1 .
