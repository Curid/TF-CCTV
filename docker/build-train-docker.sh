#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")

cd "$script_dir" || exit

docker image build -t codeberg.org/curid/tfcctv:train_v1 -f Dockerfile-train .
