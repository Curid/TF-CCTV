#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")
home_dir="$script_dir/.."

docker run \
	-it \
    --ipc=host \
    --gpus all \
	-v "$home_dir/:/tfcctv" \
	-e PYTHONPATH=/tfcctv/:/tfcctv/yolo/ \
	ultralytics/ultralytics:8.3.55 /bin/bash
