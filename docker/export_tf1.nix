let
  # Tensorflow 1.15
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/a730888ce0c608789fbc57da7449c6a618b3e906.tar.gz"){};
  tf-slim1 = pkgs.python3Packages.callPackage ./nix/tf-slim1.nix {};
in pkgs.mkShell {
  buildInputs = with pkgs; [
    (python3.withPackages (python-pkgs: with python-pkgs; [
      tensorflow
      tf-slim1
    ]))
  ];

  shellHook = ''
    PYTHONPATH="$PYTHONPATH:$(pwd)"
    export PYTHONPATH
  '';
}
