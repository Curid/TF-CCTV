let
  # Tensorflow 2.13.0
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/b44bcba394c3c8c11bf262921c92144f42506028.tar.gz"){};
  edgetpu-compiler_16 = pkgs.callPackage ./nix/edgetpu-compiler_16.nix {};
in pkgs.mkShell {
  buildInputs = with pkgs; [
    (python3.withPackages (python-pkgs: with python-pkgs; [
      tensorflow
    ]))
    edgetpu-compiler_16
  ];

  shellHook = ''
    PYTHONPATH="$PYTHONPATH:$(pwd)"
    export PYTHONPATH
  '';
}
