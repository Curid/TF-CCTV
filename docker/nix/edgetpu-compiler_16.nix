{ autoPatchelfHook
, dpkg
, fetchurl
, lib
, libcxx
, stdenv
}:
stdenv.mkDerivation rec {
  pname = "edgetpu-compiler";
  version = "16.0";

  src = fetchurl rec {
    url = "https://packages.cloud.google.com/apt/pool/coral-edgetpu-stable/edgetpu-compiler_16.0_amd64_3ccd3b6ea6298eaaae6aa045764b3184.deb";
    sha256 = "sha256-F/8uRcdgBhJbUkAqB4ydTbEZAxa3Vs3jjqv+SaogIpw=";
  };

  nativeBuildInputs = [
    autoPatchelfHook
    dpkg
  ];

  buildInputs = [
    libcxx
  ];

  unpackPhase = ''
    mkdir bin pkg

    dpkg -x $src pkg

    rm -r pkg/usr/share/lintian

    cp pkg/usr/bin/edgetpu_compiler_bin/edgetpu_compiler ./bin
    cp -r pkg/usr/share .

    rm -r pkg
  '';

  dontConfigure = true;
  dontBuild = true;

  installPhase = ''
    runHook preInstall

    mkdir -p $out
    cp -r ./{bin,share} $out

    runHook postInstall
  '';

  meta = with lib; {
    description = "A command line tool that compiles a TensorFlow Lite model into an Edge TPU compatible file.";
    mainProgram = "edgetpu_compiler";
    homepage = "https://coral.ai/docs/edgetpu/compiler";
    sourceProvenance = with sourceTypes; [ binaryNativeCode ];
    license = licenses.asl20;
    maintainers = with maintainers; [ cpcloud ];
    platforms = [ "x86_64-linux" ];
  };
}
