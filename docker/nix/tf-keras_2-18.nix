{
  buildPythonPackage,
  fetchPypi,
  numpy,
  tensorflow-bin,
  #distutils,
}:

buildPythonPackage rec {
  pname = "tf-keras";
  version = "2.18.0";
  format = "wheel";

  src = fetchPypi rec {
    inherit version format;
    pname="tf_keras";
    dist=python;
    python="py3";
    sha256 = "sha256-xDHQQCfu95D80yYc9/35PrdPPLMuBQeLV7f1pUvVMmI=";
  };

  pythonImportsCheck = [ "tf_keras" ];

  nativeCheckInputs = [ numpy tensorflow-bin ];
}
