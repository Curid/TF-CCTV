{
  buildPythonPackage,
  fetchPypi,
  tensorflow,
  absl-py,
}:

buildPythonPackage rec {
  pname = "tf_slim";
  version = "1.1.0";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    python="py2.py3";
    sha256 = "sha256-+iurY7OSW9QmARAufxeNzpl/UldCWWv0BPqKaRjhRv8=";
  };

  nativeBuildInputs = [ absl-py tensorflow ];

  pythonImportsCheck = [ "tf_slim" ];
}
