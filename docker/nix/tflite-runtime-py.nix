{
  python3Packages,
  buildPythonPackage,
  tflite-runtime,
}:
buildPythonPackage rec {
  pname = "tflite-runtime";
  version = "v2.12.1";

  src = tflite-runtime;

  PROJECT_NAME = pname;
  PACKAGE_VERSION = version;

  buildInputs = [ python3Packages.numpy ];

  pythonImportsCheck = [ "tflite_runtime" ];
}
