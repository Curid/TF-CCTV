{
  buildPythonPackage,
  fetchFromGitHub,
  onnx,
  onnx-graphsurgeon,
}:

buildPythonPackage rec {
  pname = "sng4onnx";
  version = "1.0.4";
  format = "setuptools";

  src = fetchFromGitHub {
    owner = "PINTO0309";
    repo = "sng4onnx";
    rev = "refs/tags/${version}";
    hash = "sha256-H8n443gAbgP9z71CbRAhk2SKe3r6zjKQxSOQPNzfR70=";
  };

  pythonImportsCheck = [ "sng4onnx" ];

  nativeCheckInputs = [ onnx onnx-graphsurgeon ];
}
