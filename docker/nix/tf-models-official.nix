{
  buildPythonPackage,
  fetchPypi,
}:

buildPythonPackage rec {
  pname = "tf_models_official";
  version = "2.17.0";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    dist=python;
    python="py2.py3";
    sha256 = "sha256-uIuHEpMHoRU7sl6/F7ukfYlRBTyik9Tp9GayNKLxC60=";
  };

  pythonImportsCheck = [ "official" ];
}
