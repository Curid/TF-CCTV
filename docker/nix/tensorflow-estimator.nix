{
  lib,
  fetchPypi,
  buildPythonPackage,
}:

buildPythonPackage rec {
  pname = "tensorflow-estimator";
  version = "2.13.0";
  format = "wheel";

  src = fetchPypi {
    pname = "tensorflow_estimator";
    inherit version format;
    #hash = "sha256-mqz7YNGuh/rVBsVMM4tBI0FsppPR/bqiEHwqZMB9oF8="; # 1.15.2
    hash = "sha256-b4aChOqmVK46p8rNvvIXXQkJ35/PETdPUWb4v0dZUqo="; # 2.13.0
    #hash = "sha256-rt8h7sf7LckRUPyRoc4SvETbtyJ4oItY55/4fJ4o8VM="; # 2.15.2
  };
}
