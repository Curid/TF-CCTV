{
  buildPythonPackage,
  fetchPypi,
  onnx,
  sympy,
  packaging,
}:

buildPythonPackage rec {
  pname = "onnxslim";
  version = "0.1.34";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    dist=python;
    python="py3";
    sha256 = "sha256-dVyxPP16C0fTPlwZNdsrt6aZPpCYozmHwoeU+Rh8qxc=";
  };

  pythonImportsCheck = [ "onnxslim" ];

  nativeCheckInputs = [ onnx sympy packaging ];
}
