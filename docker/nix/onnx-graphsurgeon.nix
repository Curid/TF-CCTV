{
  buildPythonPackage,
  fetchPypi,
  numpy,
  onnx,
}:

buildPythonPackage rec {
  pname = "onnx-graphsurgeon";
  version = "0.5.2";
  format = "wheel";

  src = fetchPypi rec {
    inherit version format;
    pname="onnx_graphsurgeon";
    dist=python;
    python="py2.py3";
    sha256 = "sha256-EMEw1hKf3u4ClF+BA7WxEub9TZs1bi3T6A9T4Ovue1w=";
  };

  nativeCheckInputs = [ numpy onnx ];

  pythonImportsCheck = [ "onnx_graphsurgeon" ];
}
