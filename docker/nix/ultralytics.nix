{
  buildPythonPackage,
  fetchPypi,
  numpy,
  torch,
  opencv4,
  matplotlib,
  torchvision,
  psutil,
  tqdm,
}:

buildPythonPackage rec {
  pname = "ultralytics";
  version = "8.3.55";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    dist=python;
    python="py3";
    sha256 = "sha256-2jXVueQjC3Hyt2IsGzNrEI3q0Tj8b08hmXTwrwG03gU=";
  };

  pythonImportsCheck = [ "ultralytics" ];
  nativeCheckInputs = [ numpy torch opencv4 matplotlib torchvision psutil tqdm ];
}
