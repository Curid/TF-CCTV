{
  buildPythonPackage,
  fetchPypi,
}:

buildPythonPackage rec {
  pname = "netron";
  version = "7.9.7";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    dist=python;
    python="py3";
    sha256 = "sha256-31Zr8TRI/o0GCcgB8sBGZ5uhiJvVFYV62RChIYYKY2w=";
  };

  pythonImportsCheck = [ "netron" ];
}
