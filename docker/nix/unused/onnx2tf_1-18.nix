{
  buildPythonPackage,
  fetchPypi,
  numpy,
  tensorflow-bin,
  tf-keras,
  onnx,
  onnx-graphsurgeon,
  psutil,
  sng4onnx,
  keras,
}:

buildPythonPackage rec {
  pname = "onnx2tf";
  version = "1.18.15";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    dist=python;
    python="py3";
    sha256 = "sha256-/EINnYnQP/cbIda620W2sqItFkY8MlFOTAz35iKfXBE=";
  };

  pythonImportsCheck = [ "onnx2tf" ];

  nativeCheckInputs = [
    numpy
    tensorflow-bin
    tf-keras
    onnx
    onnx-graphsurgeon
    psutil
    sng4onnx
    keras
  ];
}
