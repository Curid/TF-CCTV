{
  lib,
  buildPythonPackage,
  fetchPypi,
  pytest,
  pytest-cov,
  pytest-xdist,
  six,
  numpy,
  scipy,
  pyyaml,
  h5py,
  optree,
  keras-applications,
  keras-preprocessing,
  rich,
}:

buildPythonPackage rec {
  pname = "keras";
  version = "2.13.1";
  format = "wheel";

  src = fetchPypi {
    inherit format pname version;
    hash = "sha256-XOX3Bvd5+nMw5jYy8ye3XOOBRKEgN2sq4ZF8APphNq8=";
    python = "py3";
    dist = "py3";
  };

  nativeCheckInputs = [
    pytest
    pytest-cov
    pytest-xdist
  ];

  propagatedBuildInputs = [
    six
    pyyaml
    numpy
    scipy
    h5py
    keras-applications
    keras-preprocessing
    optree
    rich
  ];

  # Couldn't get tests working
  doCheck = false;
}
