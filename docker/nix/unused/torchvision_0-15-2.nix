{ buildPythonPackage
, fetchFromGitHub
, lib
, libjpeg_turbo
, libpng
, ninja
, numpy
, pillow
, pytest
, scipy
, symlinkJoin
, torch
, which
}:

let
  inherit (torch) cudaCapabilities cudaPackages cudaSupport;
  inherit (cudaPackages) backendStdenv cudaVersion;

  pname = "torchvision";
  version = "0.15.2";
in
buildPythonPackage {
  inherit pname version;

  src = fetchFromGitHub {
    owner = "pytorch";
    repo = "vision";
    rev = "refs/tags/v${version}";
    hash = "sha256-KNbOgd6PCINZqZ24c/Ev+ODux3ik5iUlzem9uUfQArM=";
  };

  nativeBuildInputs = [ libpng ninja which ];

  buildInputs = [ libjpeg_turbo libpng ];

  propagatedBuildInputs = [ numpy pillow torch scipy ];

  preConfigure = ''
    export TORCHVISION_INCLUDE="${libjpeg_turbo.dev}/include/"
    export TORCHVISION_LIBRARY="${libjpeg_turbo}/lib/"
  '';

  # tries to download many datasets for tests
  doCheck = false;

  pythonImportsCheck = [ "torchvision" ];
  checkPhase = ''
    HOME=$TMPDIR py.test test --ignore=test/test_datasets_download.py
  '';

  nativeCheckInputs = [ pytest ];
}
