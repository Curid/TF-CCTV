{
  buildPythonPackage,
  fetchPypi,
  rich,
  numpy,
  onnx,
  onnxruntime
}:

buildPythonPackage rec {
  pname = "onnx-simplifier";
  version = "0.4.36";
  format = "wheel";

  src = fetchPypi rec {
    inherit version format;
    pname = "onnx_simplifier";
    dist=python;
    python="cp311";
    abi="cp311";
    platform="manylinux_2_17_x86_64.manylinux2014_x86_64";
    sha256 = "sha256-tdHB8XD9nAeqR11Tnp51Kuw1JQKuqtfVAE/ef/rSyjk=";
  };

  nativeCheckInputs = [ rich numpy onnx onnxruntime ];
  pythonImportsCheck = [ "onnxsim" ];
}
