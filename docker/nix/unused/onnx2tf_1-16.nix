{
  buildPythonPackage,
  fetchPypi,
  numpy,
  tensorflow-bin,
  keras,
  optree,
  rich,
  onnx,
  onnx-graphsurgeon,
  psutil,
  sng4onnx,
}:

buildPythonPackage rec {
  pname = "onnx2tf";
  version = "1.16.31";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    dist=python;
    python="py3";
    sha256 = "sha256-7/oHBQEBuR10oVEt4rS8OMnTgAPtcFlBzsTIKfTycK8=";
  };

  pythonImportsCheck = [ "onnx2tf" ];

  nativeCheckInputs = [
    numpy
    tensorflow-bin
    keras
    optree
    rich
    onnx
    onnx-graphsurgeon
    psutil
    sng4onnx
  ];
}
