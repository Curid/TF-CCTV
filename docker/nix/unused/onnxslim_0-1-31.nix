{
  buildPythonPackage,
  fetchPypi,
  onnx,
  sympy,
  packaging,
}:

buildPythonPackage rec {
  pname = "onnxslim";
  version = "0.1.31";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    dist=python;
    python="py3";
    sha256 = "sha256-QGI+muE9bOAWUZYA7z4i+EIo/30CHl4Gx+pjRzQK+CA=";
  };

  pythonImportsCheck = [ "onnxslim" ];

  nativeCheckInputs = [ onnx sympy packaging ];
}
