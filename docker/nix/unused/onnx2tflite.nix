{
  buildPythonPackage,
  fetchFromGitHub,
  setuptools,
  wheel,
  onnxconverter-common,
  onnx,
  onnxruntime,
  onnxsim,
  numpy,
}:

buildPythonPackage rec {
  pname = "onnx2tflite";
  version = "c79af";
  format = "pyproject";
  #build-system = [ setuptools ];
  nativeBuildInputs = [ setuptools ];
  #nativeBuildInputs = [ onnx onnxruntime onnxsim numpy ];

  src = ./temp;
  #src = fetchFromGitHub rec {
  #  owner = "MPolaris";
  #  repo = "onnx2tflite";
  #  rev = "c79af32236756e37366f1620bf01c46ba3d352f5";
  #  sha256 = "sha256-+schvXk0JYC13AYi3tTCmgqJeKCea0j0EF6/oSrx5OA=";
  #};

  #nativeCheckInputs = [ onnx onnxruntime onnxsim numpy ];
  #pythonImportsCheck = [ "onnx2tflite" ];
}
