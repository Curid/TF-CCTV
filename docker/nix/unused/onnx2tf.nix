{
  buildPythonPackage,
  fetchPypi,
  numpy,
  tensorflow-bin,
  tf-keras,
  onnx,
  onnx-graphsurgeon,
  psutil,
  sng4onnx,
}:

buildPythonPackage rec {
  pname = "onnx2tf";
  version = "1.22.3";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    dist=python;
    python="py3";
    sha256 = "sha256-h+2iKaGNNy6642WeojSuubXL1r1JZjfqatT/ztrnASs=";
  };

  pythonImportsCheck = [ "onnx2tf" ];

  nativeCheckInputs = [
    numpy
    tensorflow-bin
    tf-keras
    onnx
    onnx-graphsurgeon
    psutil
    sng4onnx
  ];
}
