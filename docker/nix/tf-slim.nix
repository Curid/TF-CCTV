{
  buildPythonPackage,
  fetchPypi,
  tensorflow-bin,
  absl-py,
}:

buildPythonPackage rec {
  pname = "tf_slim";
  version = "1.1.0";
  format = "wheel";

  src = fetchPypi rec {
    inherit pname version format;
    dist=python;
    python="py2.py3";
    sha256 = "sha256-+iurY7OSW9QmARAufxeNzpl/UldCWWv0BPqKaRjhRv8=";
  };

  pythonImportsCheck = [ "tf_slim" ];

  nativeCheckInputs = [ tensorflow-bin absl-py ];
}
