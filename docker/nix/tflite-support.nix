{
  pkgs,
  buildPythonPackage,
  fetchPypi,
  flatbuffers,
  libusb1,
  autoPatchelfHook,
  numpy,
  protobuf,
}:

buildPythonPackage rec {
  pname = "tflite-support";
  version = "0.4.4";
  format = "wheel";

  src = fetchPypi rec {
    inherit version format;
    pname="tflite_support";
    dist=python;
    python="cp311";
    abi="cp311";
    platform="manylinux2014_x86_64";
    sha256 = "sha256-62v/2nUyvQ4MlcaaSj8SduBDSJG4E5whtx20DHE8zJg=";
  };

  pythonImportsCheck = [ "tflite_support" ];

  checkInputs = [
    pkgs.libusb1
  ];

  nativeCheckInputs = [
    flatbuffers
    autoPatchelfHook
    numpy
    protobuf
  ];
}
