{
  buildPythonPackage,
  fetchPypi,
  numpy,
  tensorflow,
}:

buildPythonPackage rec {
  pname = "tf-keras";
  version = "2.17.0";
  format = "wheel";

  src = fetchPypi rec {
    inherit version format;
    pname="tf_keras";
    dist=python;
    python="py3";
    sha256 = "sha256-zJdxfk3AhIfzJ7B0CphAQ6ngEjx6TiEgZxFmnT7EHIg=";
  };

  pythonImportsCheck = [ "tf_keras" ];

  nativeCheckInputs = [ numpy tensorflow ];
}
