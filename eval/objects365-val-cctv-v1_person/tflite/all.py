from py_utils.eval import print_eval_results
import os
from pathlib import Path
import importlib.util
import time


print("\ntakes 30+ minutes to run\n")
time.sleep(10)

start = time.time()

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
results = []

for file in sorted(script_dir.iterdir()):
    if file.name == "all.py" or file.suffix != ".py":
        continue

    name = file.with_suffix("").name
    print(f"\nevaluating {name}\n")

    spec = importlib.util.spec_from_file_location(
        "module_name", os.path.join(script_dir, file)
    )
    script = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(script)
    result = script.eval()

    results.append((name, result))


print_eval_results(results)

print(f"\n{(round(time.time() - start)/60)} minutes")
