import os
from pathlib import Path
from od.eval import evaluate_tflite


def eval():
    script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
    home_dir = script_dir.parent.parent.parent
    os.chdir(home_dir)

    return evaluate_tflite(
        label_map_path="./labels/person.pbtxt",
        tflite_path="./yolo/models_official/yolov8m/yolov8m_320x320.tflite",
        records_path="./records/color_objects365-val-cctv-v1_person/record-?????-of-00001",
        num_classes=1,
        width=320,
        height=320,
        model_format="nolo_tflite",
    )


if __name__ == "__main__":
    eval()
