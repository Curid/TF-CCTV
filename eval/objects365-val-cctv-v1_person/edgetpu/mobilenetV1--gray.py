import os
from pathlib import Path
from od.eval import evaluate_tflite


def eval():
    script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
    home_dir = script_dir.parent.parent.parent
    os.chdir(home_dir)

    return evaluate_tflite(
        label_map_path="./labels/person.pbtxt",
        tflite_path="./pretrained_models/mobilenetV1/mobilenetV1_edgetpu.tflite",
        records_path="./records/gray_objects365-val-cctv-v1_person/record-?????-of-00001",
        num_classes=1,
        width=300,
        height=300,
        model_format="odapi_edgetpu",
    )


if __name__ == "__main__":
    eval()
