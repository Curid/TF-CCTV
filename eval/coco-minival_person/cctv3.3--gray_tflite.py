import os
from pathlib import Path
from od.eval import evaluate_tflite

if __name__ == "__main__":
    script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
    home_dir = script_dir.parent.parent
    os.chdir(home_dir)

    evaluate_tflite(
        label_map_path="./labels/person.pbtxt",
        tflite_path="./models/cctv3.3/gray_cctv3_340x340.tflite",
        records_path="./records/gray_coco-minival_person/record-?????-of-00001",
        num_classes=1,
        width=340,
        height=340,
    )
