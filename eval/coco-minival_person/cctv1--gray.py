import os
from pathlib import Path

from od.eval import evaluate
from od.config import (
    mobiledet_model,
    mobiledet_train_config,
    coco_eval_config,
    eval_input_config,
)

if __name__ == "__main__":
    script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
    home_dir = script_dir.parent.parent
    os.chdir(home_dir)

    configs = {
        "model": mobiledet_model(num_classes=1, width=420, height=280),
        "train_config": mobiledet_train_config(),
        "eval_config": coco_eval_config(),
        "eval_input_configs": [
            eval_input_config(
                "./labels/person.pbtxt",
                "./records/gray_coco-minival_person/record-?????-of-00001",
            ),
        ],
    }
    checkpoint_dir = str(home_dir / "models" / "cctv1")
    evaluate(configs, checkpoint_dir)
