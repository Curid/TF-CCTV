import io
import tempfile
from copy import deepcopy
from pathlib import Path
from typing import Any

import numpy as np
import torch
import torch.nn as nn
from ultralytics.data.utils import check_cls_dataset, check_det_dataset
from ultralytics.models.yolo.detect.train import DetectionTrainer
from ultralytics.models.yolo.detect.val import DetectionValidator
from ultralytics.nn.autobackend import (
    check_class_names,
    default_class_names,
)
from ultralytics.utils import (
    emojis,
)
from ultralytics.utils.checks import (
    check_imgsz,
)
from ultralytics.utils.metrics import DetMetrics
from ultralytics.utils.ops import Profile
from ultralytics.utils.torch_utils import (
    de_parallel,
    select_device,
    smart_inference_mode,
)

from yolo_utils.ultralytics import export_tflite


class TfliteValTrainer(DetectionTrainer):
    def validate(self):
        """
        Runs validation on test set using self.validator.

        The returned dict is expected to contain "fitness" key.
        """

        with tempfile.TemporaryDirectory() as temp_dir:
            temp_dir = Path(temp_dir)

            buffer = io.BytesIO()
            torch.save(
                {
                    "ema": deepcopy(self.ema.ema).half(),
                },
                buffer,
            )
            serialized_ckpt = buffer.getvalue()  # get the serialized content to save
            temp_model_path = temp_dir.joinpath("temp.pt")
            temp_model_path.write_bytes(serialized_ckpt)

            tflite_path = temp_dir.joinpath("temp.tflite")
            export_tflite(temp_model_path, self.args.imgsz, tflite_path)

            tflite_metrics = val_tflite(
                tflite_model=tflite_path, data=self.args.data, imgsz=self.args.imgsz
            )

        metrics = self.validator(self)

        res = tflite_metrics.mean_results()
        print(metrics)
        metrics["metrics/mAP50-95_pt(B)"] = metrics["metrics/mAP50-95(B)"]
        metrics["metrics/precision(B)"] = res[0]
        metrics["metrics/recall(B)"] = res[1]
        metrics["metrics/mAP50(B)"] = res[2]
        metrics["metrics/mAP50-95(B)"] = res[3]
        print(metrics)
        # {
        #   'metrics/precision(B)': 0.00193,
        #   'metrics/recall(B)': 0.07969,
        #   'metrics/mAP50(B)': 0.00277,
        #   'metrics/mAP50-95(B)': 0.00055,
        #   'fitness': 0.00078,
        #   'val/box_loss': 4.25799,
        #   'val/cls_loss': 4.0163,
        #   'val/dfl_loss': 4.15596
        # }

        fitness = metrics.pop(
            "fitness", -self.loss.detach().cpu().numpy()
        )  # use loss as fitness measure if not found
        if not self.best_fitness or self.best_fitness < fitness:
            self.best_fitness = fitness
        return metrics, fitness


def val_tflite(tflite_model: Path, data: str, imgsz: int) -> DetMetrics:
    return MyModel(str(tflite_model)).val(data=data, imgsz=imgsz)


class MyModel(nn.Module):
    def __init__(self, tflite_model) -> None:
        super().__init__()
        self.overrides = {}  # overrides for trainer object
        self.metrics = None  # validation/training metrics
        self.task = "detect"  # task type
        tflite_model = str(tflite_model).strip()

        self.model = tflite_model
        self.ckpt_path = tflite_model
        self.overrides["task"] = self.task
        self.model_name = tflite_model

    def val(self, **kwargs: Any):
        custom = {"rect": True}  # method defaults
        args = {
            **self.overrides,
            **custom,
            **kwargs,
            "mode": "val",
        }

        validator = MyValidator(args=args)
        validator(model=self.model)
        self.metrics = validator.metrics
        return validator.metrics


class MyValidator(DetectionValidator):
    @smart_inference_mode()
    def __call__(self, model=None):
        """Executes validation process, running inference on dataloader and computing performance metrics."""
        self.training = False
        augment = self.args.augment

        model = MyAutoBackend(
            weights=model or self.args.model,
            device=select_device(self.args.device, self.args.batch),
        )
        # self.model = model
        self.device = model.device  # update device
        self.args.half = model.fp16  # update half
        stride = model.stride
        check_imgsz(self.args.imgsz, stride=stride)
        self.args.batch = 1

        if str(self.args.data).split(".")[-1] in {"yaml", "yml"}:
            self.data = check_det_dataset(self.args.data)
        elif self.args.task == "classify":
            self.data = check_cls_dataset(self.args.data, split=self.args.split)
        else:
            raise FileNotFoundError(
                emojis(
                    f"Dataset '{self.args.data}' for task={self.args.task} not found ❌"
                )
            )

        if self.device.type in {"cpu", "mps"}:
            # faster CPU val as time dominated by inference, not dataloading
            self.args.workers = 0
        self.args.rect = False
        self.stride = model.stride  # used in get_dataloader() for padding
        self.dataloader = self.dataloader or self.get_dataloader(
            self.data.get(self.args.split), self.args.batch
        )

        model.eval()

        self.run_callbacks("on_val_start")
        dt = (
            Profile(device=self.device),
            Profile(device=self.device),
            Profile(device=self.device),
            Profile(device=self.device),
        )
        self.init_metrics(de_parallel(model))
        self.jdict = []  # empty before each val
        for batch_i, batch in enumerate(self.dataloader):
            self.run_callbacks("on_val_batch_start")
            self.batch_i = batch_i
            # Preprocess
            with dt[0]:
                batch = self.preprocess(batch)

            # Inference
            with dt[1]:
                preds = model(batch["img"], augment=augment)

            # Loss
            with dt[2]:
                if self.training:
                    self.loss += model.loss(batch, preds)[1]

            # Postprocess
            with dt[3]:
                preds = self.postprocess(preds)

            self.update_metrics(preds, batch)
            if self.args.plots and batch_i < 3:
                self.plot_val_samples(batch, batch_i)
                self.plot_predictions(batch, preds, batch_i)

        stats = self.get_stats()
        self.check_stats(stats)
        self.speed = dict(
            zip(
                self.speed.keys(),
                (x.t / len(self.dataloader.dataset) * 1e3 for x in dt),
            )
        )
        self.finalize_metrics()

        return stats


class MyAutoBackend(nn.Module):
    @torch.no_grad()
    def __init__(
        self,
        weights,
        device=torch.device("cpu"),
        data=None,
    ):
        super().__init__()
        w = str(weights[0] if isinstance(weights, list) else weights)

        self.fp16 = False
        self.stride = 32  # default stride

        # TFLite or TFLite Edge TPU
        # https://www.tensorflow.org/lite/guide/python#install_tensorflow_lite_for_python
        try:  # https://coral.ai/docs/edgetpu/tflite-python/#update-existing-tf-lite-code-for-the-edge-tpu
            from tflite_runtime.interpreter import Interpreter, load_delegate
        except ImportError:
            import tensorflow as tf

            Interpreter, load_delegate = (
                tf.lite.Interpreter,
                tf.lite.experimental.load_delegate,
            )

        interpreter = Interpreter(model_path=w)  # load TFLite model
        interpreter.allocate_tensors()  # allocate
        self.input_details = interpreter.get_input_details()  # inputs
        self.output_details = interpreter.get_output_details()  # outputs

        # Check names
        if "names" not in locals():  # names missing
            names = default_class_names(data)
        names = check_class_names(names)

        self.__dict__.update(locals())  # assign all variables to self

    def forward(self, im, augment=False, visualize=False, embed=None):
        """
        Runs inference on the YOLOv8 MultiBackend model.

        Args:
            im (torch.Tensor): The image tensor to perform inference on.
            augment (bool): whether to perform data augmentation during inference, defaults to False
            visualize (bool): whether to visualize the output predictions, defaults to False
            embed (list, optional): A list of feature vectors/embeddings to return.

        Returns:
            (tuple): Tuple containing the raw output tensor, and processed output for visualization (if visualize=True)
        """
        _, _, h, w = im.shape  # batch, channel, height, width
        im = im.permute(0, 2, 3, 1)  # torch BCHW to numpy BHWC shape(1,320,192,3)

        im = im.cpu().numpy()
        details = self.input_details[0]
        is_int = details["dtype"] in {
            np.int8,
            np.int16,
        }  # is TFLite quantized int8 or int16 model
        if is_int:
            scale, zero_point = details["quantization"]
            im = (im / scale + zero_point).astype(details["dtype"])  # de-scale
        self.interpreter.set_tensor(details["index"], im)
        self.interpreter.invoke()
        y = []
        for output in self.output_details:
            x = self.interpreter.get_tensor(output["index"])
            if is_int:
                scale, zero_point = output["quantization"]
                x = (x.astype(np.float32) - zero_point) * scale  # re-scale
            if (
                x.ndim == 3
            ):  # if task is not classification, excluding masks (ndim=4) as well
                # Denormalize xywh by image size. See https://github.com/ultralytics/ultralytics/pull/1695
                # xywh are normalized in TFLite/EdgeTPU to mitigate quantization error of integer models
                if x.shape[-1] == 6:  # end-to-end model
                    x[:, :, [0, 2]] *= w
                    x[:, :, [1, 3]] *= h
                else:
                    x[:, [0, 2]] *= w
                    x[:, [1, 3]] *= h
            y.append(x)
        # TF segment fixes: export is reversed vs ONNX export and protos are transposed
        if len(y) == 2:  # segment with (det, proto) output order reversed
            if len(y[1].shape) != 4:
                y = list(reversed(y))  # should be y = (1, 116, 8400), (1, 160, 160, 32)
            if y[1].shape[-1] == 6:  # end-to-end model
                y = [y[1]]
            else:
                y[1] = np.transpose(
                    y[1], (0, 3, 1, 2)
                )  # should be y = (1, 116, 8400), (1, 32, 160, 160)
        y = [x if isinstance(x, np.ndarray) else x.numpy() for x in y]

        if isinstance(y, (list, tuple)):
            if len(self.names) == 999 and (len(y) == 2):
                # segments and names not defined
                nc = (
                    y[0].shape[1] - y[1].shape[1] - 4
                )  # y = (1, 32, 160, 160), (1, 116, 8400)
                self.names = {i: f"class{i}" for i in range(nc)}
            return (
                self.from_numpy(y[0])
                if len(y) == 1
                else [self.from_numpy(x) for x in y]
            )
        else:
            return self.from_numpy(y)

    def from_numpy(self, x):
        return torch.tensor(x).to(self.device) if isinstance(x, np.ndarray) else x
