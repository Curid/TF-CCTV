import importlib
import logging
import os
import random
import re
import sys
import warnings
from typing import Dict, List, Optional
import hashlib

import numpy as np
import onnx
import onnx_graphsurgeon as gs
import tensorflow as tf
import tf_keras
from absl import logging as absl_logging
from onnx2tf.utils.common_functions import (
    download_test_image_data,
    dummy_onnx_inference,
    get_tf_model_inputs,
    get_tf_model_outputs,
)
from onnx2tf.utils.logging import Color, debug, error, info, set_log_level
from sng4onnx import generate as op_name_auto_generate

sys.setrecursionlimit(2147483647)  # C int maximum

warnings.simplefilter(action="ignore", category=FutureWarning)
warnings.simplefilter(action="ignore", category=Warning)
warnings.simplefilter(action="ignore", category=DeprecationWarning)
warnings.simplefilter(action="ignore", category=RuntimeWarning)


def hash(v):
    s = "%s" % v
    print(hashlib.md5(s.encode("utf-8")).hexdigest())
    exit(0)


def onnx2tf_convert(
    input_onnx_file_path: Optional[str] = "",
    output_folder_path: Optional[str] = "saved_model",
    output_integer_quantized_tflite: Optional[bool] = False,
    quant_type: Optional[str] = "per-channel",
    input_output_quant_dtype: Optional[str] = "int8",
    not_use_onnxsim: Optional[bool] = False,
    disable_group_convolution: Optional[bool] = False,
    enable_batchmatmul_unfold: Optional[bool] = False,
    verbosity: Optional[str] = "debug",
):
    random.seed(0)
    np.random.seed(0)
    os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
    os.environ["TF_USE_LEGACY_KERAS"] = "1"
    absl_logging.set_verbosity(absl_logging.ERROR)
    tf.random.set_seed(0)
    tf_keras.utils.set_random_seed(0)
    tf.config.experimental.enable_op_determinism()
    tf.get_logger().setLevel("INFO")
    tf.autograph.set_verbosity(0)
    tf.get_logger().setLevel(logging.FATAL)

    if verbosity is None:
        verbosity = "debug"
    set_log_level(verbosity)

    # If output_folder_path is empty, set the initial value
    if not output_folder_path:
        output_folder_path = "saved_model"

    # Escape
    input_onnx_file_path = rf"{input_onnx_file_path}"
    output_folder_path = rf"{output_folder_path}"

    # Input file existence check
    if not os.path.exists(input_onnx_file_path):
        error(
            "The specified *.onnx file does not exist. "
            + f"input_onnx_file_path: {input_onnx_file_path}"
        )
        sys.exit(1)

    # Extracting onnx filenames
    output_file_name = ""
    if input_onnx_file_path:
        output_file_name = os.path.splitext(os.path.basename(input_onnx_file_path))[0]
    else:
        output_file_name = "model"

    replacement_parameters = None

    # Automatic generation of each OP name - sng4onnx
    info("")
    info(Color.REVERSE("Automatic generation of each OP name started"), "=" * 40)
    op_name_auto_generate(
        input_onnx_file_path=f"{input_onnx_file_path}",
        output_onnx_file_path=f"{input_onnx_file_path}",
        non_verbose=True,
    )
    info(Color.GREEN("Automatic generation of each OP name complete!"))

    # quantization_type
    disable_per_channel = (
        False if quant_type is not None and quant_type == "per-channel" else True
    )

    # Loading Graphs
    onnx_graph = onnx.load(input_onnx_file_path)

    domain: str = onnx_graph.domain
    ir_version: int = onnx_graph.ir_version
    meta_data = {"domain": domain, "ir_version": ir_version}
    metadata_props = None
    if hasattr(onnx_graph, "metadata_props"):
        metadata_props = onnx_graph.metadata_props
    graph = gs.import_onnx(onnx_graph)

    # List Output
    # Cut the ONNX graph when an output name is specified that interrupts the conversion
    output_names = [graph_output.name for graph_output in graph.outputs]

    def sanitizing(node):
        if hasattr(node, "name"):
            node.name = node.name.replace(":", "__")
            if hasattr(node, "outputs"):
                for o in node.outputs:
                    if hasattr(o, "name"):
                        o.name = o.name.replace(":", "__")
                    elif hasattr(o, "_name"):
                        o._name = o._name.replace(":", "__")
            node.name = re.sub("^/", "wa/", node.name)
            if hasattr(node, "inputs"):
                for i in node.inputs:
                    if hasattr(i, "name"):
                        i.name = re.sub("^/", "wa/", i.name)
                    elif hasattr(i, "_name"):
                        i._name = re.sub("^/", "wa/", i._name)
            if hasattr(node, "outputs"):
                for o in node.outputs:
                    if hasattr(o, "name"):
                        o.name = re.sub("^/", "wa/", o.name)
                    elif hasattr(o, "_name"):
                        o._name = re.sub("^/", "wa/", o._name)
        elif hasattr(node, "_name"):
            node._name = node._name.replace(":", "__")
            if hasattr(node, "outputs"):
                for o in node.outputs:
                    if hasattr(o, "name"):
                        o.name = o.name.replace(":", "__")
                    elif hasattr(o, "_name"):
                        o._name = o._name.replace(":", "__")
            node._name = re.sub("^/", "wa/", node._name)
            if hasattr(node, "inputs"):
                for i in node.inputs:
                    if hasattr(i, "name"):
                        i.name = re.sub("^/", "wa/", i.name)
                    elif hasattr(i, "_name"):
                        i._name = re.sub("^/", "wa/", i._name)
            if hasattr(node, "outputs"):
                for o in node.outputs:
                    if hasattr(o, "name"):
                        o.name = re.sub("^/", "wa/", o.name)
                    elif hasattr(o, "_name"):
                        o._name = re.sub("^/", "wa/", o._name)

    # sanitizing ':', '/'
    _ = [sanitizing(graph_input) for graph_input in graph.inputs]
    _ = [sanitizing(graph_node) for graph_node in graph.nodes]
    _ = [sanitizing(graph_output) for graph_output in graph.outputs]

    new_output_names = []
    for output_name in output_names:
        output_name = output_name.replace(":", "__")
        output_name = re.sub("^/", "wa/", output_name)
        new_output_names.append(output_name)
    output_names = new_output_names

    onnx_graph = gs.export_onnx(graph=graph, do_type_check=False, **meta_data)
    if metadata_props is not None:
        onnx_graph.metadata_props.extend(metadata_props)

    # CUDA is used for dummy inference during accuracy checks, but accuracy is degraded.
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    info("")
    info(Color.REVERSE("Model loaded"), "=" * 72)

    # Create Output folder
    os.makedirs(output_folder_path, exist_ok=True)

    # debug counta
    op_counta = 1
    total_op_count = len(graph.inputs) + len(graph.nodes)

    # Define additional parameters
    additional_parameters = {
        "input_onnx_file_path": input_onnx_file_path,
        "onnx_graph": onnx_graph,
        "opset": graph.opset,
        "op_counta": op_counta,
        "total_op_count": total_op_count,
        "disable_group_convolution": disable_group_convolution,
        "enable_rnn_unroll": False,
        "disable_suppression_flextranspose": False,
        "number_of_dimensions_after_flextranspose_compression": 6,
        "disable_suppression_flexstridedslice": False,
        "disable_strict_mode": False,
        "number_of_dimensions_after_flexstridedslice_compression": 5,
        "optimization_for_gpu_delegate": False,
        "replace_argmax_to_reducemax_and_indices_is_int64": False,
        "replace_argmax_to_reducemax_and_indices_is_float32": False,
        "replace_argmax_to_fused_argmax_and_indices_is_int64": False,
        "replace_argmax_to_fused_argmax_and_indices_is_float32": False,
        "fused_argmax_scale_ratio": 0.5,
        "replace_to_pseudo_operators": [],
        "replacement_parameters": replacement_parameters,
        "mvn_epsilon": 0.0000000001,
        "output_signaturedefs": False,
        "output_nms_with_dynamic_tensor": False,
        "output_integer_quantized_tflite": True,
        "gelu_replace_op_names": {},
        "space_to_depth_replace_op_names": {},
        "relu_relu6_merge_op_names": {},
        "mul_div_replace_op_names": {},
        "use_cuda": False,
    }

    tf_layers_dict = {}

    info("")
    info(Color.REVERSE("Model conversion started"), "=" * 60)

    with graph.node_ids():
        # Inputs
        for graph_input in graph.inputs:
            """
            graph_input.shape: [1]
            graph_input.dtype: dtype('float32')
            graph_input.name: 'abs6_input'

            graph_input.shape: [1, 3, 192, 256]
            graph_input.dtype: dtype('float32')
            graph_input.name: 'input'

            graph_input.shape: [1, 3, 'height', 'width']
            graph_input.dtype: dtype('float32')
            graph_input.name: 'input'
            """
            # AUTO calib 4D check
            if graph_input.dtype != np.float32 or len(graph_input.shape) != 4:
                error(
                    "For INT8 quantization, the input data type must be Float32. "
                    + "all input OPs must assume 4D tensor image data. "
                    + f"INPUT Name: {graph_input.name} INPUT Shape: {graph_input.shape} INPUT dtype: {graph_input.dtype}"
                )
                sys.exit(1)

            # make input
            op = importlib.import_module("onnx2tf.ops.Input")

            # substitution because saved_model does not allow colons
            # Substitution because saved_model does not allow leading slashes in op names
            sanitizing(graph_input)

            op.make_node(
                graph_input=graph_input,
                tf_layers_dict=tf_layers_dict,
                keep_ncw_or_nchw_or_ncdhw_input_names=None,
                keep_nwc_or_nhwc_or_ndhwc_input_names=None,
                keep_shape_absolutely_input_names=None,
                **additional_parameters,
            )
            op_counta += 1
            additional_parameters["op_counta"] = op_counta

        # Get Inputs
        inputs = get_tf_model_inputs(
            tf_layers_dict=tf_layers_dict,
        )

        # download test data
        all_four_dim = sum(
            [
                1
                for input in inputs
                if len(input.shape) == 4
                and input.shape[0] is not None
                and input.shape[0] <= 20
                and input.shape[-1] == 3
                and input.shape[1] is not None
                and input.shape[2] is not None
            ]
        ) == len(inputs)
        same_batch_dim = False
        if all_four_dim:
            batch_size = inputs[0].shape[0]
            for input in inputs:
                same_batch_dim = batch_size == input.shape[0]
        test_data_nhwc = None
        if all_four_dim and same_batch_dim:
            test_data: np.ndarray = download_test_image_data()
            test_data_nhwc = test_data[: inputs[0].shape[0], ...]
            pass

        # ONNX dummy inference
        # Generate output for all OPs.
        # Used to verify the output error of each OP in the TensorFlow model.
        full_ops_output_names = []
        onnx_tensor_infos_for_validation = None
        for graph_node in graph.nodes:
            full_ops_output_names_sub = []
            for graph_node_output in graph_node.outputs:
                full_ops_output_names_sub.append(graph_node_output.name)
            full_ops_output_names.extend(full_ops_output_names_sub)
        # Models with errors during inference in onnxruntime skip dummy inference.
        onnx_outputs_for_validation: List[np.ndarray] = dummy_onnx_inference(
            onnx_graph=onnx_graph,
            output_names=full_ops_output_names,
            test_data_nhwc=test_data_nhwc,
            tf_layers_dict=tf_layers_dict,
        )
        """
        onnx_tensor_infos_for_validation:
            {
                onnx_output_name: np.ndarray,
                onnx_output_name: np.ndarray,
                onnx_output_name: np.ndarray,
                            :
            }
        """
        onnx_tensor_infos_for_validation = {
            ops_output_name: onnx_output_for_validation
            for ops_output_name, onnx_output_for_validation in zip(
                full_ops_output_names, onnx_outputs_for_validation
            )
        }
        del onnx_outputs_for_validation

        additional_parameters[
            "onnx_tensor_infos_for_validation"
        ] = onnx_tensor_infos_for_validation
        additional_parameters["test_data_nhwc"] = test_data_nhwc
        additional_parameters["custom_input_op_name_np_data_path"] = None

        # Addressing Einsum and OneHot shape_inference failure for onnx.
        # However, relief is provided only when the input tensor does not contain undefined dimensions.
        is_none_in_inputs = False
        for graph_input in graph.inputs:
            if graph_input.shape is not None:
                for s in graph_input.shape:
                    if isinstance(s, str):
                        is_none_in_inputs = True
                        break
            else:
                is_none_in_inputs = True
                break
        if onnx_tensor_infos_for_validation is not None and not is_none_in_inputs:
            correction_ops = [
                graph_node
                for graph_node in graph.nodes
                if graph_node.op in ["Einsum", "OneHot"]
            ]
            if len(correction_ops) > 0:
                for correction_op in correction_ops:
                    correction_op_output: gs.Variable = correction_op.outputs[0]
                    if correction_op_output.name in onnx_tensor_infos_for_validation:
                        onnx_output_shape = list(
                            onnx_tensor_infos_for_validation[
                                correction_op_output.name
                            ].shape
                        )
                        correction_op_output.shape = onnx_output_shape
                exported_onnx_graph = gs.export_onnx(
                    graph, do_type_check=False, **meta_data
                )
                if metadata_props is not None:
                    exported_onnx_graph.metadata_props.extend(metadata_props)
                estimated_graph = onnx.shape_inference.infer_shapes(exported_onnx_graph)
                if input_onnx_file_path is not None:
                    onnx.save(estimated_graph, input_onnx_file_path)
                    append_param = []
                    append_param = append_param
                    graph = gs.import_onnx(onnx.load(input_onnx_file_path))

                else:
                    graph = gs.import_onnx(estimated_graph)

        # Nodes
        # https://github.com/onnx/onnx/blob/main/docs/Operators.md
        for graph_node in graph.nodes:
            optype = graph_node.op
            op = importlib.import_module(f"onnx2tf.ops.{optype}")

            # substitution because saved_model does not allow colons
            # Substitution because saved_model does not allow leading slashes in op names
            sanitizing(graph_node)

            op.make_node(
                graph_node=graph_node,
                tf_layers_dict=tf_layers_dict,
                **additional_parameters,
            )
            op_counta += 1
            additional_parameters["op_counta"] = op_counta

        del additional_parameters["onnx_tensor_infos_for_validation"]
        del onnx_tensor_infos_for_validation

        # Get Outputs
        outputs = get_tf_model_outputs(
            tf_layers_dict=tf_layers_dict,
            output_names=output_names,
        )
        if not outputs:
            output_names = [
                output_name.replace(":", "__") for output_name in output_names
            ]
            output_names = [
                re.sub("^/", "", output_name) for output_name in output_names
            ]
            outputs = get_tf_model_outputs(
                tf_layers_dict=tf_layers_dict,
                output_names=output_names,
            )

        # Bring back output names from ONNX model
        for output, name in zip(outputs, output_names):
            if hasattr(output, "node"):
                output.node.layer._name = name.replace(":", "__")
                output.node.layer._name = re.sub("^/", "", output.node.layer._name)

        # Support for constant output
        # Bring constant layers unconnected to the model into the model.
        # It is assumed that the "-nuo" option is specified because
        # running onnxsim will remove constants from the ONNX file.
        # https://github.com/PINTO0309/onnx2tf/issues/627
        if isinstance(inputs, List) and len(inputs) > 0 and isinstance(outputs, List):
            x = inputs[0]

            for oidx, output_layer in enumerate(outputs):
                if isinstance(output_layer, tf.Tensor) and hasattr(
                    output_layer, "numpy"
                ):
                    y = output_layer.numpy()
                    outputs[oidx] = tf_keras.layers.Lambda(lambda _: tf.constant(y))(x)

        model = tf_keras.Model(inputs=inputs, outputs=outputs)
        debug("")

        # Create concrete func
        run_model = tf.function(lambda *inputs: model(inputs))
        concrete_func = run_model.get_concrete_function(
            *[tf.TensorSpec(tensor.shape, tensor.dtype) for tensor in model.inputs]
        )

        SIGNATURE_KEY = "serving_default"

        # saved_model
        # concrete_func
        info(Color.REVERSE("saved_model output started"), "=" * 58)
        tf.saved_model.save(model, output_folder_path)
        info(Color.GREEN("saved_model output complete!"))

        # TFLite
        converter = tf.lite.TFLiteConverter.from_concrete_functions([concrete_func])
        converter.target_spec.supported_ops = [
            tf.lite.OpsSet.TFLITE_BUILTINS,
            tf.lite.OpsSet.SELECT_TF_OPS,
        ]
        converter.unfold_batchmatmul = enable_batchmatmul_unfold

        # Quantized TFLite
        MEAN = np.asarray([[[[0.485, 0.456, 0.406]]]], dtype=np.float32)
        STD = np.asarray([[[[0.229, 0.224, 0.225]]]], dtype=np.float32)

        # Get signatures/input keys
        loaded_saved_model = tf.saved_model.load(output_folder_path).signatures[
            SIGNATURE_KEY
        ]
        input_keys = list(loaded_saved_model.structured_input_signature[1].keys())
        input_shapes = [
            v.shape for v in loaded_saved_model.structured_input_signature[1].values()
        ]
        input_dtypes = [
            v.dtype for v in loaded_saved_model.structured_input_signature[1].values()
        ]
        info(Color.BLUE("Input signature information for quantization"))
        info(Color.BLUE("signature_name") + f": {SIGNATURE_KEY}")
        for idx, (input_key, input_shape, input_dtype) in enumerate(
            zip(input_keys, input_shapes, input_dtypes)
        ):
            info(
                Color.BLUE(f"input_name.{idx}")
                + f": {input_key} "
                + Color.BLUE("shape")
                + f": {input_shape} "
                + Color.BLUE("dtype")
                + f": {input_dtype}"
            )

        # INT8 Converter
        converter = tf.lite.TFLiteConverter.from_saved_model(
            output_folder_path,
        )

        # Download sample calibration data - MS-COCO x20 images
        # Used only when there is only one input OP, a 4D tensor image,
        # and --quant_calib_input_op_name_np_data_path is not specified.
        # Otherwise, calibrate using the data specified in --quant_calib_input_op_name_np_data_path.
        calib_data_dict: Dict[str, List[np.ndarray, np.ndarray, np.ndarray]] = {}
        model_input_name_list = [model_input.name for model_input in model.inputs]
        data_count = 0
        if model.inputs[0].dtype == tf.float32 and len(model.inputs[0].shape) == 4:
            # AUTO calib 4D images
            calib_data: np.ndarray = download_test_image_data()
            data_count = calib_data.shape[0]
            for model_input in model.inputs:
                if (
                    model_input.dtype != tf.float32
                    or len(model_input.shape) != 4
                    or model_input.shape[-1] != 3
                ):
                    error(
                        "For models that have multiple input OPs and need to perform INT8 quantization calibration "
                        + "using non-rgb-image input tensors, specify the calibration data with "
                        + "--quant_calib_input_op_name_np_data_path. "
                        + "model_input[n].shape: {model_input.shape}"
                    )
                    sys.exit(1)

                calib_data_dict[model_input.name] = [
                    tf.image.resize(
                        calib_data.copy(),
                        (model_input.shape[1], model_input.shape[2]),
                    ),
                    MEAN,
                    STD,
                ]

        # representative_dataset_gen
        def representative_dataset_gen():
            for idx in range(data_count):
                yield_data_dict = {}
                for model_input_name in model_input_name_list:
                    calib_data, mean, std = calib_data_dict[model_input_name]
                    normalized_calib_data: np.ndarray = (calib_data[idx] - mean) / std
                    yield_data_dict[model_input_name] = tf.cast(
                        tf.convert_to_tensor(normalized_calib_data), tf.float32
                    )
                yield yield_data_dict

        # Full Integer Quantization
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
        converter.target_spec.supported_ops = [
            tf.lite.OpsSet.TFLITE_BUILTINS_INT8,
            tf.lite.OpsSet.SELECT_TF_OPS,
        ]
        converter._experimental_disable_per_channel = disable_per_channel
        converter.unfold_batchmatmul = enable_batchmatmul_unfold
        converter.representative_dataset = representative_dataset_gen
        inf_type = None
        if input_output_quant_dtype == "uint8":
            inf_type = tf.uint8
        else:
            inf_type = tf.int8
        converter.inference_input_type = inf_type
        converter.inference_output_type = inf_type
        tflite_model = converter.convert()
        with open(
            f"{output_folder_path}/{output_file_name}_full_integer_quant.tflite",
            "wb",
        ) as w:
            w.write(tflite_model)
        info(Color.GREEN("Full INT8 Quantization tflite output complete!"))
