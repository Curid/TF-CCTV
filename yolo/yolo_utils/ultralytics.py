import os
import random
import shutil
import subprocess
import tempfile
import warnings
from copy import deepcopy
from pathlib import Path

import numpy as np
import tensorflow as tf
import torch
from ultralytics import YOLO, settings
from ultralytics.cfg import TASK2DATA
from ultralytics.engine.exporter import Exporter
from ultralytics.nn.autobackend import check_class_names, default_class_names
from ultralytics.nn.modules import Detect, RTDETRDecoder
from ultralytics.nn.tasks import (
    DetectionModel,
    SegmentationModel,
    nn,
)
from ultralytics.utils import (
    DEFAULT_CFG,
    LOGGER,
    __version__,
    colorstr,
    yaml_save,
)
from ultralytics.utils.checks import (
    check_imgsz,
    check_version,
)
from ultralytics.utils.files import file_size
from ultralytics.utils.torch_utils import (
    get_latest_opset,
    smart_inference_mode,
)

from yolo_utils.onnx2tf_convert import onnx2tf_convert


def set_ultralytics_output_dir(output_dir: Path):
    # Define desired paths
    desired_paths = {
        "datasets_dir": str(output_dir / "dataset"),
        "runs_dir": str(output_dir / "runs"),
    }

    # Check current settings and collect needed changes
    settings_to_update = {}
    for name, desired_path in desired_paths.items():
        current_path = settings.get(name)
        if current_path != desired_path:
            settings_to_update[name] = (current_path, desired_path)

    # Apply any needed updates
    if settings_to_update:
        for name, (old_path, new_path) in settings_to_update.items():
            settings.update({name: new_path})
            print(f"\nUpdated {name}:")
            print(f"  Old: {old_path}")
            print(f"  New: {new_path}")
        print("\nSettings have been updated. Please restart the script.")
        exit(0)


def set_seeds():
    tf.random.set_seed(0)
    np.random.seed(0)
    random.seed(0)
    torch.manual_seed(0)
    tf.keras.utils.set_random_seed(0)
    tf.config.experimental.enable_op_determinism()


def export_tflite(model_path: Path, imgsz: int, output_path: Path):
    model_dir = _export(YOLO(str(model_path)), format="tflite", imgsz=imgsz)
    tflite_path = os.path.join(
        os.path.dirname(model_dir), f"{model_path.stem}_full_integer_quant.tflite"
    )
    shutil.copy(tflite_path, output_path)


def export_edgetpu(output_dir, model_name: str, imgsz: int, ext="pt"):
    set_seeds()

    with tempfile.TemporaryDirectory() as temp_dir:
        os.chdir(temp_dir)

        output_prefix = f"{model_name}_{imgsz}x{imgsz}"
        edgetpu_path = _export(
            YOLO(f"{model_name}.{ext}"), format="edgetpu", imgsz=imgsz
        )
        new_edgetpu_path = os.path.join(output_dir, f"{output_prefix}_edgetpu.tflite")

        shutil.copy(edgetpu_path, new_edgetpu_path)
        return new_edgetpu_path


def export_cpu_and_edgetpu(output_dir, model_name: str, imgsz: int):
    with tempfile.TemporaryDirectory() as temp_dir:
        os.chdir(temp_dir)
        model = YOLO(f"{model_name}.pt")
        _export_cpu_and_edgetpu(model, model_name, imgsz, output_dir)


def export_cpu_and_edgetpu2(output_dir, model_name: str, model_path: Path, imgsz: int):
    with tempfile.TemporaryDirectory() as temp_dir:
        new_model_path = os.path.join(temp_dir, f"{model_name}.pt")
        shutil.copy(model_path, new_model_path)

        model = YOLO(new_model_path)
        _export_cpu_and_edgetpu(model, model_name, imgsz, output_dir)


def _export_cpu_and_edgetpu(model: YOLO, model_name: str, imgsz: int, output_dir: str):
    output_prefix = f"{model_name}_{imgsz}x{imgsz}"
    edgetpu_path = _export(model, format="edgetpu", imgsz=imgsz)
    new_edgetpu_path = os.path.join(output_dir, f"{output_prefix}_edgetpu.tflite")

    tflite_path = os.path.join(
        os.path.dirname(edgetpu_path), f"{model_name}_full_integer_quant.tflite"
    )
    new_tflite_path = os.path.join(output_dir, f"{output_prefix}.tflite")

    shutil.copy(tflite_path, new_tflite_path)
    shutil.copy(edgetpu_path, new_edgetpu_path)


def _export(
    yolo: YOLO,
    **kwargs,
):
    _check_is_pytorch_model(yolo)
    custom = {
        "batch": 1,
        "data": None,
        "device": None,  # reset to avoid multi-GPU errors
        "verbose": False,
    }  # method defaults
    args = {
        **yolo.overrides,
        **custom,
        **kwargs,
        "mode": "export",
    }  # highest priority args on the right
    assert isinstance(yolo.model, DetectionModel)
    return MyExporter(overrides=args).call(model=yolo.model)


class MyExporter(Exporter):
    @smart_inference_mode()
    def call(self, model: DetectionModel) -> str:
        assert isinstance(model, DetectionModel)
        """Returns list of exported files/dirs after running callbacks."""
        self.device = "cpu"

        # Checks
        if not hasattr(model, "names"):
            model.names = default_class_names()
        model.names = check_class_names(model.names)

        assert not (self.args.half and self.args.int8)

        self.imgsz = check_imgsz(
            self.args.imgsz, stride=model.stride, min_dim=2
        )  # check image size

        if self.args.int8 and not self.args.data:
            self.args.data = (
                DEFAULT_CFG.data or TASK2DATA[getattr(model, "task", "detect")]
            )  # assign default data
            LOGGER.warning(
                "WARNING ⚠️ INT8 export requires a missing 'data' arg for calibration. "
                f"Using default 'data={self.args.data}'."
            )
        # Input
        im = torch.zeros(self.args.batch, 3, *self.imgsz).to(self.device)
        file = Path(
            getattr(model, "pt_path", None)
            or getattr(model, "yaml_file", None)
            or model.yaml.get("yaml_file", "")
        )
        if file.suffix in {".yaml", ".yml"}:
            file = Path(file.name)

        # Update model
        model = deepcopy(model).to(self.device)
        for p in model.parameters():
            p.requires_grad = False
        model.eval()
        model.float()
        model = model.fuse()
        for m in model.modules():
            if isinstance(
                m, (Detect, RTDETRDecoder)
            ):  # includes all Detect subclasses like Segment, Pose, OBB
                m.dynamic = self.args.dynamic
                m.export = True
                m.format = self.args.format
                m.max_det = self.args.max_det

        y = None
        for _ in range(2):
            y = model(im)  # dry runs

        # Filter warnings
        warnings.filterwarnings(
            "ignore", category=torch.jit.TracerWarning
        )  # suppress TracerWarning
        warnings.filterwarnings(
            "ignore", category=UserWarning
        )  # suppress shape prim::Constant missing ONNX warning
        warnings.filterwarnings(
            "ignore", category=DeprecationWarning
        )  # suppress CoreML np.bool deprecation warning

        # Assign
        self.im = im
        self.model = model
        self.file = file
        self.output_shape = (
            tuple(y.shape)
            if isinstance(y, torch.Tensor)
            else tuple(tuple(x.shape if isinstance(x, torch.Tensor) else []) for x in y)
        )
        self.pretty_name = Path(
            self.model.yaml.get("yaml_file", self.file)
        ).stem.replace("yolo", "YOLO")
        data = (
            model.args["data"]
            if hasattr(model, "args") and isinstance(model.args, dict)
            else ""
        )
        description = f'Ultralytics {self.pretty_name} model {f"trained on {data}" if data else ""}'
        self.metadata = {
            "description": description,
            "author": "Ultralytics",
            "version": __version__,
            "license": "AGPL-3.0 License (https://ultralytics.com/license)",
            "docs": "https://docs.ultralytics.com",
            "stride": int(max(model.stride)),
            "task": model.task,
            "batch": self.args.batch,
            "imgsz": self.imgsz,
            "names": model.names,
        }  # model metadata

        LOGGER.info(
            f"\n{colorstr('PyTorch:')} starting from '{file}' with input shape {tuple(im.shape)} BCHW and "
            f'output shape(s) {self.output_shape} ({file_size(file):.1f} MB)'
        )

        self.args.int8 = True

        saved_model_path, _ = self.export_saved_model()
        tflite_path = Path(saved_model_path).joinpath(
            f"{self.file.stem}_full_integer_quant.tflite"
        )
        if self.args.format == "edgetpu":
            edgetpu_model_path, _ = self.export_edgetpu(tflite_model=str(tflite_path))
            LOGGER.info("\nExport complete")
            return edgetpu_model_path
        else:
            LOGGER.info("\nExport complete")
            return tflite_path

    def export_saved_model(self, prefix=colorstr("TensorFlow SavedModel:")):
        import tensorflow as tf  # noqa

        LOGGER.info(f"\n{prefix} starting export with tensorflow {tf.__version__}...")
        check_version(
            tf.__version__,
            ">=2.0.0",
            name="tensorflow",
            verbose=True,
            msg="https://github.com/ultralytics/ultralytics/issues/5161",
        )

        f = Path(str(self.file).replace(self.file.suffix, "_saved_model"))
        if f.is_dir():
            shutil.rmtree(f)  # delete output folder

        # Pre-download calibration file to fix https://github.com/PINTO0309/onnx2tf/issues/545
        # onnx2tf_file = Path("calibration_image_sample_data_20x128x128x3_float32.npy")
        # if not onnx2tf_file.exists():
        #    attempt_download_asset(f"{onnx2tf_file}.zip", unzip=True, delete=True)

        # Export to ONNX
        f_onnx, _ = self.export_onnx2(True)

        # Export to TF
        # int8 calibration images file
        tmp_file = f / "tmp_tflite_int8_calibration_images.npy"

        LOGGER.info(f"{prefix} starting TFLite export with onnx2tf...")
        onnx2tf_convert(
            input_onnx_file_path=f_onnx,
            output_folder_path=str(f),
            not_use_onnxsim=True,
            verbosity="error",
            # input_output_quant_dtype="uint8",
            output_integer_quantized_tflite=self.args.int8,
            quant_type="per-tensor",  # "per-tensor" (faster) or "per-channel" (slower but more accurate)
            disable_group_convolution=True,  # for end-to-end model compatibility
            enable_batchmatmul_unfold=True,  # for end-to-end model compatibility
        )
        yaml_save(f / "metadata.yaml", self.metadata)  # add metadata.yaml

        # Remove/rename TFLite models
        if self.args.int8:
            tmp_file.unlink(missing_ok=True)
            for file in f.rglob("*_dynamic_range_quant.tflite"):
                file.rename(
                    file.with_name(
                        file.stem.replace("_dynamic_range_quant", "_int8") + file.suffix
                    )
                )
            for file in f.rglob("*_integer_quant_with_int16_act.tflite"):
                file.unlink()  # delete extra fp16 activation TFLite files

        # Add TFLite metadata
        for file in f.rglob("*.tflite"):
            f.unlink() if "quant_with_int16_act.tflite" in str(
                f
            ) else self._add_tflite_metadata(file)

        return str(f), tf.saved_model.load(
            f, tags=None, options=None
        )  # load saved_model as Keras model

    def export_onnx2(self, simplify: bool, prefix=colorstr("ONNX:")):
        import onnx

        opset_version = self.args.opset or get_latest_opset()
        LOGGER.info(
            f"\n{prefix} starting export with onnx {onnx.__version__} opset {opset_version}..."
        )
        f = str(self.file.with_suffix(".onnx"))

        output_names = (
            ["output0", "output1"]
            if isinstance(self.model, SegmentationModel)
            else ["output0"]
        )

        torch.onnx.export(
            self.model,
            self.im,
            f,
            verbose=False,
            opset_version=opset_version,
            do_constant_folding=True,  # WARNING: DNN inference with torch>=1.12 may require do_constant_folding=False
            input_names=["images"],
            output_names=output_names,
        )

        # Checks
        model_onnx = onnx.load(f)  # load onnx model

        # Simplify
        if simplify:
            try:
                import onnxslim

                LOGGER.info(
                    f"{prefix} slimming with onnxslim {onnxslim.__version__}..."
                )
                model_onnx = onnxslim.slim(model_onnx)

            except Exception as e:
                LOGGER.warning(f"{prefix} simplifier failure: {e}")

        # Metadata
        for k, v in self.metadata.items():
            meta = model_onnx.metadata_props.add()
            meta.key, meta.value = k, str(v)

        onnx.save(model_onnx, f)
        return f, model_onnx

    def export_edgetpu(self, tflite_model, prefix=colorstr("Edge TPU:")):
        cmd = "edgetpu_compiler --version"
        ver = (
            subprocess.run(cmd, shell=True, capture_output=True, check=True)
            .stdout.decode()
            .split()[-1]
        )

        LOGGER.info(f"\n{prefix} starting export with Edge TPU compiler {ver}...")
        f = str(tflite_model).replace(".tflite", "_edgetpu.tflite")  # Edge TPU model

        cmd = f'edgetpu_compiler -s -d -k 10 --out_dir "{Path(f).parent}" "{tflite_model}"'
        LOGGER.info(f"{prefix} running '{cmd}'")
        subprocess.run(cmd, shell=True)
        self._add_tflite_metadata(f)
        return f, None

    def _add_tflite_metadata(self, file):
        import flatbuffers

        try:
            # TFLite Support bug https://github.com/tensorflow/tflite-support/issues/954#issuecomment-2108570845
            from tensorflow_lite_support.metadata import (
                metadata_schema_py_generated as schema,
            )

            # noqa
            from tensorflow_lite_support.metadata.python import metadata  # noqa
        except (
            ImportError
        ):  # ARM64 systems may not have the 'tensorflow_lite_support' package available
            from tflite_support import metadata  # noqa
            from tflite_support import metadata_schema_py_generated as schema  # noqa

        # Create model info
        model_meta = schema.ModelMetadataT()
        model_meta.name = self.metadata["description"]
        model_meta.version = self.metadata["version"]
        model_meta.author = self.metadata["author"]
        model_meta.license = self.metadata["license"]

        # Create input info
        input_meta = schema.TensorMetadataT()
        input_meta.name = "image"
        input_meta.description = "Input image to be detected."
        input_meta.content = schema.ContentT()
        input_meta.content.contentProperties = schema.ImagePropertiesT()
        input_meta.content.contentProperties.colorSpace = schema.ColorSpaceType.RGB
        input_meta.content.contentPropertiesType = (
            schema.ContentProperties.ImageProperties
        )

        # Create output info
        output1 = schema.TensorMetadataT()
        output1.name = "output"
        output1.description = (
            "Coordinates of detected objects, class labels, and confidence score"
        )
        if self.model.task == "segment":
            output2 = schema.TensorMetadataT()
            output2.name = "output"
            output2.description = "Mask protos"

        # Create subgraph info
        subgraph = schema.SubGraphMetadataT()
        subgraph.inputTensorMetadata = [input_meta]
        subgraph.outputTensorMetadata = (
            [output1, output2] if self.model.task == "segment" else [output1]
        )
        model_meta.subgraphMetadata = [subgraph]

        b = flatbuffers.Builder(0)
        b.Finish(
            model_meta.Pack(b), metadata.MetadataPopulator.METADATA_FILE_IDENTIFIER
        )
        metadata_buf = b.Output()

        populator = metadata.MetadataPopulator.with_model_file(str(file))
        populator.load_metadata_buffer(metadata_buf)
        populator.populate()


def _check_is_pytorch_model(model: YOLO) -> None:
    pt_str = isinstance(model, (str, Path)) and Path(model).suffix == ".pt"
    pt_module = isinstance(model, nn.Module)
    if not (pt_module or pt_str):
        raise TypeError(
            f"model='{model}' should be a *.pt PyTorch model to run this method, but is a different format. "
        )
