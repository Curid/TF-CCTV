import json
import os
import shutil
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path

import numpy as np
from py_utils.dataset import objects365_to_coco_class_id, read_list_file
from ultralytics.utils.ops import xyxy2xywhn


@dataclass
class Label:
    category_id: int
    x_center: float
    y_center: float
    width: float
    height: float


@dataclass
class Image:
    id: int
    path: Path
    labels: list[Label]


def gen_coco(
    datasets_path: Path,
    labels_path: Path,
    dataset_id: int,
    gray=False,
) -> list[Image]:
    print(f"gen_coco gray={gray}")

    coco_blacklist_path = os.path.join(datasets_path, "mscoco_blacklist_ids.txt")
    coco_blacklist_ids = read_list_file(coco_blacklist_path)

    coco_gray_path = os.path.join(datasets_path, "mscoco_gray_ids.txt")
    coco_gray_ids = read_list_file(coco_gray_path)

    class_ids = read_class_ids(labels_path)

    coco_path = datasets_path.joinpath("coco2017")

    print("reading instances_train2017.json")
    data_file = coco_path.joinpath("instances_train2017.json")
    data_json = json.load(open(data_file, "r"))

    class_ids = read_class_ids(labels_path)

    imgs = {img["id"]: img for img in data_json["images"]}

    labels = defaultdict(list[Label])
    for ann in data_json["annotations"]:
        img_id = ann["image_id"]
        if img_id in coco_blacklist_ids:
            continue
        category_id = ann["category_id"] - 1
        if ann["iscrowd"] and category_id == 0:
            continue
        if not gray and ann["image_id"] in coco_gray_ids:
            continue
        img_id = ann["image_id"]
        if category_id not in class_ids:
            continue
        width = imgs[img_id]["width"]
        height = imgs[img_id]["height"]
        labels[img_id].append(
            coco_bbox_to_label(width, height, category_id, ann["bbox"])
        )

    if gray:
        images_path = datasets_path.joinpath("gray-coco2017", "images")
    else:
        images_path = datasets_path.joinpath("coco2017", "images")

    images: list[Image] = []
    for img_id, labels in labels.items():
        images.append(
            Image(
                id=img_id + 1000000000 * dataset_id,
                path=images_path.joinpath(imgs[img_id]["file_name"]),
                labels=labels,
            )
        )

    return images


def gen_objects365(
    datasets_path: Path,
    labels_path: Path,
    dataset_id: int,
    val_cctv_v1=False,
    gray=False,
) -> list[Image]:
    print(f"gen_obj365 gray={gray}")
    val_cctv_v1_path = os.path.join(datasets_path, "objects365_val-cctv_v1.txt")
    val_cctv_v1_ids = read_list_file(val_cctv_v1_path)

    class_ids = read_class_ids(labels_path)

    val_path = os.path.join(datasets_path, "val")

    print("reading zhiyuan_objv2_val.json")
    val_file = os.path.join(val_path, "zhiyuan_objv2_val.json")
    val_json = json.load(open(val_file, "r"))

    imgs = {img["id"]: img for img in val_json["images"]}

    labels = defaultdict(list[Label])
    for ann in val_json["annotations"]:
        category_id = objects365_to_coco_class_id(ann["category_id"] - 1)
        if category_id == -1:
            continue
        if ann["iscrowd"] and category_id == 0:
            continue
        img_id = ann["image_id"]
        if val_cctv_v1 and img_id not in val_cctv_v1_ids:
            continue
        if category_id not in class_ids:
            continue
        width = imgs[img_id]["width"]
        height = imgs[img_id]["height"]
        labels[img_id].append(
            coco_bbox_to_label(width, height, category_id, ann["bbox"])
        )

    if gray:
        images_path = datasets_path.joinpath("images-gray")
    else:
        images_path = datasets_path.joinpath("images")

    images: list[Image] = []
    for img_id, labels in labels.items():
        if val_cctv_v1 and img_id not in val_cctv_v1_ids:
            continue
        images.append(
            Image(
                id=img_id + 1000000000 * dataset_id,
                path=images_path.joinpath(str(img_id) + ".jpg"),
                labels=labels,
            )
        )

    return images


def coco_bbox_to_label(
    width: int, height: int, category_id: int, bbox: list[float]
) -> Label:
    x, y, w, h = bbox
    xyxy = np.array([x, y, x + w, y + h])[None]
    x, y, w, h = xyxy2xywhn(xyxy, w=width, h=height, clip=True)[0]
    return Label(
        category_id=category_id,
        x_center=float(x),
        y_center=float(y),
        width=float(w),
        height=float(h),
    )


def write_dataset(output_dir: Path, dataset: list[Image], dataset_type="train"):
    images_dir = output_dir.joinpath("images", dataset_type)
    labels_dir = output_dir.joinpath("labels", dataset_type)
    os.makedirs(images_dir)
    os.makedirs(labels_dir)

    for image in dataset:
        if not image.path.exists():
            raise Exception(f"image does not exist: {image.path}")
        new_image_path = images_dir.joinpath(str(image.id) + ".jpg")
        shutil.copy(image.path, new_image_path)

        label_file = labels_dir.joinpath(str(image.id) + ".txt")
        with open(label_file, "w") as file:
            for label in image.labels:
                cid, x, y, w, h = (
                    label.category_id,
                    label.x_center,
                    label.y_center,
                    label.width,
                    label.height,
                )
                file.write(f"{cid} {x:.5f} {y:.5f} {w:.5f} {h:.5f}\n")


# Reads -map.txt labels file.
def read_class_ids(path):
    ids = []

    with open(path, "r") as labels_file:
        for line in labels_file:
            class_id = line.strip().split("  ")[0]
            class_id = int(class_id)
            if class_id in ids:
                raise ValueError("duplicate id", class_id)
            ids.append(class_id)
    return ids
