import os
from pathlib import Path
from yolo_utils.dataset import write_dataset, gen_objects365

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
home_dir = script_dir.parent.parent.parent
datasets_path = home_dir.joinpath("datasets", "objects365")

labels_path = home_dir.joinpath("labels", "person-map.txt")

gray_objects365 = gen_objects365(
    datasets_path, labels_path, 1, val_cctv_v1=True, gray=True
)

print(f"{len(gray_objects365)} images")

write_dataset(script_dir, gray_objects365, dataset_type="val")
