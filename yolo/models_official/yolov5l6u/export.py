import os
from pathlib import Path

from yolo_utils.ultralytics import export_edgetpu

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))

path_320 = export_edgetpu(script_dir, "yolov5l6u", 320)
changing_bytes = bytes([0x54, 0x29, 0x9B, 0x74, 0x17, 0xFB, 0x47, 0x0F])
with open(path_320, "r+b") as f:
    f.seek(0x6AD8)
    f.write(changing_bytes)
    f.seek(0x6E66C8)
    f.write(changing_bytes)
