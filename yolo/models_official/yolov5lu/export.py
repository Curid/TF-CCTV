import os
from pathlib import Path

from yolo_utils.ultralytics import export_edgetpu

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))

path_320 = export_edgetpu(script_dir, "yolov5lu", 320)
changing_bytes = bytes([0xD2, 0x91, 0x3F, 0x4, 0xE, 0x80, 0x8E, 0xAB])
with open(path_320, "r+b") as f:
    f.seek(0x6A18)
    f.write(changing_bytes)
    f.seek(0x6E66C8)
    f.write(changing_bytes)
