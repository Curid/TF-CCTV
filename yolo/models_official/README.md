## YOLOv11

#### n_320x320

```
On-chip memory used for caching model parameters: 1.38MiB
On-chip memory remaining for caching model parameters: 6.14MiB
Off-chip memory used for streaming uncached model parameters: 8.00KiB
Number of operations that will run on Edge TPU: 275
Number of operations that will run on CPU: 222
```

<br>

## YOLOv10

https://github.com/ultralytics/ultralytics/issues/16074

<br>

## YOLOv9

#### t_320x320

```
On-chip memory used for caching model parameters: 3.18MiB
On-chip memory remaining for caching model parameters: 4.34MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 654
Number of operations that will run on CPU: 22
```

#### t_480x480

```
On-chip memory used for caching model parameters: 3.21MiB
On-chip memory remaining for caching model parameters: 3.61MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 652
Number of operations that will run on CPU: 24
```

#### t_576x576

```
On-chip memory used for caching model parameters: 3.29MiB
On-chip memory remaining for caching model parameters: 3.36MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 651
Number of operations that will run on CPU: 24
```

#### s_320x320

```
On-chip memory used for caching model parameters: 7.03MiB
On-chip memory remaining for caching model parameters: 256.00B
Off-chip memory used for streaming uncached model parameters: 517.50KiB
Number of operations that will run on Edge TPU: 654
Number of operations that will run on CPU: 22
```

#### s_480x480

```
On-chip memory used for caching model parameters: 6.98MiB
On-chip memory remaining for caching model parameters: 1.50KiB
Off-chip memory used for streaming uncached model parameters: 549.19KiB
Number of operations that will run on Edge TPU: 652
Number of operations that will run on CPU: 24
```

#### m_320x320

```
On-chip memory used for caching model parameters: 6.86MiB
On-chip memory remaining for caching model parameters: 1.00KiB
Off-chip memory used for streaming uncached model parameters: 14.01MiB
Number of operations that will run on Edge TPU: 492
Number of operations that will run on CPU: 22
```

#### c_320x320

```
On-chip memory used for caching model parameters: 6.57MiB
On-chip memory remaining for caching model parameters: 768.00B
Off-chip memory used for streaming uncached model parameters: 17.77MiB
Number of operations that will run on Edge TPU: 525
Number of operations that will run on CPU: 22
```

<br>

## YOLOv8

#### n_320x320

```
On-chip memory used for caching model parameters: 3.43MiB
On-chip memory remaining for caching model parameters: 4.10MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 233
Number of operations that will run on CPU: 22
```

#### n_480x480

```
On-chip memory used for caching model parameters: 3.46MiB
On-chip memory remaining for caching model parameters: 3.37MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 231
Number of operations that will run on CPU: 24
```

#### n_576x576

```
On-chip memory used for caching model parameters: 3.54MiB
On-chip memory remaining for caching model parameters: 3.11MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 230
Number of operations that will run on CPU: 24
```

#### s_320x320

```
On-chip memory used for caching model parameters: 7.04MiB
On-chip memory remaining for caching model parameters: 0.00B
Off-chip memory used for streaming uncached model parameters: 3.69MiB
Number of operations that will run on Edge TPU: 233
Number of operations that will run on CPU: 22
```

#### s_480x480

```
On-chip memory used for caching model parameters: 6.98MiB
On-chip memory remaining for caching model parameters: 512.00B
Off-chip memory used for streaming uncached model parameters: 3.78MiB
Number of operations that will run on Edge TPU: 231
Number of operations that will run on CPU: 24
```

#### m_320x320

```
On-chip memory used for caching model parameters: 7.14MiB
On-chip memory remaining for caching model parameters: 0.00B
Off-chip memory used for streaming uncached model parameters: 18.64MiB
Number of operations that will run on Edge TPU: 299
Number of operations that will run on CPU: 22
```

<br>

## YOLOv7

https://github.com/WongKinYiu/yolov7/issues/52

<br>

## YOLOv6

https://github.com/meituan/YOLOv6/issues/630

<br>

## YOLOv5


#### nu_320x320

```
On-chip memory used for caching model parameters: 2.94MiB
On-chip memory remaining for caching model parameters: 4.59MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 254
Number of operations that will run on CPU: 22
```


#### nu_480x480

```
On-chip memory used for caching model parameters: 2.97MiB
On-chip memory remaining for caching model parameters: 3.85MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 252
Number of operations that will run on CPU: 24
```

#### nu_576x576

```
On-chip memory used for caching model parameters: 3.05MiB
On-chip memory remaining for caching model parameters: 3.60MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 252
Number of operations that will run on CPU: 24
```

#### n6u_320x320

```
On-chip memory used for caching model parameters: 4.81MiB
On-chip memory remaining for caching model parameters: 2.72MiB
Off-chip memory used for streaming uncached model parameters: 0.00B
Number of operations that will run on Edge TPU: 335
Number of operations that will run on CPU: 22
```

<br>

#### su_320x320

```
On-chip memory used for caching model parameters: 7.04MiB
On-chip memory remaining for caching model parameters: 0.00B
Off-chip memory used for streaming uncached model parameters: 1.78MiB
Number of operations that will run on Edge TPU: 254
Number of operations that will run on CPU: 22
```


#### su_480x480

```
On-chip memory used for caching model parameters: 6.97MiB
On-chip memory remaining for caching model parameters: 512.00B
Off-chip memory used for streaming uncached model parameters: 1.87MiB
Number of operations that will run on Edge TPU: 252
Number of operations that will run on CPU: 24
```

#### s6u_320x320

```
On-chip memory used for caching model parameters: 7.04MiB
On-chip memory remaining for caching model parameters: 0.00B
Off-chip memory used for streaming uncached model parameters: 7.67MiB
Number of operations that will run on Edge TPU: 335
Number of operations that will run on CPU: 22
```


<br>

#### mu_320x320

```
On-chip memory used for caching model parameters: 7.14MiB
On-chip memory remaining for caching model parameters: 768.00B
Off-chip memory used for streaming uncached model parameters: 17.14MiB
Number of operations that will run on Edge TPU: 327
Number of operations that will run on CPU: 22

```

#### m6u_320x320

```
On-chip memory used for caching model parameters: 7.14MiB
On-chip memory remaining for caching model parameters: 256.00B
Off-chip memory used for streaming uncached model parameters: 33.21MiB
Number of operations that will run on Edge TPU: 427
Number of operations that will run on CPU: 22
```


<br>

#### lu_320x320

```
On-chip memory used for caching model parameters: 6.86MiB
On-chip memory remaining for caching model parameters: 768.00B
Off-chip memory used for streaming uncached model parameters: 44.03MiB
Number of operations that will run on Edge TPU: 400
Number of operations that will run on CPU: 22
```

#### l6u_320x320

```
On-chip memory used for caching model parameters: 6.86MiB
On-chip memory remaining for caching model parameters: 0.00B
Off-chip memory used for streaming uncached model parameters: 75.42MiB
Number of operations that will run on Edge TPU: 519
Number of operations that will run on CPU: 22
```


<br>

## YOLOv4

https://github.com/hunglc007/tensorflow-yolov4-tflite/issues/69
https://wiki.loliot.net/docs/lang/python/libraries/yolov4/python-yolov4-edge-tpu

## YOLOv3

https://github.com/guichristmann/edge-tpu-tiny-yolo
https://github.com/zzh8829/yolov3-tf2

## YOLOv2

https://stackoverflow.com/questions/58687216/how-to-run-yolov2-lite-tflite-in-coral-edge-tpu-usb-accelerator
