import os
from pathlib import Path

from yolo_utils.ultralytics import export_edgetpu

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))

path_320 = export_edgetpu(script_dir, "yolo11n", 320)
changing_bytes = bytes([0x48, 0x36, 0x32, 0x7D, 0x7D, 0x1D, 0x32, 0xA0])
with open(path_320, "r+b") as f:
    f.seek(0x163E3C)
    f.write(changing_bytes)
    f.seek(0x2B5B1C)
    f.write(changing_bytes)
