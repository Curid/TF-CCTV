import os
from pathlib import Path

from yolo_utils.ultralytics import export_edgetpu

print("broken")
exit(0)

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
file_path = export_edgetpu(script_dir, "yolov10n", 320)

# Make edgetpu_compiler output reproducible.
changing_bytes = bytes([0x74, 0x46, 0x52, 0xCC, 0xE3, 0x8B, 0x56, 0x56])
with open(file_path, "r+b") as f:
    f.seek(0x2A9DBE)
    f.write(changing_bytes)
    f.seek(0x2ACCC6)
    f.write(changing_bytes)
