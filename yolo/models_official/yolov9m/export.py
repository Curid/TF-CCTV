import os
from pathlib import Path

from yolo_utils.ultralytics import export_edgetpu

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))

path_320 = export_edgetpu(script_dir, "yolov9m", 320)
changing_bytes = bytes([0x16, 0xA0, 0x23, 0x6D, 0xE1, 0x5D, 0xD4, 0x7F])
with open(path_320, "r+b") as f:
    f.seek(0x6A48)
    f.write(changing_bytes)
    f.seek(0x2738E8)
    f.write(changing_bytes)
