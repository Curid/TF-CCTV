import os
from pathlib import Path

from yolo_utils.ultralytics import export_edgetpu

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))

path_320 = export_edgetpu(script_dir, "yolov5m6u", 320)
changing_bytes = bytes([0x8, 0xAC, 0x1F, 0xD6, 0x1, 0x66, 0x31, 0xFF])
with open(path_320, "r+b") as f:
    f.seek(0x6AD8)
    f.write(changing_bytes)
    f.seek(0x72D710)
    f.write(changing_bytes)
