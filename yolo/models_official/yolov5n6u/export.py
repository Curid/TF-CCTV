import os
from pathlib import Path

from yolo_utils.ultralytics import export_cpu_and_edgetpu

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))

export_cpu_and_edgetpu(script_dir, "yolov5n6u", 320)
