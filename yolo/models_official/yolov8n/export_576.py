import os
from pathlib import Path

from yolo_utils.ultralytics import export_edgetpu

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))

path_576 = export_edgetpu(script_dir, "yolov8n", 576)
changing_bytes = bytes([0x55, 0xA0, 0x2F, 0xF6, 0xA0, 0xA1, 0x4B, 0x37])
with open(path_576, "r+b") as f:
    f.seek(0xD8DC)
    f.write(changing_bytes)
    f.seek(0x3886EC)
    f.write(changing_bytes)
