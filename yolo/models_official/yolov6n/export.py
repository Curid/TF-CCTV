import os
from pathlib import Path

from yolo_utils.ultralytics import export_edgetpu

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))

print("broken")
exit(1)

path_320 = export_edgetpu(script_dir, "yolov6n", 320, ext="yaml")
changing_bytes = bytes([0x72, 0xE0, 0xF4, 0x32, 0xE9, 0xF3, 0xF9, 0x81])
with open(path_320, "r+b") as f:
    f.seek(0x6A00)
    f.write(changing_bytes)
    f.seek(0x4918F8)
    f.write(changing_bytes)
