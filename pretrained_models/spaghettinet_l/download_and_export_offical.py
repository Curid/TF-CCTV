import os
import subprocess
import tarfile
import tempfile
import urllib.request
from pathlib import Path
import shutil

from tensorflow.python import tf2

if not tf2.enabled():
    raise Exception("Requires Tensorflow 2")

model_url = "http://download.tensorflow.org/models/object_detection/tf1/spaghettinet_edgetpu_l_2021_10_13.tar.gz"

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
os.chdir(script_dir)

with tempfile.TemporaryDirectory() as output_dir:
    tar_path = os.path.join(output_dir, "model.tar.gz")
    urllib.request.urlretrieve(model_url, tar_path)

    with tarfile.open(tar_path) as tar:
        for member in tar.getmembers():
            parts = Path(member.name).parts
            if len(parts) != 2:
                continue

            # Trim root dir.
            member.name = os.path.join(*[v for v in parts][1:])
            tar.extract(member, output_dir)

    subprocess.run(["edgetpu_compiler", "-s", os.path.join(output_dir, "model.tflite")])

    os.remove("./model_edgetpu.log")
    shutil.move("./model_edgetpu.tflite", "spaghettinet_l_official.tflite")
