import os
import shutil
import tarfile
import tempfile
import urllib.request
from pathlib import Path

import tensorflow.compat.v1 as tf
from google.protobuf import text_format
from od import export_tflite_ssd_graph_lib, tf_version
from od.protos import pipeline_pb2

if not tf_version.is_tf1():
    raise Exception("Requires unmaintained Tensorflow 1.15")

model_url = "http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v1_quantized_300x300_coco14_sync_2018_07_18.tar.gz"

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
os.chdir(script_dir)

with tempfile.TemporaryDirectory() as output_dir:
    tar_path = os.path.join(output_dir, "model.tar.gz")
    urllib.request.urlretrieve(model_url, tar_path)

    with tarfile.open(tar_path) as tar:
        for member in tar.getmembers():
            parts = Path(member.name).parts
            if len(parts) != 2:
                continue

            # Trim root dir.
            member.name = os.path.join(*[v for v in parts][1:])
            tar.extract(member, output_dir)

    trained_checkpoint_prefix = os.path.join(output_dir, "model.ckpt")
    pipeline_config_path = os.path.join(output_dir, "pipeline.config")

    pipeline_config = pipeline_pb2.TrainEvalPipelineConfig()

    with tf.gfile.GFile(pipeline_config_path, "r") as f:
        text_format.Merge(f.read(), pipeline_config)

    export_tflite_ssd_graph_lib.export_tflite_graph(
        pipeline_config,
        trained_checkpoint_prefix,
        output_dir,
        True,
    )

    shutil.move(os.path.join(output_dir, "tflite_graph.pb"), "./tflite_graph.pb")
