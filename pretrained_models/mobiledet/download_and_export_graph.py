import os
import shutil
import tarfile
import tempfile
import urllib.request
from pathlib import Path

import tensorflow.compat.v1 as tf
from google.protobuf import text_format
from od import export_tflite_ssd_graph_lib, tf_version
from od.protos import pipeline_pb2

if not tf_version.is_tf1():
    raise Exception("Requires unmaintained Tensorflow 1.15")

model_name = "mobiledet"

model_url = "http://download.tensorflow.org/models/object_detection/ssdlite_mobiledet_edgetpu_320x320_coco_2020_05_19.tar.gz"
official_model_url = "https://raw.githubusercontent.com/google-coral/test_data/master/ssdlite_mobiledet_coco_qat_postprocess_edgetpu.tflite"

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
os.chdir(script_dir)

tar_files = [
    "uint8/model.ckpt-400000.data-00000-of-00001",
    "uint8/model.ckpt-400000.index",
    "uint8/model.ckpt-400000.meta",
    "uint8/pipeline.config",
]

with tempfile.TemporaryDirectory() as output_dir:
    tar_path = os.path.join(output_dir, "{model_name}.tar.gz")
    urllib.request.urlretrieve(model_url, tar_path)
    urllib.request.urlretrieve(official_model_url, f"./{model_name}_official.tflite")

    with tarfile.open(tar_path) as tar:
        for member in tar.getmembers():
            parts = Path(member.name).parts
            if len(parts) != 3:
                continue

            # Trim root dir.
            member.name = os.path.join(*[v for v in parts][1:])
            if member.name in tar_files:
                tar.extract(member, output_dir)

    trained_checkpoint_prefix = os.path.join(output_dir, "uint8", "model.ckpt-400000")
    pipeline_config_path = os.path.join(output_dir, "uint8", "pipeline.config")

    pipeline_config = pipeline_pb2.TrainEvalPipelineConfig()

    with tf.gfile.GFile(pipeline_config_path, "r") as f:
        text_format.Merge(f.read(), pipeline_config)

    export_tflite_ssd_graph_lib.export_tflite_graph(
        pipeline_config,
        trained_checkpoint_prefix,
        output_dir,
        True,
    )

    shutil.move(os.path.join(output_dir, "tflite_graph.pb"), "./tflite_graph.pb")
