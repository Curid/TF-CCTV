import os
import subprocess
import tempfile
from pathlib import Path

import tensorflow as tf
from tensorflow.python import tf2

if not tf2.enabled():
    raise Exception("Requires Tensorflow 2")

model_name = "mobilenetV2"

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
os.chdir(script_dir)

converter = tf.compat.v1.lite.TFLiteConverter.from_frozen_graph(
    graph_def_file="./tflite_graph.pb",
    input_arrays=["normalized_input_image_tensor"],
    output_arrays=[
        "TFLite_Detection_PostProcess",
        "TFLite_Detection_PostProcess:1",
        "TFLite_Detection_PostProcess:2",
        "TFLite_Detection_PostProcess:3",
    ],
    input_shapes={"normalized_input_image_tensor": [1, 300, 300, 3]},
)

converter.allow_custom_ops = True
converter.unfold_batchmatmul = True

converter.inference_input_type = tf.uint8
converter.inference_output_type = tf.uint8

converter.quantized_input_stats = {"normalized_input_image_tensor": (128, 127)}

tflite_model = converter.convert()

with tempfile.TemporaryDirectory() as output_dir:
    tflite_path = os.path.join(output_dir, f"{model_name}.tflite")
    open(tflite_path, "wb").write(tflite_model)

    subprocess.run(["edgetpu_compiler", "-s", tflite_path])
    os.remove(f"./{model_name}_edgetpu.log")
