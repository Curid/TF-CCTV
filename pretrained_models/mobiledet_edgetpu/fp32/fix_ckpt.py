import os

from tensorflow.python import pywrap_tensorflow
import tensorflow as tf


def main():
    script_path = os.path.realpath(__file__)
    dir_path = os.path.dirname(script_path)
    ckpt_path = os.path.join(dir_path, "model.ckpt-400000")

    with tf.compat.v1.Session() as sess:
        var_list = []

        reader = pywrap_tensorflow.NewCheckpointReader(ckpt_path)
        for var_name in reader.get_variable_to_shape_map():
            new_name = var_name
            if var_name[:17] == "FeatureExtractor/":
                new_name = var_name[17:]

            var = reader.get_tensor(var_name)
            var = tf.Variable(var, name=new_name)
            var_list.append(var)

        sess.run(tf.compat.v1.global_variables_initializer())
        saver = tf.compat.v1.train.Saver(var_list)
        saver.save(sess, ckpt_path)


if __name__ == "__main__":
    main()
