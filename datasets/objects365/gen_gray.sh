#!/bin/sh

set -e

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir" || exit

mkdir -p "$script_dir/images-gray"

# Generate black and white version of all images.
for file in "$script_dir"/images/*; do
	out=$(printf '%s' "$file" | sed -e 's|images|images-gray|')
	printf "input: %s\noutput: %s\n" "$file" "$out"
	ffmpeg -y -loglevel fatal -i "$file" -vf hue=s=0 "$out"
done
