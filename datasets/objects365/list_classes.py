import os
import json
from collections import defaultdict
from py_utils.dataset import read_class_ids2, objects365_to_coco_class_id


def read_list_file(path: str) -> set:
    raw_file = open(path, "r")
    lines = raw_file.readlines()
    return set([int(i) for i in lines])


def main():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    home_path = os.path.dirname(os.path.dirname(dir_path))

    datasets_path = os.path.join(home_path, "datasets")
    objects365_path = os.path.join(datasets_path, "objects365")
    image_path = os.path.join(objects365_path, "images")

    # train_file = os.path.join(objects365, "instances_train2017.json")
    val_file = os.path.join(objects365_path, "val", "zhiyuan_objv2_val.json")

    # train_json = json.load(open(train_file, "r"))
    val_json = json.load(open(val_file, "r"))

    labels_file = os.path.join(home_path, "labels", "mscoco_cctv-map.txt")
    class_to_label = read_class_ids2(labels_file)
    labels = set(class_to_label.values())

    # Free memory.
    # train_file = []
    val_file = []

    eval_path = os.path.join(objects365_path, "objects365_val-cctv_v1.txt")
    eval_ids = read_list_file(eval_path)

    objects365_val_path = os.path.join(objects365_path, "objects365_val_ids.txt")
    objects365_val = read_list_file(objects365_val_path)

    def filter_id(image_id):
        if image_id not in eval_ids:
            return False
        if image_id not in objects365_val:
            raise Exception("id is in eval_ids, but not objects365 val", image_id)
        return True

    ids_to_labels = defaultdict(set)
    for ann in val_json["annotations"]:
        img_id = ann["image_id"]
        if not filter_id(img_id):
            continue
        category_id = ann["category_id"]
        coco_class_id = objects365_to_coco_class_id(category_id - 1)
        if coco_class_id == -1:
            continue
        label = class_to_label[coco_class_id]
        if label not in labels:
            continue
        ids_to_labels[img_id].add(label)

    label_counts = defaultdict(int)
    for labels in ids_to_labels.values():
        for label in labels:
            label_counts[label] += 1

    print("")
    for [k, v] in sorted(label_counts.items(), key=lambda v: v[1], reverse=True):
        print("{:<11}{}".format(k, v))


if __name__ == "__main__":
    main()
