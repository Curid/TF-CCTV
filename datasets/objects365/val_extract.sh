#!/bin/sh

set -e

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir"

mkdir -p ./val/temp
find ./val/compressed/*/*.tar.gz -print0 | xargs -0 -i tar -xzf {} --strip-components 1 -C ./val/temp/

mkdir -p ./images
for file in ./val/temp/*; do
	new_name=$(printf "%s" "$file" | sed -e 's/.\/val\/temp\/objects365_v[[:digit:]]_0*//')
	new_path="./images/$new_name"
	if [ -e "$new_path" ]; then
		printf "file already exists: %s" "$new_path"
		exit 1
	fi
	mv "$file" "$new_path"
done

rm -r ./val/temp
