#!/bin/bash

set -e

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir"

# OUTPUT.
# ── val
#     ├── checksum
#     ├── compressed
#     │   ├── v1
#     │   │   ├── patch0.tar.gz
#     │   │   ├── patch1.tar.gz
#     │   │   ├── patch2.tar.gz
#     │   │   ├── patch3.tar.gz
#     │   │   ├── patch4.tar.gz
#     │   │   ├── patch5.tar.gz
#     │   │   ├── patch6.tar.gz
#     │   │   ├── patch7.tar.gz
#     │   │   ├── patch8.tar.gz
#     │   │   ├── patch9.tar.gz
#     │   │   ├── patch10.tar.gz
#     │   │   ├── patch11.tar.gz
#     │   │   ├── patch12.tar.gz
#     │   │   ├── patch13.tar.gz
#     │   │   ├── patch14.tar.gz
#     │   │   ├── patch15.tar.gz
#     │   └── v2
#     │       ├── patch16.tar.gz
#     │       ├── patch17.tar.gz
#     │       ├── patch18.tar.gz
#     │       ├── patch19.tar.gz
#     │       ├── patch20.tar.gz
#     │       ├── patch21.tar.gz
#     │       ├── patch22.tar.gz
#     │       ├── patch23.tar.gz
#     │       ├── patch24.tar.gz
#     │       ├── patch25.tar.gz
#     │       ├── patch26.tar.gz
#     │       ├── patch27.tar.gz
#     │       ├── patch28.tar.gz
#     │       ├── patch29.tar.gz
#     │       ├── patch30.tar.gz
#     │       ├── patch31.tar.gz
#     │       ├── patch32.tar.gz
#     │       ├── patch33.tar.gz
#     │       ├── patch34.tar.gz
#     │       ├── patch35.tar.gz
#     │       ├── patch36.tar.gz
#     │       ├── patch37.tar.gz
#     │       ├── patch38.tar.gz
#     │       ├── patch39.tar.gz
#     │       ├── patch40.tar.gz
#     │       ├── patch41.tar.gz
#     │       ├── patch42.tar.gz
#     │       ├── patch43.tar.gz
#     └── zhiyuan_objv2_val.json


mkdir -p val/compressed/v1/
mkdir -p val/compressed/v2/


wget -c https://dorc.ks3-cn-beijing.ksyun.com/data-set/2020Objects365%E6%95%B0%E6%8D%AE%E9%9B%86/val/zhiyuan_objv2_val.json -P val/

# val
for i in {0..15}
  do wget -c "https://dorc.ks3-cn-beijing.ksyun.com/data-set/2020Objects365%E6%95%B0%E6%8D%AE%E9%9B%86/val/images/v1/patch${i}.tar.gz" -P val/compressed/v1
done

for i in {16..43}
  do wget -c "https://dorc.ks3-cn-beijing.ksyun.com/data-set/2020Objects365%E6%95%B0%E6%8D%AE%E9%9B%86/val/images/v2/patch${i}.tar.gz" -P val/compressed/v2/
done
