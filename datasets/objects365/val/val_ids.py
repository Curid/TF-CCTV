import os
import json
from collections import defaultdict


def main():
    script_dir = os.path.dirname(os.path.realpath(__file__))

    val_file = os.path.join(script_dir, "zhiyuan_objv2_val.json")
    val_json = json.load(open(val_file, "r"))

    ids = [img["id"] for img in val_json["images"]]
    write_list_file(ids, script_dir + "/../objects365_val_ids.txt")


def write_list_file(items, path):
    items.sort()
    f = open(path, "w")
    for item in items:
        f.writelines(str(item) + "\n")
    f.close()


if __name__ == "__main__":
    main()
