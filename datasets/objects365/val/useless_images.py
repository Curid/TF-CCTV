import os
import json
from collections import defaultdict


def main():
    script_dir = os.path.dirname(os.path.realpath(__file__))
    home_path = os.path.dirname(os.path.dirname(os.path.dirname(script_dir)))

    val_file = os.path.join(script_dir, "zhiyuan_objv2_val.json")
    val_json = json.load(open(val_file, "r"))

    useful_labels = read_labels_file(
        os.path.join(home_path, "labels", "objects365_cctv-map.txt")
    )
    useful_labels = set([v for v in useful_labels])
    print("useful classes:", sorted(useful_labels))

    print("total images:", len(val_json["images"]))

    # Free memory.
    val_file = []

    useless_images = set()

    id_to_labels = defaultdict(set)
    for ann in val_json["annotations"]:
        img_id = ann["image_id"]
        category_id = ann["category_id"] - 1
        if ann["iscrowd"] and category_id is 0:
            useless_images.add(img_id)
        id_to_labels[img_id].add(category_id)

    # Free memory.
    val_json = []

    for img_id, labels in id_to_labels.items():
        if not labels.intersection(useful_labels):
            useless_images.add(img_id)

    print("useless images:", len(useless_images))
    write_list_file(
        list(useless_images), os.path.join(script_dir, "useless_images.txt")
    )


def read_labels_file(path: str) -> set:
    raw_file = open(path, "r")
    lines = raw_file.readlines()
    return set([int(l.split(" ")[0]) for l in lines])


def write_list_file(items, path):
    items.sort()
    f = open(path, "w")
    for item in items:
        f.writelines(str(item) + "\n")
    f.close()


if __name__ == "__main__":
    main()
