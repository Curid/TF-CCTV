#!/bin/sh

set -e

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir"

while read -r id; do
	rm "./images/$id.jpg" || true
done < "./val/useless_images.txt"
