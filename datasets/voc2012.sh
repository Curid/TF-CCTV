#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")

cd "$script_dir" || exit

wget http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar

tar -xvf ./VOCtrainval_11-May-2012.tar

mv ./VOCdevkit/VOC2012 ./

rm -r ./VOCdevkit
