#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir" || exit

mkdir -p "$script_dir/gray-coco2017/images"
cp -r "$script_dir/coco2017/instances_train2017.json" "$script_dir/gray-coco2017/"
cp -r "$script_dir/coco2017/instances_val2017.json" "$script_dir/gray-coco2017/"

# Generate black and white version of all images.
for file in "$script_dir"/coco2017/images/*; do
	out=$(printf '%s' "$file" | sed -e 's|coco2017|gray-coco2017|')
	echo input "$file" output "$out"
	ffmpeg -y -loglevel fatal -i "$file" -vf hue=s=0 "$out"
done
