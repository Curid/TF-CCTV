CHANGES

-   remove telemetry
-   change default grid_zoom
-   disable ViewBar
-   disable HorizontalNav
-   disable cells
