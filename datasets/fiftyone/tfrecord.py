import os
import tempfile
import fiftyone as fo

dir_path = os.path.dirname(os.path.realpath(__file__))
home_path = os.path.dirname(os.path.dirname(dir_path))

# dataset_dir = os.path.join(home_path, "records/gray_objects365-val-cctv-v1_person")
dataset_dir = os.path.join(home_path, "records/gray_coco-minival_person")

dataset = fo.Dataset.from_dir(
    dataset_dir=dataset_dir,
    dataset_type=fo.types.TFObjectDetectionDataset,
    images_dir=tempfile.mkdtemp(),
    max_samples=50,
)

session = fo.launch_app(dataset=dataset, port=6006)
session.wait()
