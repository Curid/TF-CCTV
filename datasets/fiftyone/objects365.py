import os
import json
import argparse
from collections import defaultdict
import numpy as np
from skimage import measure
from skimage.io import imread
from skimage.color import rgb2hsv
import fiftyone as fo
import fiftyone.core.labels as fol
import fiftyone.core.utils as fou

mask_utils = fou.lazy_import(
    "pycocotools.mask", callback=lambda: fou.ensure_import("pycocotools")
)


def parse_bbox(width, height, label, bbox):  # pylint: disable=unused-argument
    x, y, w, h = bbox
    bounding_box = [x / width, y / height, w / width, h / height]

    return fo.Detection(
        label=label,
        bounding_box=bounding_box,
    )


def read_list_file(path: str) -> set:
    raw_file = open(path, "r")
    lines = raw_file.readlines()
    return set([int(i) for i in lines])


def write_list_file(items, path):
    items.sort()
    f = open(path, "w")
    for item in items:
        f.writelines(str(item) + "\n")
    f.close()


def get_saturation(path):
    img = imread(path)

    is_grayscale = img.ndim == 2
    if is_grayscale:
        return 0

    hsv = rgb2hsv(img)
    avg_hsv_per_row = np.average(hsv, axis=0)
    avg_hsv = np.average(avg_hsv_per_row, axis=0)
    return avg_hsv[1]


def main():  # pylint: disable=too-many-locals
    parser = argparse.ArgumentParser()
    parser.add_argument("--min", help="Minimum image ID", type=int, required=True)
    parser.add_argument("--max", help="Maximum image ID", type=int, required=True)
    args = parser.parse_args()

    dir_path = os.path.dirname(os.path.realpath(__file__))
    home_path = os.path.dirname(os.path.dirname(dir_path))

    datasets_path = os.path.join(home_path, "datasets")
    objects365_path = os.path.join(datasets_path, "objects365")
    image_path = os.path.join(objects365_path, "images")

    # train_file = os.path.join(objects365, "instances_train2017.json")
    val_file = os.path.join(objects365_path, "val", "zhiyuan_objv2_val.json")

    # train_json = json.load(open(train_file, "r"))
    val_json = json.load(open(val_file, "r"))

    # Free memory.
    # train_file = []
    val_file = []

    # coco_blacklist_path = os.path.join(datasets_path, "mscoco_blacklist_ids.txt")
    # coco_blacklist = read_list_file(coco_blacklist_path)

    objects365_val_path = os.path.join(objects365_path, "objects365_val_ids.txt")
    objects365_val = read_list_file(objects365_val_path)

    objects365_cctv_val_path = os.path.join(
        objects365_path, "objects365_val-cctv_v1.txt"
    )
    objects365_cctv_val = read_list_file(objects365_cctv_val_path)

    # blacklist2 = read_list_file(datasets_path + "/blacklist2.txt")

    def filter_id(image_id):
        if image_id < args.min or image_id > args.max:
            return False
        if image_id not in objects365_val:
            return False
        if image_id in objects365_cctv_val:
            return False
        # if image_id in coco_blacklist:
        #  return False
        # if image_id not in blacklist2:
        #  return False

        return True

    images = defaultdict(list)
    for img in val_json["images"]:
        img_id = img["id"]
        if not filter_id(img_id):
            continue

        images[img_id] = {
            "width": img["width"],
            "height": img["height"],
            "is_crowd": False,
        }

    whitelist = set()
    people_crowd_ids = set()
    annotations = defaultdict(list)
    for ann in val_json["annotations"]:
        img_id = ann["image_id"]
        category_id = ann["category_id"] - 1
        if ann["iscrowd"] and category_id == 0:
            people_crowd_ids.add(img_id)
            # continue
        if category_id != 0 and category_id != 99:
            continue
        if not filter_id(img_id):
            continue
        if category_id == 99:
            whitelist.add(img_id)

        annotations[img_id].append(
            {
                "category_id": ann["category_id"],
                "bbox": ann["bbox"],
                "area": ann["area"],
            }
        )

    # Free memory.
    # train_json = []
    val_json = []

    samples = []
    filtered_images = []
    ids = []

    for img_id, anns in annotations.items():
        if img_id in people_crowd_ids:
            continue
        if img_id not in whitelist:
            continue

        img = images[img_id]
        width = img["width"]
        height = img["height"]

        path = os.path.join(image_path, str(img_id) + ".jpg")

        # sat = get_saturation(path)
        # if sat > 0.06:
        #  continue

        ids.append(img_id)
        filtered_images.append(path)

        sample = fo.Sample(
            filepath=path,
            img_id=img_id,
        )

        boxes = []
        for ann in anns:
            label = str(ann["category_id"])
            boxes.append(
                parse_bbox(
                    width=width,
                    height=height,
                    label=label,
                    bbox=ann["bbox"],
                )
            )

        sample["bbox"] = fo.Detections(detections=boxes)
        samples.append(sample)

    # write_list_file(ids, "./temp.txt")

    dataset = fo.Dataset.from_images(filtered_images)
    dataset.add_samples(samples)
    view = dataset.sort_by("img_id")
    # view = dataset.sort_by("area", reverse=True)
    # view = dataset.sort_by("sat")

    # Free memory.
    train_json = []
    val_json = []
    images = {}
    annotations = {}
    filtered_images = []

    session = fo.launch_app(dataset=dataset, port=6006, view=view)
    session.wait(-1)


if __name__ == "__main__":
    main()
