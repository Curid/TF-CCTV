import os
import json
import argparse
from collections import defaultdict
import numpy as np
from skimage import measure
from skimage.io import imread
from skimage.color import rgb2hsv
import fiftyone as fo
import fiftyone.core.labels as fol
import fiftyone.core.utils as fou

mask_utils = fou.lazy_import(
    "pycocotools.mask", callback=lambda: fou.ensure_import("pycocotools")
)


def _mask_to_polygons(mask):
    # Pad mask to close contours of shapes which start and end at an edge
    padded_mask = np.pad(mask, pad_width=1, mode="constant", constant_values=0)

    contours = measure.find_contours(padded_mask, 0.5)
    contours = [c - 1 for c in contours]  # undo padding

    polygons = []
    for contour in contours:
        contour = measure.approximate_polygon(contour, 2)
        if len(contour) < 3:
            continue

        contour = np.flip(contour, axis=1)
        segmentation = contour.ravel().tolist()

        # After padding and subtracting 1 there may be -0.5 points
        segmentation = [0 if i < 0 else i for i in segmentation]

        polygons.append(segmentation)

    return polygons


def _pairwise(x):
    y = iter(x)
    return zip(y, y)


def _get_polygons_for_segmentation(width, height, segmentation):
    # Convert to [[x1, y1, x2, y2, ...]] polygons
    if isinstance(segmentation, list):
        abs_points = segmentation
    else:
        rle = mask_utils.frPyObjects(segmentation, height, width)
        mask = mask_utils.decode(rle)
        abs_points = _mask_to_polygons(mask)

    # Convert to [[(x1, y1), (x2, y2), ...]] in relative coordinates
    rel_points = []
    for apoints in abs_points:
        rel_points.append([(x / width, y / height) for x, y in _pairwise(apoints)])

        return rel_points


def parse_segmentation(width, height, label, segmentation):
    points = _get_polygons_for_segmentation(width, height, segmentation)

    return fol.Polyline(
        label=label,
        points=points,
        closed=False,
        filled=False,
    )


def parse_bbox(width, height, label, bbox):  # pylint: disable=unused-argument
    x, y, w, h = bbox
    bounding_box = [x / width, y / height, w / width, h / height]

    return fo.Detection(
        # label=label,
        bounding_box=bounding_box,
    )


def read_list_file(path: str) -> set:
    raw_file = open(path, "r")
    lines = raw_file.readlines()
    return set([int(i) for i in lines])


def write_list_file(items, path):
    items.sort()
    f = open(path, "w")
    for item in items:
        f.writelines(str(item) + "\n")
    f.close()


def get_largest_area(anns, w, h):
    size = w * h
    largest = 0
    for ann in anns:
        area = ann["area"] / size * 100
        if area > largest:
            largest = area
    return largest


def get_saturation(path):
    img = imread(path)

    is_grayscale = img.ndim == 2
    if is_grayscale:
        return 0

    hsv = rgb2hsv(img)
    avg_hsv_per_row = np.average(hsv, axis=0)
    avg_hsv = np.average(avg_hsv_per_row, axis=0)
    return avg_hsv[1]


def main():  # pylint: disable=too-many-locals
    parser = argparse.ArgumentParser()
    parser.add_argument("--min", help="Minimum image ID", type=int, required=True)
    parser.add_argument("--max", help="Maximum image ID", type=int, required=True)
    args = parser.parse_args()

    dir_path = os.path.dirname(os.path.realpath(__file__))
    home_path = os.path.dirname(os.path.dirname(dir_path))

    datasets_path = os.path.join(home_path, "datasets")
    coco_path = os.path.join(datasets_path, "coco2017")
    image_path = os.path.join(coco_path, "images")

    train_file = os.path.join(coco_path, "instances_train2017.json")
    val_file = os.path.join(coco_path, "instances_val2017.json")

    train_json = json.load(open(train_file, "r"))
    val_json = json.load(open(val_file, "r"))

    # Free memory.
    train_file = []
    val_file = []

    coco_blacklist_path = os.path.join(datasets_path, "mscoco_blacklist_ids.txt")
    coco_blacklist = read_list_file(coco_blacklist_path)

    coco_minival_path = os.path.join(datasets_path, "mscoco_minival_ids.txt")
    coco_minival = read_list_file(coco_minival_path)

    # blacklist2 = read_list_file(datasets_path + "/blacklist2.txt")

    def filter_id(image_id):
        if image_id < args.min or image_id > args.max:
            return False
        if image_id in coco_minival:
            return False
        if image_id in coco_blacklist:
            return False
        # if image_id not in blacklist2:
        #  return False

        return True

    images = defaultdict(list)
    for _images in [train_json["images"], val_json["images"]]:
        for img in _images:
            img_id = img["id"]
            if not filter_id(img_id):
                continue

            images[img_id] = {
                "width": img["width"],
                "height": img["height"],
                "file_name": img["file_name"],
            }

    annotations = defaultdict(list)
    for _annotations in [train_json["annotations"], val_json["annotations"]]:
        for ann in _annotations:
            img_id = ann["image_id"]
            if ann["category_id"] != 1:  # and ann["category_id"] != 77:
                continue
            if ann["iscrowd"]:
                continue
            if not filter_id(img_id):
                continue

            annotations[img_id].append(
                {
                    "category_id": ann["category_id"],
                    "segmentation": ann["segmentation"],
                    "bbox": ann["bbox"],
                    "area": ann["area"],
                }
            )

    # Free memory.
    train_json = []
    val_json = []

    samples = []
    filtered_images = []
    ids = []

    for img_id, anns in annotations.items():
        img = images[img_id]
        width = img["width"]
        height = img["height"]
        # area = get_largest_area(anns, width, height)

        path = os.path.join(image_path, img["file_name"])

        # sat = get_saturation(path)
        # if sat > 0.06:
        #  continue

        ids.append(img_id)
        filtered_images.append(path)

        sample = fo.Sample(
            filepath=path,
            img_id=img_id,
            # sat=sat,
        )

        polylines = []
        boxes = []
        for ann in anns:
            label = str(ann["category_id"])
            polylines.append(
                parse_segmentation(
                    width=width,
                    height=height,
                    label=label,
                    segmentation=ann["segmentation"],
                )
            )
            boxes.append(
                parse_bbox(
                    width=width,
                    height=height,
                    label=label,
                    bbox=ann["bbox"],
                )
            )

        # sample["seg"] = fo.Polylines(polylines=polylines)
        sample["bbox"] = fo.Detections(detections=boxes)
        samples.append(sample)

    # write_list_file(ids, "./temp.txt")

    dataset = fo.Dataset.from_images(filtered_images)
    dataset.add_samples(samples)
    view = dataset.sort_by("img_id")
    # view = dataset.sort_by("area", reverse=True)
    # view = dataset.sort_by("sat")

    # Free memory.
    train_json = []
    val_json = []
    images = {}
    annotations = {}
    filtered_images = []

    session = fo.launch_app(dataset=dataset, port=6006, view=view)
    session.wait(-1)


if __name__ == "__main__":
    main()
