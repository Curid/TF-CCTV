from od.protos.anchor_generator_pb2 import AnchorGenerator
from od.protos.argmax_matcher_pb2 import ArgMaxMatcher
from od.protos.box_coder_pb2 import BoxCoder
from od.protos.box_predictor_pb2 import (
    BoxPredictor,
    ConvolutionalBoxPredictor,
)
from od.protos.eval_pb2 import EvalConfig
from od.protos.faster_rcnn_box_coder_pb2 import FasterRcnnBoxCoder
from od.protos.graph_rewriter_pb2 import GraphRewriter, Quantization
from od.protos.hyperparams_pb2 import (
    BatchNorm,
    Hyperparams,
    Initializer,
    L2Regularizer,
    RandomNormalInitializer,
    Regularizer,
    TruncatedNormalInitializer,
)
from od.protos.image_resizer_pb2 import FixedShapeResizer, ImageResizer
from od.protos.input_reader_pb2 import InputReader, TFRecordInputReader
from od.protos.losses_pb2 import (
    ClassificationLoss,
    LocalizationLoss,
    Loss,
    SigmoidFocalClassificationLoss,
    WeightedSmoothL1LocalizationLoss,
)
from od.protos.matcher_pb2 import Matcher
from od.protos.model_pb2 import DetectionModel
from od.protos.optimizer_pb2 import (
    CosineDecayLearningRate,
    LearningRate,
    MomentumOptimizer,
    Optimizer,
)
from od.protos.post_processing_pb2 import (
    BatchNonMaxSuppression,
    PostProcessing,
)
from od.protos.preprocessor_pb2 import (
    PreprocessingStep,
    RandomHorizontalFlip,
    SSDRandomCrop,
)
from od.protos.region_similarity_calculator_pb2 import (
    IouSimilarity,
    RegionSimilarityCalculator,
)
from od.protos.ssd_anchor_generator_pb2 import SsdAnchorGenerator
from od.protos.ssd_pb2 import Ssd, SsdFeatureExtractor
from od.protos.train_pb2 import TrainConfig


def mobiledet_model(
    num_classes=90,
    width=320,
    height=320,
    anchor_num_layers=6,
    anchor_min_scale=0.2,
    anchor_max_scale=0.95,
    anchor_aspect_ratios=[1.0, 2.0, 0.5, 3.0, 0.3333],
):
    return DetectionModel(
        ssd=Ssd(
            num_classes=num_classes,
            image_resizer=ImageResizer(
                fixed_shape_resizer=FixedShapeResizer(
                    width=width,
                    height=height,
                ),
            ),
            feature_extractor=SsdFeatureExtractor(
                type="ssd_mobiledet_edgetpu",
                depth_multiplier=1.0,
                min_depth=16,
                conv_hyperparams=Hyperparams(
                    regularizer=Regularizer(
                        l2_regularizer=L2Regularizer(
                            weight=0.00004,
                        ),
                    ),
                    initializer=Initializer(
                        truncated_normal_initializer=TruncatedNormalInitializer(
                            stddev=0.03,
                            mean=0.0,
                        ),
                    ),
                    activation="RELU_6",
                    batch_norm=BatchNorm(
                        decay=0.97,
                        center=True,
                        scale=True,
                        epsilon=0.001,
                        train=True,
                    ),
                ),
                use_depthwise=True,
                override_base_feature_extractor_hyperparams=False,
            ),
            box_coder=BoxCoder(
                faster_rcnn_box_coder=FasterRcnnBoxCoder(
                    y_scale=10.0,
                    x_scale=10.0,
                    height_scale=5.0,
                    width_scale=5.0,
                ),
            ),
            matcher=Matcher(
                argmax_matcher=ArgMaxMatcher(
                    matched_threshold=0.5,
                    unmatched_threshold=0.5,
                    ignore_thresholds=False,
                    negatives_lower_than_unmatched=True,
                    force_match_for_each_row=True,
                    use_matmul_gather=True,
                ),
            ),
            similarity_calculator=RegionSimilarityCalculator(
                iou_similarity=IouSimilarity(),
            ),
            box_predictor=BoxPredictor(
                convolutional_box_predictor=ConvolutionalBoxPredictor(
                    conv_hyperparams=Hyperparams(
                        regularizer=Regularizer(
                            l2_regularizer=L2Regularizer(
                                weight=0.00004,
                            ),
                        ),
                        initializer=Initializer(
                            random_normal_initializer=RandomNormalInitializer(
                                mean=0.0,
                                stddev=0.03,
                            ),
                        ),
                        activation="RELU_6",
                        batch_norm=BatchNorm(
                            scale=True,
                            decay=0.97,
                            center=True,
                            epsilon=0.001,
                            train=True,
                        ),
                    ),
                    min_depth=0,
                    max_depth=0,
                    num_layers_before_predictor=0,
                    use_dropout=False,
                    dropout_keep_probability=0.8,
                    kernel_size=3,
                    use_depthwise=True,
                    box_code_size=4,
                    apply_sigmoid_to_scores=False,
                    class_prediction_bias_init=-4.6,
                ),
            ),
            anchor_generator=AnchorGenerator(
                ssd_anchor_generator=SsdAnchorGenerator(
                    num_layers=anchor_num_layers,
                    min_scale=anchor_min_scale,
                    max_scale=anchor_max_scale,
                    aspect_ratios=anchor_aspect_ratios,
                ),
            ),
            post_processing=PostProcessing(
                batch_non_max_suppression=BatchNonMaxSuppression(
                    score_threshold=1e-8,
                    iou_threshold=0.6,
                    max_detections_per_class=100,
                    max_total_detections=100,
                    use_static_shapes=True,
                ),
                score_converter="SIGMOID",
            ),
            normalize_loss_by_num_matches=True,
            loss=Loss(
                localization_loss=LocalizationLoss(
                    weighted_smooth_l1=WeightedSmoothL1LocalizationLoss(
                        delta=1.0,
                    ),
                ),
                classification_loss=ClassificationLoss(
                    weighted_sigmoid_focal=SigmoidFocalClassificationLoss(
                        gamma=2.0,
                        alpha=0.75,
                    ),
                ),
                classification_weight=1.0,
                localization_weight=1.0,
            ),
            encode_background_as_zeros=True,
            normalize_loc_loss_by_codesize=True,
            inplace_batchnorm_update=True,
            freeze_batchnorm=False,
        ),
    )


def mobiledet_train_config(
    batch_size=512,
    total_steps=400000,
    learning_rate_base=0.8,
    warmup_learning_rate=0.13333,
    warmup_steps=2000,
    fine_tune=None,
):
    return TrainConfig(
        batch_size=batch_size,
        data_augmentation_options=[
            PreprocessingStep(
                random_horizontal_flip=RandomHorizontalFlip(),
            ),
            PreprocessingStep(
                ssd_random_crop=SSDRandomCrop(),
            ),
        ],
        sync_replicas=True,
        optimizer=Optimizer(
            momentum_optimizer=MomentumOptimizer(
                learning_rate=LearningRate(
                    cosine_decay_learning_rate=CosineDecayLearningRate(
                        learning_rate_base=learning_rate_base,
                        total_steps=total_steps,
                        warmup_learning_rate=warmup_learning_rate,
                        warmup_steps=warmup_steps,
                    ),
                ),
                momentum_optimizer_value=0.9,
            ),
            use_moving_average=False,
        ),
        fine_tune_checkpoint=fine_tune,
        num_steps=total_steps,
        startup_delay_steps=0.0,
        replicas_to_aggregate=32,
        max_number_of_boxes=100,
        unpad_groundtruth_tensors=False,
    )


def train_input_config(input_path, label_map_path="./labels/person.pbtxt"):
    return InputReader(
        label_map_path=label_map_path,
        tf_record_input_reader=TFRecordInputReader(
            input_path=[input_path],
        ),
    )


def coco_eval_config(num_examples=8000):
    return EvalConfig(
        num_examples=num_examples,
        metrics_set=["coco_detection_metrics"],
        use_moving_averages=False,
    )


def eval_input_config(label_map_path, input_path):
    return InputReader(
        label_map_path=label_map_path,
        shuffle=False,
        num_epochs=1,
        tf_record_input_reader=TFRecordInputReader(
            input_path=[input_path],
        ),
    )


def default_graph_rewriter():
    return GraphRewriter(
        quantization=Quantization(
            delay=0,
            weight_bits=8,
            activation_bits=8,
        ),
    )
