# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import os
import tempfile

import numpy as np
import tensorflow.compat.v1 as tf
from tensorflow.compat.v1 import estimator as tf_estimator
from tensorflow.python.data.ops import readers

from od import (
    box_list,
    box_list_ops,
    coco_tools,
    config_util,
    dataset_builder,
    eval_util,
    image_resizer_builder,
    inputs,
    label_map_util,
    model_builder,
    model_lib,
    object_detection_evaluation,
    post_processing,
    shape_utils,
    tf_example_decoder,
)
from od import ops as util_ops
from od import standard_fields as fields
from od.protos import input_reader_pb2
from od.protos.image_resizer_pb2 import FixedShapeResizer, ImageResizer


def evaluate(configs, checkpoint_dir, feature_extractor_class=None):
    model_dir = tempfile.gettempdir() + "/train"
    run_config = tf_estimator.RunConfig(model_dir=model_dir)

    configs["eval_input_config"] = configs["eval_input_configs"][0]
    model_config = configs["model"]

    model_fn = model_lib.create_model_fn2(
        model_builder.build,
        configs,
        feature_extractor_class,
    )
    estimator = tf_estimator.Estimator(model_fn=model_fn, config=run_config)

    def input_fn(params=None):
        return inputs.eval_input(
            configs["eval_config"],
            configs["eval_input_configs"][0],
            model_config,
            params=params,
        )

    checkpoint_path = tf.train.latest_checkpoint(checkpoint_dir)
    estimator.evaluate(input_fn, checkpoint_path=checkpoint_path)


def evaluate_tflite(
    label_map_path,
    tflite_path,
    records_path,
    num_classes,
    width,
    height,
    model_format,
):
    tf.enable_eager_execution()

    model_fn = create_model_fn(label_map_path, num_classes, tflite_path, model_format)
    estimator = tf_estimator.Estimator(model_fn=model_fn)

    def input_fn(params):
        return eval_input(
            label_map_path,
            records_path,
            num_classes,
            width,
            height,
        )

    return estimator.evaluate(input_fn)


def eval_input(
    label_map_path,
    input_path,
    num_classes,
    width,
    height,
    batch_size=1,
    max_number_of_boxes=100,
    include_source_id=False,
    input_type=input_reader_pb2.InputType.Value("TF_EXAMPLE"),
):
    """Returns `features` and `labels` tensor dictionaries for evaluation.

    Returns:
      A tf.data.Dataset that holds (features, labels) tuple.

      features: Dictionary of feature tensors.
        features[fields.InputDataFields.image] is a [1, H, W, C] float32 tensor
          with preprocessed images.
        features[HASH_KEY] is a [1] int32 tensor representing unique
          identifiers for the images.
        features[fields.InputDataFields.true_image_shape] is a [1, 3]
          int32 tensor representing the true image shapes, as preprocessed
          images could be padded.
        features[fields.InputDataFields.original_image] is a [1, H', W', C]
          float32 tensor with the original image.
      labels: Dictionary of groundtruth tensors.
        labels[fields.InputDataFields.groundtruth_boxes] is a [1, num_boxes, 4]
          float32 tensor containing the corners of the groundtruth boxes.
        labels[fields.InputDataFields.groundtruth_classes] is a
          [num_boxes, num_classes] float32 one-hot tensor of classes.
        labels[fields.InputDataFields.groundtruth_area] is a [1, num_boxes]
          float32 tensor containing object areas.
    """

    if input_type != (input_reader_pb2.InputType.Value("TF_EXAMPLE")):
        raise TypeError("not implemented")

    image_resizer_config = ImageResizer(
        fixed_shape_resizer=FixedShapeResizer(
            width=width,
            height=height,
        )
    )

    def feature_extractor_preprocess(resized_inputs):
        return (2.0 / 255.0) * resized_inputs - 1.0

    image_resizer_fn = image_resizer_builder.build(image_resizer_config)

    def model_preprocess_fn(inputs):
        with tf.name_scope("Preprocessor"):
            normalized_inputs = feature_extractor_preprocess(inputs)
            return shape_utils.resize_images_and_return_shapes(
                normalized_inputs, image_resizer_fn
            )

    def transform_and_pad_input_data_fn(tensor_dict):
        """Combines transform and pad operation."""

        image_resizer_fn = image_resizer_builder.build(image_resizer_config)

        def transform_data_fn(*fargs, **fkeywords):
            return transform_input_data(
                *fargs,
                model_preprocess_fn=model_preprocess_fn,
                image_resizer_fn=image_resizer_fn,
                num_classes=num_classes,
                **fkeywords,
            )

        tensor_dict = inputs.pad_input_data_to_static_shapes(
            tensor_dict=transform_data_fn(tensor_dict),
            max_num_boxes=max_number_of_boxes,
            num_classes=num_classes,
            spatial_image_shape=config_util.get_spatial_image_size(
                image_resizer_config
            ),
        )
        return (
            inputs._get_features_dict(tensor_dict, include_source_id),
            inputs._get_labels_dict(tensor_dict),
        )

    dataset = build_dataset(
        label_map_path,
        input_path,
        batch_size=batch_size,
        transform_input_data_fn=transform_and_pad_input_data_fn,
    )
    return dataset


def transform_input_data(
    tensor_dict,
    model_preprocess_fn,
    image_resizer_fn,
    num_classes,
):
    """A single function that is responsible for all input data transformations.

    Data transformation functions are applied in the following order.
    1. model_preprocess_fn: applied only on image tensor in tensor_dict.
    2. image_resizer_fn: applied on original image and instance mask tensor in
       tensor_dict.
    3. one_hot_encoding: applied to classes tensor in tensor_dict.

    Args:
      tensor_dict: dictionary containing input tensors keyed by
        fields.InputDataFields.
      model_preprocess_fn: model's preprocess function to apply on image tensor.
        This function must take in a 4-D float tensor and return a 4-D preprocess
        float tensor and a tensor containing the true image shape.
      image_resizer_fn: image resizer function to apply on groundtruth instance
        `masks. This function must take a 3-D float tensor of an image and a 3-D
        tensor of instance masks and return a resized version of these along with
        the true shapes.
      num_classes: number of max classes to one-hot (or k-hot) encode the class
        labels.

    Returns:
      A dictionary keyed by fields.InputDataFields containing the tensors obtained
      after applying all the transformations.

    Raises:
      KeyError: If both groundtruth_labeled_classes and groundtruth_image_classes
        are provided by the decoder in tensor_dict since both fields are
        considered to contain the same information.
    """
    out_tensor_dict = tensor_dict.copy()
    input_fields = fields.InputDataFields

    if (
        input_fields.groundtruth_labeled_classes in out_tensor_dict
        and input_fields.groundtruth_image_classes in out_tensor_dict
    ):
        raise KeyError(
            "groundtruth_labeled_classes and groundtruth_image_classes"
            "are provided by the decoder, but only one should be set."
        )

    if input_fields.groundtruth_boxes in out_tensor_dict:
        out_tensor_dict = util_ops.filter_groundtruth_with_nan_box_coordinates(
            out_tensor_dict
        )
        out_tensor_dict = util_ops.filter_unrecognized_classes(out_tensor_dict)

    out_tensor_dict[input_fields.original_image] = tf.cast(
        image_resizer_fn(out_tensor_dict[input_fields.image], None)[0], tf.uint8
    )

    # Unsupported type
    # Apply model preprocessing ops and resize instance masks.
    image = out_tensor_dict[input_fields.image]
    preprocessed_resized_image, true_image_shape = model_preprocess_fn(
        tf.expand_dims(
            tf.cast(image, dtype=tf.float32),
            axis=0,
        ),
    )

    preprocessed_shape = tf.shape(preprocessed_resized_image)
    new_height, new_width = preprocessed_shape[1], preprocessed_shape[2]

    im_box = tf.stack(
        [
            0.0,
            0.0,
            tf.to_float(new_height) / tf.to_float(true_image_shape[0, 0]),
            tf.to_float(new_width) / tf.to_float(true_image_shape[0, 1]),
        ]
    )

    if input_fields.groundtruth_boxes in tensor_dict:
        bboxes = out_tensor_dict[input_fields.groundtruth_boxes]
        boxlist = box_list.BoxList(bboxes)
        realigned_bboxes = box_list_ops.change_coordinate_frame(boxlist, im_box)

        realigned_boxes_tensor = realigned_bboxes.get()
        valid_boxes_tensor = inputs.assert_or_prune_invalid_boxes(
            realigned_boxes_tensor
        )
        out_tensor_dict[input_fields.groundtruth_boxes] = valid_boxes_tensor

    # TypeError
    out_tensor_dict[input_fields.image] = tf.squeeze(preprocessed_resized_image, axis=0)
    # KeyError: true_image_shape
    out_tensor_dict[input_fields.true_image_shape] = tf.squeeze(
        true_image_shape, axis=0
    )

    zero_indexed_groundtruth_classes = (
        out_tensor_dict[input_fields.groundtruth_classes] - inputs._LABEL_OFFSET
    )
    out_tensor_dict[input_fields.groundtruth_classes] = tf.one_hot(
        zero_indexed_groundtruth_classes,
        num_classes,
    )

    # inputs.py:656
    if input_fields.groundtruth_boxes in out_tensor_dict:
        out_tensor_dict[input_fields.num_groundtruth_boxes] = tf.shape(
            out_tensor_dict[input_fields.groundtruth_boxes]
        )[0]

    return out_tensor_dict


def build_dataset(
    label_map_path,
    input_path,
    batch_size=None,
    transform_input_data_fn=None,
    load_instance_masks=False,
    load_multiclass_scores=False,
    load_context_features=False,
    mask_type=1,
    use_display_name=False,
    num_keypoints=0,
    expand_labels_hierarchy=False,
    load_track_id=False,
    load_keypoint_depth_features=False,
    num_parallel_batches=8,
    num_parallel_map_calls=64,
    drop_remainder=True,
    num_prefetch_batches=2,
):
    """Builds a tf.data.Dataset.

    Builds a tf.data.Dataset by applying the `transform_input_data_fn` on all
    records. Applies a padded batch to the resulting dataset.

    Args:
      batch_size: Batch size. If batch size is None, no batching is performed.
      transform_input_data_fn: Function to apply transformation to all records,
        or None if no extra decoding is required.

    Returns:
      A tf.data.Dataset.
    """

    decoder = tf_example_decoder.TfExampleDecoder(
        load_instance_masks=load_instance_masks,
        load_multiclass_scores=load_multiclass_scores,
        load_context_features=load_context_features,
        instance_mask_type=mask_type,
        label_map_proto_file=label_map_path,
        use_display_name=use_display_name,
        num_keypoints=num_keypoints,
        expand_hierarchy_labels=expand_labels_hierarchy,
        load_track_id=load_track_id,
        load_keypoint_depth_features=load_keypoint_depth_features,
    )

    def dataset_map_fn(dataset, fn_to_map, batch_size=None):
        """Handles whether or not to use the legacy map function.

        Args:
          dataset: A tf.Dataset.
          fn_to_map: The function to be mapped for that dataset.
          batch_size: Batch size. If batch size is None, no batching is performed.

        Returns:
          A tf.data.Dataset mapped with fn_to_map.
        """
        if hasattr(dataset, "map_with_legacy_function"):
            if batch_size:
                num_parallel_calls = batch_size * (num_parallel_batches)
            else:
                num_parallel_calls = num_parallel_map_calls
            dataset = dataset.map_with_legacy_function(
                fn_to_map, num_parallel_calls=num_parallel_calls
            )
        else:
            dataset = dataset.map(fn_to_map, tf.data.experimental.AUTOTUNE)
        return dataset

    shard_fn = dataset_builder.shard_function_for_context(None)
    dataset = read_dataset(
        input_path,
        filename_shard_fn=shard_fn,
    )

    dataset = dataset_map_fn(dataset, decoder.decode, batch_size)
    if transform_input_data_fn is not None:
        dataset = dataset_map_fn(dataset, transform_input_data_fn, batch_size)
    if batch_size:
        dataset = dataset.batch(batch_size, drop_remainder=drop_remainder)
    dataset = dataset.prefetch(num_prefetch_batches)
    return dataset


def read_dataset(
    input_files,
    num_readers=64,
    filename_shard_fn=None,
    shuffle=True,
    filenames_shuffle_buffer_size=100,
    read_block_length=32,
    shuffle_buffer_size=100,
):
    """Reads a dataset, and handles repetition and shuffling.

    Args:
      input_files: A list of file paths to read.
      num_readers: Number of readers to use.
      filename_shard_fn: optional, A function used to shard filenames across
        replicas. This function takes as input a TF dataset of filenames and is
        expected to return its sharded version. It is useful when the dataset is
        being loaded on one of possibly many replicas and we want to evenly shard
        the files between the replicas.

    Returns:
      A tf.data.Dataset of (undecoded) tf-records.

    Raises:
      RuntimeError: If no files are found at the supplied path(s).
    """
    filenames = tf.gfile.Glob(input_files)
    tf.logging.info("Reading record datasets for input file: %s" % input_files)
    tf.logging.info("Number of filenames to read: %s" % len(filenames))
    if not filenames:
        raise RuntimeError(
            "Did not find any input files matching the glob pattern " "{}".format(
                input_files
            )
        )
    if num_readers > len(filenames):
        num_readers = len(filenames)
        tf.logging.warning(
            "num_readers has been reduced to %d to match input file "
            "shards." % num_readers
        )
    filename_dataset = tf.data.Dataset.from_tensor_slices(filenames)
    if shuffle:
        filename_dataset = filename_dataset.shuffle(filenames_shuffle_buffer_size)
    elif num_readers > 1:
        tf.logging.warning(
            "`shuffle` is false, but the input data stream is "
            "still slightly shuffled since `num_readers` > 1."
        )
    if filename_shard_fn:
        filename_dataset = filename_shard_fn(filename_dataset)

    records_dataset = filename_dataset.apply(
        parallel_interleave(
            cycle_length=num_readers, block_length=read_block_length, sloppy=shuffle
        )
    )

    if shuffle:
        records_dataset = records_dataset.shuffle(shuffle_buffer_size)
    return records_dataset


def parallel_interleave(cycle_length, block_length=1, sloppy=False):
    def map_func(*fargs, **fkeywords):
        return tf.data.TFRecordDataset(*fargs, buffer_size=8 * 1000 * 1000, **fkeywords)

    def _apply_fn(dataset):
        return readers.ParallelInterleaveDataset(
            dataset, map_func, cycle_length, block_length, sloppy, None, None
        )

    return _apply_fn


def create_model_fn(
    label_map_path,
    num_classes,
    tflite_path,
    model_format,
    max_number_of_boxes=100,
):
    """Creates a model function for `Estimator`.

    Returns:
      `model_fn` for `Estimator`.
    """

    def model_fn(features, labels, mode, params=None):
        """Constructs the object detection model.

        Args:
          features: Dictionary of feature tensors, returned from `input_fn`.
          labels: Dictionary of groundtruth tensors if mode is TRAIN or EVAL,
            otherwise None.
          mode: Mode key from tf.estimator.ModeKeys.

        Returns:
          An `EstimatorSpec` that encapsulates the model and its serving
            configurations.
        """

        boxes_shape = (
            labels[fields.InputDataFields.groundtruth_boxes].get_shape().as_list()
        )

        unpad_groundtruth_tensors = boxes_shape[1] is not None
        labels = model_lib.unstack_batch(
            labels, unpad_groundtruth_tensors=unpad_groundtruth_tensors
        )

        original_images = features[fields.InputDataFields.original_image]
        groundtruth = prepare_groundtruth(labels, max_number_of_boxes)

        true_image_shapes = tf.slice(
            features[fields.InputDataFields.true_image_shape], [0, 0], [-1, 3]
        )
        original_image_spatial_shapes = features[
            fields.InputDataFields.original_image_spatial_shape
        ]

        eval_dict = result_dict_for_batched_example(
            original_images,
            features[inputs.HASH_KEY],
            groundtruth,
            scale_to_absolute=True,
            original_image_spatial_shapes=original_image_spatial_shapes,
            true_image_shapes=true_image_shapes,
        )

        category_index = label_map_util.create_category_index_from_labelmap(
            label_map_path
        )

        eval_metric_ops = get_eval_metric_ops_for_evaluators(
            list(category_index.values()),
            eval_dict,
            num_classes,
            tflite_path,
            model_format,
        )

        return tf_estimator.EstimatorSpec(
            mode=mode,
            loss=tf.constant(1.0),
            eval_metric_ops=eval_metric_ops,
        )

    return model_fn


def prepare_groundtruth(labels, max_number_of_boxes):
    """
    Args:
      labels: The labels for the training or evaluation inputs.
      max_number_of_boxes: Max number of groundtruth boxes.
    """

    input_data_fields = fields.InputDataFields()

    groundtruth_boxes = tf.stack(labels[input_data_fields.groundtruth_boxes])
    groundtruth_boxes_shape = tf.shape(groundtruth_boxes)

    groundtruth_classes_one_hot = tf.stack(
        labels[input_data_fields.groundtruth_classes]
    )

    label_id_offset = 1  # Applying label id offset (b/63711816)
    groundtruth_classes = (
        tf.argmax(groundtruth_classes_one_hot, axis=2) + label_id_offset
    )
    groundtruth = {
        input_data_fields.groundtruth_boxes: groundtruth_boxes,
        input_data_fields.groundtruth_classes: groundtruth_classes,
    }

    if input_data_fields.groundtruth_is_crowd in labels:
        groundtruth[input_data_fields.groundtruth_is_crowd] = tf.stack(
            labels[input_data_fields.groundtruth_is_crowd]
        )

    groundtruth[input_data_fields.num_groundtruth_boxes] = tf.tile(
        [max_number_of_boxes], multiples=[groundtruth_boxes_shape[0]]
    )

    return groundtruth


def result_dict_for_batched_example(
    original_images,
    keys,
    groundtruth=None,
    scale_to_absolute=False,
    original_image_spatial_shapes=None,
    true_image_shapes=None,
):
    """Merges all detection and groundtruth information for a single example.

    Note that evaluation tools require classes that are 1-indexed, and so this
    function performs the offset. If `class_agnostic` is True, all output classes
    have label 1.
    The groundtruth coordinates of boxes/keypoints in 'groundtruth' dictionary are
    normalized relative to the (potentially padded) input image, while the
    coordinates in 'detection' dictionary are normalized relative to the true
    image shape.

    Args:
      original_images: A single 4D uint8 image tensor of shape [batch_size, H, W, C].
      keys: A [batch_size] string/int tensor with image identifier.
      detection_classes:
      groundtruth: (Optional) Dictionary of groundtruth items, with fields:
        'groundtruth_boxes': [batch_size, max_number_of_boxes, 4] float32 tensor
          of boxes, in normalized coordinates.
        'groundtruth_classes':  [batch_size, max_number_of_boxes] int64 tensor of
          1-indexed classes.
      scale_to_absolute: Boolean indicating whether boxes and keypoints should be
        scaled to absolute coordinates. Note that for IoU based evaluations, it
        does not matter whether boxes are expressed in absolute or relative
        coordinates. Default False.
      original_image_spatial_shapes: A 2D int32 tensor of shape [batch_size, 2]
        used to resize the image. When set to None, the image size is retained.
      true_image_shapes: A 2D int32 tensor of shape [batch_size, 3]
        containing the size of the unpadded original_image.
      max_gt_boxes: [batch_size] tensor representing the maximum number of
        groundtruth boxes to pad.
      label_id_offset: offset for class ids.

    Returns:
      A dictionary with:
      'original_image': A [batch_size, H, W, C] uint8 image tensor.
      'original_image_spatial_shape': A [batch_size, 2] tensor containing the
        original image sizes.
      'key': A [batch_size] string tensor with image identifier.
      'detection_classes': [batch_size, max_detections] int64 tensor of 1-indexed
        classes.
      'detection_surface_coords': [batch_size, max_detection, H, W, 2] float32
        tensor with normalized surface coordinates (e.g. DensePose UV
        coordinates). (Optional)
      'groundtruth_boxes': [batch_size, num_boxes, 4] float32 tensor of boxes, in
        normalized or absolute coordinates, depending on the value of
        `scale_to_absolute`. (Optional)
      'groundtruth_classes': [batch_size, num_boxes] int64 tensor of 1-indexed
        classes. (Optional)

    Raises:
      ValueError: if original_image_spatial_shape is not 2D int32 tensor of shape
        [2].
    """
    input_data_fields = fields.InputDataFields
    if original_image_spatial_shapes is None:
        original_image_spatial_shapes = tf.tile(
            tf.expand_dims(tf.shape(original_images)[1:3], axis=0),
            multiples=[tf.shape(original_images)[0], 1],
        )
    else:
        if (
            len(original_image_spatial_shapes.shape) != 2
            and original_image_spatial_shapes.shape[1] != 2
        ):
            raise ValueError(
                "`original_image_spatial_shape` should be a 2D tensor of shape "
                "[batch_size, 2]."
            )

    if true_image_shapes is None:
        true_image_shapes = tf.tile(
            tf.expand_dims(tf.shape(original_images)[1:4], axis=0),
            multiples=[tf.shape(original_images)[0], 1],
        )
    else:
        if len(true_image_shapes.shape) != 2 and true_image_shapes.shape[1] != 3:
            raise ValueError(
                "`true_image_shapes` should be a 2D tensor of " "shape [batch_size, 3]."
            )

    output_dict = {
        input_data_fields.original_image: original_images,
        input_data_fields.key: keys,
        input_data_fields.original_image_spatial_shape: (original_image_spatial_shapes),
        input_data_fields.true_image_shape: true_image_shapes,
    }

    if groundtruth:
        output_dict.update(groundtruth)

        image_shape = tf.cast(tf.shape(original_images), tf.float32)
        image_height, image_width = image_shape[1], image_shape[2]

        def _scale_box_to_normalized_true_image(args):
            """Scale the box coordinates to be relative to the true image shape."""
            boxes, true_image_shape = args
            true_image_shape = tf.cast(true_image_shape, tf.float32)
            true_height, true_width = true_image_shape[0], true_image_shape[1]
            normalized_window = tf.stack(
                [0.0, 0.0, true_height / image_height, true_width / image_width]
            )
            return box_list_ops.change_coordinate_frame(
                box_list.BoxList(boxes), normalized_window
            ).get()

        groundtruth_boxes = groundtruth[input_data_fields.groundtruth_boxes]
        groundtruth_boxes = shape_utils.static_or_dynamic_map_fn(
            _scale_box_to_normalized_true_image,
            elems=[groundtruth_boxes, true_image_shapes],
            dtype=tf.float32,
        )
        output_dict[input_data_fields.groundtruth_boxes] = groundtruth_boxes

        if scale_to_absolute:
            groundtruth_boxes = output_dict[input_data_fields.groundtruth_boxes]
            output_dict[
                input_data_fields.groundtruth_boxes
            ] = shape_utils.static_or_dynamic_map_fn(
                eval_util._scale_box_to_absolute,
                elems=[groundtruth_boxes, original_image_spatial_shapes],
                dtype=tf.float32,
            )

    return output_dict


def get_eval_metric_ops_for_evaluators(
    categories,
    eval_dict,
    num_classes,
    tflite_path,
    model_format,
    include_metrics_per_category=False,
    all_metrics_per_category=False,
    skip_predictions_for_unlabeled_class=False,
):
    """Returns eval metrics ops to use with `tf.estimator.EstimatorSpec`.

    Args:
      categories: A list of dicts, each of which has the following keys -
          'id': (required) an integer id uniquely identifying this category.
          'name': (required) string representing category name e.g., 'cat', 'dog'.
      eval_dict: An evaluation dictionary, returned from
        result_dict_for_single_example().

    Returns:
      A dictionary of metric names to tuple of value_op and update_op that can be
      used as eval metric ops in tf.EstimatorSpec.
    """

    evaluator = CocoDetectionEvaluator(
        categories,
        include_metrics_per_category=include_metrics_per_category,
        all_metrics_per_category=all_metrics_per_category,
        skip_predictions_for_unlabeled_class=skip_predictions_for_unlabeled_class,
    )
    return evaluator.get_estimator_eval_metric_ops(
        eval_dict, num_classes, tflite_path, model_format
    )


class CocoDetectionEvaluator(object_detection_evaluation.DetectionEvaluator):
    """Class to evaluate COCO detection metrics."""

    def __init__(
        self,
        categories,
        include_metrics_per_category=False,
        all_metrics_per_category=False,
        skip_predictions_for_unlabeled_class=False,
    ):
        """Constructor.

        Args:
          categories: A list of dicts, each of which has the following keys -
            'id': (required) an integer id uniquely identifying this category.
            'name': (required) string representing category name e.g., 'cat', 'dog'.
          include_metrics_per_category: If True, include metrics for each category.
          all_metrics_per_category: Whether to include all the summary metrics for
            each category in per_category_ap. Be careful with setting it to true if
            you have more than handful of categories, because it will pollute
            your mldash.
          skip_predictions_for_unlabeled_class: Skip predictions that do not match
            with the labeled classes for the image.
        """
        super(CocoDetectionEvaluator, self).__init__(categories)
        # _image_ids is a dictionary that maps unique image ids to Booleans which
        # indicate whether a corresponding detection has been added.
        self._image_ids = {}
        self._groundtruth_list = []
        self._detection_boxes_list = []
        self._category_id_set = set([cat["id"] for cat in self._categories])
        self._annotation_id = 1
        self._metrics = None
        self._include_metrics_per_category = include_metrics_per_category
        self._all_metrics_per_category = all_metrics_per_category
        self._skip_predictions_for_unlabeled_class = (
            skip_predictions_for_unlabeled_class
        )
        self._groundtruth_labeled_classes = {}

    def clear(self):
        """Clears the state to prepare for a fresh evaluation."""
        self._image_ids.clear()
        self._groundtruth_list = []
        self._detection_boxes_list = []

    def add_single_ground_truth_image_info(self, image_id, groundtruth_dict):
        """Adds groundtruth for a single image to be used for evaluation.

        If the image has already been added, a warning is logged, and groundtruth is
        ignored.

        Args:
          image_id: A unique string/integer identifier for the image.
          groundtruth_dict: A dictionary containing -
            InputDataFields.groundtruth_boxes: float32 numpy array of shape
              [num_boxes, 4] containing `num_boxes` groundtruth boxes of the format
              [ymin, xmin, ymax, xmax] in absolute image coordinates.
            InputDataFields.groundtruth_classes: integer numpy array of shape
              [num_boxes] containing 1-indexed groundtruth classes for the boxes.
            InputDataFields.groundtruth_area (optional): float numpy array of
              shape [num_boxes] containing the area (in the original absolute
              coordinates) of the annotated object.
            InputDataFields.groundtruth_keypoints (optional): float numpy array of
              keypoints with shape [num_boxes, num_keypoints, 2].
            InputDataFields.groundtruth_keypoint_visibilities (optional): integer
              numpy array of keypoint visibilities with shape [num_gt_boxes,
              num_keypoints]. Integer is treated as an enum with 0=not labeled,
              1=labeled but not visible and 2=labeled and visible.
            InputDataFields.groundtruth_labeled_classes (optional): a tensor of
              shape [num_classes + 1] containing the multi-hot tensor indicating the
              classes that each image is labeled for. Note that the classes labels
              are 1-indexed.
        """
        if image_id in self._image_ids:
            tf.logging.warning(
                "Ignoring ground truth with image id %s since it was "
                "previously added",
                image_id,
            )
            return

        # Drop optional fields if empty tensor.
        groundtruth_area = groundtruth_dict.get(fields.InputDataFields.groundtruth_area)
        groundtruth_keypoints = groundtruth_dict.get(
            fields.InputDataFields.groundtruth_keypoints
        )
        groundtruth_keypoint_visibilities = groundtruth_dict.get(
            fields.InputDataFields.groundtruth_keypoint_visibilities
        )
        if groundtruth_area is not None and not groundtruth_area.shape[0]:
            groundtruth_area = None
        if groundtruth_keypoints is not None and not groundtruth_keypoints.shape[0]:
            groundtruth_keypoints = None
        if (
            groundtruth_keypoint_visibilities is not None
            and not groundtruth_keypoint_visibilities.shape[0]
        ):
            groundtruth_keypoint_visibilities = None

        self._groundtruth_list.extend(
            coco_tools.ExportSingleImageGroundtruthToCoco(
                image_id=image_id,
                next_annotation_id=self._annotation_id,
                category_id_set=self._category_id_set,
                groundtruth_boxes=groundtruth_dict[
                    fields.InputDataFields.groundtruth_boxes
                ],
                groundtruth_classes=groundtruth_dict[
                    fields.InputDataFields.groundtruth_classes
                ],
                groundtruth_area=groundtruth_area,
                groundtruth_keypoints=groundtruth_keypoints,
                groundtruth_keypoint_visibilities=groundtruth_keypoint_visibilities,
            )
        )

        self._annotation_id += groundtruth_dict[
            fields.InputDataFields.groundtruth_boxes
        ].shape[0]
        if (fields.InputDataFields.groundtruth_labeled_classes) in groundtruth_dict:
            labeled_classes = groundtruth_dict[
                fields.InputDataFields.groundtruth_labeled_classes
            ]
            if labeled_classes.shape != (len(self._category_id_set) + 1,):
                raise ValueError(
                    "Invalid shape for groundtruth labeled classes: {}, "
                    "num_categories_including_background: {}".format(
                        labeled_classes, len(self._category_id_set) + 1
                    )
                )
            self._groundtruth_labeled_classes[image_id] = np.flatnonzero(
                groundtruth_dict[fields.InputDataFields.groundtruth_labeled_classes]
                == 1
            ).tolist()

        # Boolean to indicate whether a detection has been added for this image.
        self._image_ids[image_id] = False

    def add_single_detected_image_info(self, image_id, detections_dict):
        """Adds detections for a single image to be used for evaluation.

        If a detection has already been added for this image id, a warning is
        logged, and the detection is skipped.

        Args:
          image_id: A unique string/integer identifier for the image.
          detections_dict: A dictionary containing -
            DetectionResultFields.detection_boxes: float32 numpy array of shape
              [num_boxes, 4] containing `num_boxes` detection boxes of the format
              [ymin, xmin, ymax, xmax] in absolute image coordinates.
            DetectionResultFields.detection_scores: float32 numpy array of shape
              [num_boxes] containing detection scores for the boxes.
            DetectionResultFields.detection_classes: integer numpy array of shape
              [num_boxes] containing 1-indexed detection classes for the boxes.
            DetectionResultFields.detection_keypoints (optional): float numpy array
              of keypoints with shape [num_boxes, num_keypoints, 2].
        Raises:
          ValueError: If groundtruth for the image_id is not available.
        """
        if image_id not in self._image_ids:
            raise ValueError("Missing groundtruth for image id: {}".format(image_id))

        if self._image_ids[image_id]:
            tf.logging.warning(
                "Ignoring detection with image id %s since it was " "previously added",
                image_id,
            )
            return

        # Drop optional fields if empty tensor.
        detection_keypoints = detections_dict.get(
            fields.DetectionResultFields.detection_keypoints
        )
        if detection_keypoints is not None and not detection_keypoints.shape[0]:
            detection_keypoints = None

        if self._skip_predictions_for_unlabeled_class:
            det_classes = detections_dict[
                fields.DetectionResultFields.detection_classes
            ]
            num_det_boxes = det_classes.shape[0]
            keep_box_ids = []
            for box_id in range(num_det_boxes):
                if det_classes[box_id] in self._groundtruth_labeled_classes[image_id]:
                    keep_box_ids.append(box_id)
            self._detection_boxes_list.extend(
                coco_tools.ExportSingleImageDetectionBoxesToCoco(
                    image_id=image_id,
                    category_id_set=self._category_id_set,
                    detection_boxes=detections_dict[
                        fields.DetectionResultFields.detection_boxes
                    ][keep_box_ids],
                    detection_scores=detections_dict[
                        fields.DetectionResultFields.detection_scores
                    ][keep_box_ids],
                    detection_classes=detections_dict[
                        fields.DetectionResultFields.detection_classes
                    ][keep_box_ids],
                    detection_keypoints=detection_keypoints,
                )
            )
        else:
            self._detection_boxes_list.extend(
                coco_tools.ExportSingleImageDetectionBoxesToCoco(
                    image_id=image_id,
                    category_id_set=self._category_id_set,
                    detection_boxes=detections_dict[
                        fields.DetectionResultFields.detection_boxes
                    ],
                    detection_scores=detections_dict[
                        fields.DetectionResultFields.detection_scores
                    ],
                    detection_classes=detections_dict[
                        fields.DetectionResultFields.detection_classes
                    ],
                    detection_keypoints=detection_keypoints,
                )
            )
        self._image_ids[image_id] = True

    def evaluate(self):
        """Evaluates the detection boxes and returns a dictionary of coco metrics.

        Returns:
          A dictionary holding -

          1. summary_metrics:
          'DetectionBoxes_Precision/mAP': mean average precision over classes
            averaged over IOU thresholds ranging from .5 to .95 with .05
            increments.
          'DetectionBoxes_Precision/mAP@.50IOU': mean average precision at 50% IOU
          'DetectionBoxes_Precision/mAP@.75IOU': mean average precision at 75% IOU
          'DetectionBoxes_Precision/mAP (small)': mean average precision for small
            objects (area < 32^2 pixels).
          'DetectionBoxes_Precision/mAP (medium)': mean average precision for
            medium sized objects (32^2 pixels < area < 96^2 pixels).
          'DetectionBoxes_Precision/mAP (large)': mean average precision for large
            objects (96^2 pixels < area < 10000^2 pixels).
          'DetectionBoxes_Recall/AR@1': average recall with 1 detection.
          'DetectionBoxes_Recall/AR@10': average recall with 10 detections.
          'DetectionBoxes_Recall/AR@100': average recall with 100 detections.
          'DetectionBoxes_Recall/AR@100 (small)': average recall for small objects
            with 100.
          'DetectionBoxes_Recall/AR@100 (medium)': average recall for medium objects
            with 100.
          'DetectionBoxes_Recall/AR@100 (large)': average recall for large objects
            with 100 detections.

          2. per_category_ap: if include_metrics_per_category is True, category
          specific results with keys of the form:
          'Precision mAP ByCategory/category' (without the supercategory part if
          no supercategories exist). For backward compatibility
          'PerformanceByCategory' is included in the output regardless of
          all_metrics_per_category.
            If super_categories are provided, then this will additionally include
          metrics aggregated along the super_categories with keys of the form:
          `PerformanceBySuperCategory/<super-category-name>`
        """
        tf.logging.info("Performing evaluation on %d images.", len(self._image_ids))
        groundtruth_dict = {
            "annotations": self._groundtruth_list,
            "images": [{"id": image_id} for image_id in self._image_ids],
            "categories": self._categories,
        }
        coco_wrapped_groundtruth = coco_tools.COCOWrapper(groundtruth_dict)
        coco_wrapped_detections = coco_wrapped_groundtruth.LoadAnnotations(
            self._detection_boxes_list
        )
        box_evaluator = coco_tools.COCOEvalWrapper(
            coco_wrapped_groundtruth, coco_wrapped_detections, agnostic_mode=False
        )
        box_metrics, box_per_category_ap = box_evaluator.ComputeMetrics(
            include_metrics_per_category=self._include_metrics_per_category,
            all_metrics_per_category=self._all_metrics_per_category,
            super_categories=None,
        )
        box_metrics.update(box_per_category_ap)
        box_metrics = {
            "DetectionBoxes_" + key: value for key, value in iter(box_metrics.items())
        }
        return box_metrics

    def add_eval_dict(self, eval_dict, num_classes, tflite_path, model_format):
        """Observes an evaluation result dict for a single example.

        When executing eagerly, once all observations have been observed by this
        method you can use `.evaluate()` to get the final metrics.

        When using `tf.estimator.Estimator` for evaluation this function is used by
        `get_estimator_eval_metric_ops()` to construct the metric update op.

        Args:
          eval_dict: A dictionary that holds tensors for evaluating an object
            detection model, returned from
            eval_util.result_dict_for_single_example().

        Returns:
          None when executing eagerly, or an update_op that can be used to update
          the eval metrics in `tf.estimator.EstimatorSpec`.
        """

        if model_format.split("_")[1] == "edgetpu":
            print("\nUSING EDGETPU!\n")
            import tflite_runtime.interpreter as tflite

            delegate = tflite.Delegate("libedgetpu.so.1")
            interpreter = tflite.Interpreter(
                model_path=tflite_path,
                experimental_delegates=[delegate],
            )
        else:
            interpreter = tf.lite.Interpreter(model_path=tflite_path)

        interpreter.allocate_tensors()

        def update_op(
            image_id_batched,
            groundtruth_boxes_batched,
            groundtruth_classes_batched,
            num_gt_boxes_per_image,
            original_image_batched,
            original_image_spatial_shape_batched,
            is_annotated_batched,
        ):
            """Update operation for adding batch of images to Coco evaluator."""
            for (
                image_id,
                gt_box,
                gt_class,
                num_gt_box,
                org_img,
                org_img_spacial,
                is_annotated,
            ) in zip(
                image_id_batched,
                groundtruth_boxes_batched,
                groundtruth_classes_batched,
                num_gt_boxes_per_image,
                original_image_batched,
                original_image_spatial_shape_batched,
                is_annotated_batched,
            ):
                img = tf.expand_dims(org_img, 0)

                if model_format.split("_")[0] == "odapi":
                    boxes, scores, classes = parse_odapi(
                        interpreter, img, org_img_spacial, num_classes
                    )
                elif model_format.split("_")[0] == "nolo":
                    boxes, scores, classes = parse_nolo(
                        interpreter, img, org_img_spacial, num_classes
                    )
                else:
                    raise Exception("unknown model format")

                if is_annotated:
                    self.add_single_ground_truth_image_info(
                        image_id,
                        {
                            "groundtruth_boxes": gt_box[:num_gt_box],
                            "groundtruth_classes": gt_class[:num_gt_box],
                        },
                    )
                    self.add_single_detected_image_info(
                        image_id,
                        {
                            "detection_boxes": boxes,
                            "detection_scores": scores,
                            "detection_classes": classes,
                        },
                    )

        # Unpack items from the evaluation dictionary.
        input_data_fields = fields.InputDataFields
        image_id = eval_dict[input_data_fields.key]
        groundtruth_boxes = eval_dict[input_data_fields.groundtruth_boxes]
        groundtruth_classes = eval_dict[input_data_fields.groundtruth_classes]
        original_image = eval_dict[input_data_fields.original_image]
        original_image_spatial_shape = eval_dict[
            input_data_fields.original_image_spatial_shape
        ]
        num_gt_boxes_per_image = eval_dict.get(
            input_data_fields.num_groundtruth_boxes, None
        )
        is_annotated = eval_dict.get("is_annotated", None)

        if not image_id.shape.as_list():
            # Apply a batch dimension to all tensors.
            image_id = tf.expand_dims(image_id, 0)
            groundtruth_boxes = tf.expand_dims(groundtruth_boxes, 0)
            groundtruth_classes = tf.expand_dims(groundtruth_classes, 0)
            original_image = tf.expand_dims(original_image, 0)

            if num_gt_boxes_per_image is None:
                num_gt_boxes_per_image = tf.shape(groundtruth_boxes)[1:2]
            else:
                num_gt_boxes_per_image = tf.expand_dims(num_gt_boxes_per_image, 0)

            if is_annotated is None:
                is_annotated = tf.constant([True])
            else:
                is_annotated = tf.expand_dims(is_annotated, 0)
        else:
            if num_gt_boxes_per_image is None:
                num_gt_boxes_per_image = tf.tile(
                    tf.shape(groundtruth_boxes)[1:2],
                    multiples=tf.shape(groundtruth_boxes)[0:1],
                )
            if is_annotated is None:
                is_annotated = tf.ones_like(image_id, dtype=tf.bool)

        return tf.py_func(
            update_op,
            [
                image_id,
                groundtruth_boxes,
                groundtruth_classes,
                num_gt_boxes_per_image,
                original_image,
                original_image_spatial_shape,
                is_annotated,
            ],
            [],
        )

    def get_estimator_eval_metric_ops(
        self, eval_dict, num_classes, tflite_path, model_format
    ):
        """Returns a dictionary of eval metric ops.

        Note that once value_op is called, the detections and groundtruth added via
        update_op are cleared.

        This function can take in groundtruth and detections for a batch of images,
        or for a single image. For the latter case, the batch dimension for input
        tensors need not be present.

        Args:
          eval_dict: A dictionary that holds tensors for evaluating object detection
            performance. For single-image evaluation, this dictionary may be
            produced from eval_util.result_dict_for_single_example(). If multi-image
            evaluation, `eval_dict` should contain the fields
            'num_groundtruth_boxes_per_image' and 'num_det_boxes_per_image' to
            properly unpad the tensors from the batch.

        Returns:
          a dictionary of metric names to tuple of value_op and update_op that can
          be used as eval metric ops in tf.estimator.EstimatorSpec. Note that all
          update ops must be run together and similarly all value ops must be run
          together to guarantee correct behaviour.
        """
        update_op = self.add_eval_dict(
            eval_dict, num_classes, tflite_path, model_format
        )
        metric_names = [
            "DetectionBoxes_Precision/mAP",
            "DetectionBoxes_Precision/mAP@.50IOU",
            "DetectionBoxes_Precision/mAP@.75IOU",
            "DetectionBoxes_Precision/mAP (large)",
            "DetectionBoxes_Precision/mAP (medium)",
            "DetectionBoxes_Precision/mAP (small)",
            "DetectionBoxes_Recall/AR@1",
            "DetectionBoxes_Recall/AR@10",
            "DetectionBoxes_Recall/AR@100",
            "DetectionBoxes_Recall/AR@100 (large)",
            "DetectionBoxes_Recall/AR@100 (medium)",
            "DetectionBoxes_Recall/AR@100 (small)",
        ]
        if self._include_metrics_per_category:
            for category_dict in self._categories:
                metric_names.append(
                    "DetectionBoxes_PerformanceByCategory/mAP/" + category_dict["name"]
                )

        def first_value_func():
            self._metrics = self.evaluate()
            self.clear()
            return np.float32(self._metrics[metric_names[0]])

        def value_func_factory(metric_name):
            def value_func():
                return np.float32(self._metrics[metric_name])

            return value_func

        # Ensure that the metrics are only evaluated once.
        first_value_op = tf.py_func(first_value_func, [], tf.float32)
        eval_metric_ops = {metric_names[0]: (first_value_op, update_op)}
        with tf.control_dependencies([first_value_op]):
            for metric_name in metric_names[1:]:
                eval_metric_ops[metric_name] = (
                    tf.py_func(value_func_factory(metric_name), [], np.float32),
                    update_op,
                )
        return eval_metric_ops


def parse_odapi(interpreter, img, org_img_spacial, num_classes):
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    interpreter.set_tensor(input_details[0]["index"], img)
    interpreter.invoke()

    # (100, 4) 0.18767011 0.31827283 1.0016407  0.5489831
    boxes = interpreter.get_tensor(output_details[0]["index"])[0]
    # (100)
    classes = interpreter.get_tensor(output_details[1]["index"])[0]
    # (100)  0.78125    0.703125   0.61328125
    scores = interpreter.get_tensor(output_details[2]["index"])[0]

    def _scale_box_to_absolute(box):
        y_scale = tf.cast(org_img_spacial[0], tf.float32)
        x_scale = tf.cast(org_img_spacial[1], tf.float32)
        [y_min, x_min, y_max, x_max] = box
        y_min = y_scale * y_min
        y_max = y_scale * y_max
        x_min = x_scale * x_min
        x_max = x_scale * x_max
        return np.array([y_min, x_min, y_max, x_max])

    # 0.29089028  0.07160883  0.60911095  0.22839238
    boxes = np.array(list(map(_scale_box_to_absolute, boxes)))
    # 148.93582    48.90883   311.8648    155.99199

    # One indexing.
    classes += 1

    return non_max_suppression(
        num_classes,
        boxes,
        scores,
        classes,
        score_thresh=1e-8,
        iou_thresh=0.6,
        max_size_per_class=100,
        max_total_size=100,
    )


def parse_nolo(interpreter, img, org_img_spacial, num_classes):
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    # (1, 320, 320, 3) Convert to floats between 0 and 1
    img = img.numpy()
    img = (img.astype(np.float32)) / 255
    _, w, h, _ = img.shape

    # Quantization preprocessing.
    scale, zero_point = input_details[0]["quantization"]
    img = (img / scale + zero_point).astype(input_details[0]["dtype"])

    interpreter.set_tensor(input_details[0]["index"], img)
    interpreter.invoke()

    # (84, 2100)
    output = interpreter.get_tensor(output_details[0]["index"])[0]

    # Quantization postprocessing.
    scale, zero_point = output_details[0]["quantization"]
    output = (output.astype(np.float32) - zero_point) * scale

    # Denormalize to input size.
    output[[0, 2]] *= w
    output[[1, 3]] *= h

    # (84, 2100)
    output = output.transpose(-1, -2)
    # (2100, 84)

    # (2100, 4)
    boxes = output[:, :4]

    # (2100, 80)
    classes = output[:, 4:]
    classes = classes[:, :num_classes]  # Optimization.

    boxes2 = []
    scores2 = []
    classes2 = []
    for i, cls in enumerate(classes):
        for class_index, score in enumerate(cls):
            boxes2.append(boxes[i])
            scores2.append(score)
            classes2.append(class_index)

    boxes = np.array(boxes2)
    scores = np.array(scores2)
    classes = np.array(classes2)

    boxes = xywh2xyxy(boxes)

    # Swap x and y.
    boxes[:, [0, 1, 2, 3]] = boxes[:, [1, 0, 3, 2]]

    # One indexing.
    classes += 1

    [bbox, conf, cls] = non_max_suppression(
        num_classes,
        boxes.astype(np.float32),
        scores,
        classes,
        score_thresh=1e-8,
        iou_thresh=0.6,
        max_size_per_class=100,
        max_total_size=100,
    )

    def _scale_box_to_absolute(box):
        y_scale = tf.cast(org_img_spacial[0], tf.float32)
        x_scale = tf.cast(org_img_spacial[1], tf.float32)
        [y_min, x_min, y_max, x_max] = box
        y_min = y_scale * (y_min / h)
        y_max = y_scale * (y_max / h)
        x_min = x_scale * (x_min / w)
        x_max = x_scale * (x_max / w)
        return np.array([y_min, x_min, y_max, x_max])

    bbox = np.array(list(map(_scale_box_to_absolute, bbox)))

    return [bbox, conf, cls]


def xywh2xyxy(x):
    out = np.empty_like(x)
    xy = x[..., :2]
    wh = x[..., 2:] / 2
    out[..., :2] = xy - wh
    out[..., 2:] = xy + wh
    return out


def non_max_suppression(
    num_classes,
    boxes,
    scores,
    classes,
    score_thresh,
    iou_thresh,
    max_size_per_class=100,
    max_total_size=100,
):
    """
    Returns:
      'nmsed_boxes': A [max_detections, 4] float32 containing the non-max suppressed boxes.
      'nmsed_scores': A [max_detections] float32 containing the scores for the boxes.
      'nmsed_classes': A [max_detections] float32 containing the class for boxes.
    """

    boxes_shape = boxes.shape
    num_anchors = shape_utils.get_dim_as_int(boxes_shape[0])

    detection_boxes = tf.expand_dims(boxes, 0)
    # (1, 100, 4)
    detection_boxes = tf.identity(detection_boxes, "raw_box_locations")
    detection_boxes = tf.expand_dims(detection_boxes, axis=2)
    # (1, 100, 1, 4) float32
    # [[
    #     [[ 15.130978  38.979225 459.76993  614.61847 ]]
    #     [[153.69075  250.46078  256.30933  318.87317 ]]
    # ]]

    # (100, 2)
    scores2 = np.full((num_anchors, num_classes + 1), -1, np.float32)
    for i in range(len(scores)):
        if (int(classes[i]) - 1) >= num_classes:
            continue
        scores2[i][int(classes[i])] = scores[i]

    scores2 = tf.expand_dims(tf.cast(scores2, dtype=tf.float32), axis=0)
    # (1, 100, 2) 0.76953125  float32
    # [[
    #    [0.        ]
    #    [0.        ]
    #    [0.59375   ]
    # ]]
    detection_scores = tf.identity(scores2, "raw_box_scores")

    (
        nmsed_boxes,
        nmsed_scores,
        nmsed_classes,
        _,
        _,
        _,
    ) = post_processing.batch_multiclass_non_max_suppression(
        detection_boxes,
        detection_scores,
        score_thresh,
        iou_thresh,
        max_size_per_class,
        max_total_size,
    )

    return (
        tf.squeeze(nmsed_boxes).numpy(),
        tf.squeeze(nmsed_scores).numpy(),
        tf.squeeze(nmsed_classes).numpy(),
    )
