# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

r"""Tool to export an object detection model for inference.

Prepares an object detection tensorflow graph for inference using model
configuration and a trained checkpoint. Outputs inference
graph, associated checkpoint files, a frozen inference graph and a
SavedModel (https://tensorflow.github.io/serving/serving_basic.html).

Notes:
 * This tool uses `use_moving_averages` from eval_config to decide which
   weights to freeze.

Example Usage:
--------------
python export_inference_graph.py \
    --pipeline_config_path path/to/ssd_inception_v2.config \
    --trained_checkpoint_prefix path/to/model.ckpt \
    --output_directory path/to/exported_model_directory

The expected output would be in the directory
path/to/exported_model_directory (which is created if it does not exist)
with contents:
 - inference_graph.pbtxt
 - model.ckpt.data-00000-of-00001
 - model.ckpt.info
 - model.ckpt.meta
 - frozen_inference_graph.pb
 + saved_model (a directory)

"""
import tensorflow.compat.v1 as tf
from google.protobuf import text_format
from od import exporter
# from od.protos import pipeline_pb2

flags = tf.app.flags

flags.DEFINE_string(
    "pipeline_config_path",
    None,
    "Path to a pipeline_pb2.TrainEvalPipelineConfig config " "file.",
)
flags.DEFINE_string(
    "trained_checkpoint_prefix",
    None,
    "Path to trained checkpoint, typically of the form " "path/to/model.ckpt",
)
flags.DEFINE_string("output_directory", None, "Path to write outputs.")

tf.app.flags.mark_flag_as_required("pipeline_config_path")
tf.app.flags.mark_flag_as_required("trained_checkpoint_prefix")
tf.app.flags.mark_flag_as_required("output_directory")
FLAGS = flags.FLAGS


def main(_):
    pipeline_config = pipeline_pb2.TrainEvalPipelineConfig()
    with tf.gfile.GFile(FLAGS.pipeline_config_path, "r") as f:
        text_format.Merge(f.read(), pipeline_config)

    exporter.export_inference_graph(
        pipeline_config, FLAGS.trained_checkpoint_prefix, FLAGS.output_directory
    )


if __name__ == "__main__":
    tf.app.run()
