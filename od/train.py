# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import functools

import tensorflow.compat.v1 as tf
from tensorflow.compat.v1 import estimator as tf_estimator

from od import model_lib
from od import model_builder
from od import inputs


def train(configs, model_dir="./train", feature_extractor_class=None):
    configs["eval_input_config"] = configs["eval_input_configs"][0]
    run_config = tf_estimator.RunConfig(model_dir=model_dir)

    model_config = configs["model"]
    detection_model_fn = functools.partial(
        model_builder.build, model_config=model_config
    )

    model_fn = model_lib.create_model_fn(
        detection_model_fn, configs, feature_extractor_class
    )
    estimator = tf_estimator.Estimator(model_fn=model_fn, config=run_config)

    train_config = configs["train_config"]
    train_input_config = configs["train_input_config"]

    # Create the input functions for TRAIN/EVAL/PREDICT.
    train_input_fn = inputs.create_train_input_fn(
        train_config, train_input_config, model_config
    )

    eval_config = configs["eval_config"]
    eval_input_configs = configs["eval_input_configs"]

    eval_input_fns = []
    for eval_input_config in eval_input_configs:
        eval_input_fns.append(
            inputs.create_eval_input_fn(
                eval_config=eval_config,
                eval_input_config=eval_input_config,
                model_config=model_config,
            )
        )

    eval_input_names = [
        eval_input_config.name for eval_input_config in eval_input_configs
    ]
    eval_input_names = (eval_input_names,)

    predict_input_fn = inputs.create_predict_input_fn(
        model_config=model_config, predict_input_config=eval_input_configs[0]
    )

    # update train_steps from config but only when non-zero value is provided
    if train_config.num_steps != 0:
        train_steps = train_config.num_steps

    train_spec, eval_specs = model_lib.create_train_and_eval_specs(
        train_input_fn, eval_input_fns, predict_input_fn, train_steps
    )

    # Currently only a single Eval Spec is allowed.
    tf_estimator.train_and_evaluate(estimator, train_spec, eval_specs[0])
