# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Binary to run train and evaluation on object detection model."""

from __future__ import absolute_import, division, print_function

import functools

import tensorflow.compat.v1 as tf
from absl import flags
from tensorflow.compat.v1 import estimator as tf_estimator

from od import inputs, model_builder, model_lib

# from od import config_util


flags.DEFINE_string(
    "model_dir",
    None,
    "Path to output model directory "
    "where event and checkpoint files will be written.",
)
flags.DEFINE_string("pipeline_config_path", None, "Path to pipeline config " "file.")
flags.DEFINE_string(
    "checkpoint_dir",
    None,
    "Path to directory holding a checkpoint.  If "
    "`checkpoint_dir` is provided, this binary operates in eval-only mode, "
    "writing resulting metrics to `model_dir`.",
)
flags.DEFINE_boolean(
    "run_once",
    False,
    "If running in eval-only mode, whether to run just "
    "one round of eval vs running continuously (default).",
)
flags.DEFINE_integer(
    "max_eval_retries",
    0,
    "If running continuous eval, the maximum number of "
    "retries upon encountering tf.errors.InvalidArgumentError. If negative, "
    "will always retry the evaluation.",
)
FLAGS = flags.FLAGS


def main(unused_argv):
    flags.mark_flag_as_required("model_dir")
    flags.mark_flag_as_required("pipeline_config_path")

    run_config = tf_estimator.RunConfig(model_dir=FLAGS.model_dir)
    configs = config_util.get_configs_from_pipeline_file(FLAGS.pipeline_config_path)

    model_config = configs["model"]
    detection_model_fn = functools.partial(
        model_builder.build, model_config=model_config
    )

    model_fn = model_lib.create_model_fn(detection_model_fn, configs)
    estimator = tf_estimator.Estimator(model_fn=model_fn, config=run_config)

    train_config = configs["train_config"]
    train_input_config = configs["train_input_config"]

    # Create the input functions for TRAIN/EVAL/PREDICT.
    train_input_fn = inputs.create_train_input_fn(
        train_config, train_input_config, model_config
    )

    eval_config = configs["eval_config"]
    eval_input_configs = configs["eval_input_configs"]

    eval_input_fns = []
    for eval_input_config in eval_input_configs:
        eval_input_fns.append(
            inputs.create_eval_input_fn(
                eval_config=eval_config,
                eval_input_config=eval_input_config,
                model_config=model_config,
            )
        )

    eval_input_names = [
        eval_input_config.name for eval_input_config in eval_input_configs
    ]
    eval_input_names = (eval_input_names,)

    predict_input_fn = inputs.create_predict_input_fn(
        model_config=model_config, predict_input_config=eval_input_configs[0]
    )

    # update train_steps from config but only when non-zero value is provided
    if train_config.num_steps != 0:
        train_steps = train_config.num_steps

    if FLAGS.checkpoint_dir:
        # The first eval input will be evaluated.
        input_fn = eval_input_fns[0]
        if FLAGS.run_once:
            checkpoint_path = tf.train.latest_checkpoint(FLAGS.checkpoint_dir)
            estimator.evaluate(input_fn, checkpoint_path=checkpoint_path)
        else:
            model_lib.continuous_eval(
                estimator,
                FLAGS.checkpoint_dir,
                input_fn,
                train_steps,
                "validation_data",
                FLAGS.max_eval_retries,
            )
    else:
        train_spec, eval_specs = model_lib.create_train_and_eval_specs(
            train_input_fn, eval_input_fns, predict_input_fn, train_steps
        )

        # Currently only a single Eval Spec is allowed.
        tf_estimator.train_and_evaluate(estimator, train_spec, eval_specs[0])


if __name__ == "__main__":
    tf.app.run()
