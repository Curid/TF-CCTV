import os
import json
import hashlib


def check_sum(obj, path):
    sum_path = os.path.join(path, "checksum")
    checksums = []
    with open(sum_path) as sum_file:
        lines = sum_file.readlines()
        checksums = [i.strip() for i in lines]

    dump = json.dumps(obj).encode("utf-8")
    obj_hash = hashlib.md5(dump).hexdigest()
    if obj_hash not in checksums:
        print("invalid checksum.\nexpected:\n", checksums, "\ngot:\n", obj_hash)
    else:
        print("Checksum Ok.")


def parse_xml(xml):
    """Recursively parses XML contents to python dict.

    We assume that `object` tags are the only ones that can appear
    multiple times at the same level of a tree.

    Args:
      xml: xml tree obtained by parsing XML file contents using lxml.etree

    Returns:
      Python dictionary holding XML contents.
    """
    if len(xml) == 0:
        return {xml.tag: xml.text}
    result = {}
    for child in xml:
        child_result = parse_xml(child)
        if child.tag != "object":
            result[child.tag] = child_result[child.tag]
        else:
            if child.tag not in result:
                result[child.tag] = []
            result[child.tag].append(child_result[child.tag])
    return {xml.tag: result}
