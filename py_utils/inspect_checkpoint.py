import sys

import numpy as np

from tensorflow.python import pywrap_tensorflow


def main():
    if len(sys.argv) != 2:
        print("Usage: inspect_checkpoint ./model.ckpt-400000")
        exit(1)

    file_name = sys.argv[1]

    try:
        reader = pywrap_tensorflow.NewCheckpointReader(file_name)
        var_to_shape_map = reader.get_variable_to_shape_map()

        for key, value in var_to_shape_map.items():
            # if "spaghetti_net/stem_node/stem/weights" in key:
            #  print(key, value, np.shape(value))
            if "FeatureExtractor/MobileDetEdgeTPU/Conv/weights" in key:
                print(key, value, np.shape(value))

        # Count total number of parameters
        print("# Total number of params: %d" % _count_total_params(reader))

    except Exception as e:  # pylint: disable=broad-except
        print(str(e))
        if "corrupted compressed block contents" in str(e):
            print(
                "It's likely that your checkpoint file has been compressed "
                "with SNAPPY."
            )
        if "Data loss" in str(e) and any(
            e in file_name for e in [".index", ".meta", ".data"]
        ):
            proposed_file = ".".join(file_name.split(".")[0:-1])
            v2_file_error_template = """
It's likely that this is a V2 checkpoint and you need to provide the filename
*prefix*.  Try removing the '.' and extension.  Try:
inspect checkpoint --file_name = {}"""
            print(v2_file_error_template.format(proposed_file))


def _count_total_params(reader):
    """Count total number of variables."""
    var_to_shape_map = reader.get_variable_to_shape_map()

    var_sizes = [np.prod(var_to_shape_map[v]) for v in var_to_shape_map]
    return np.sum(var_sizes, dtype=int)


if __name__ == "__main__":
    main()
