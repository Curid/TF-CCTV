from tensorflow.python import pywrap_tensorflow
import tensorflow as tf
import numpy as np

flags = tf.app.flags
flags.DEFINE_string(
    "input_path",
    "./pretrained_models/mobiledet_edgetpu/fp32/model.ckpt-400000",
    "path of pretrained_checkpoint",
)

flags.DEFINE_string(
    "output_path",
    "./pretrained_models/mobiledet1/model.ckpt-400000",
    "output checkpoint",
)

FLAGS = flags.FLAGS


if __name__ == "__main__":
    reader = pywrap_tensorflow.NewCheckpointReader(FLAGS.input_path)
    var_to_shape_map = reader.get_variable_to_shape_map()
    var_to_edit_names = [
        "FeatureExtractor/MobileDetEdgeTPU/Conv/weights",
        "FeatureExtractor/MobileDetEdgeTPU/Conv/weights/Momentum",
    ]
    print("Loading checkpoint...")
    for key in sorted(var_to_shape_map):
        if key not in var_to_edit_names:
            var = tf.Variable(reader.get_tensor(key), name=key, dtype=tf.float32)
        else:
            print("Found variable: {}".format(key))
    vars_to_edit = []
    for name in var_to_edit_names:
        if reader.has_tensor(name):
            vars_to_edit.append(reader.get_tensor(name))
        else:
            raise Exception(
                "{} not found in checkpoint. Check feature extractor name. Exiting.".format(
                    name
                )
            )

    new_vars = []
    sess = tf.Session()

    num_input_channels = 1

    for name, var_to_edit in zip(var_to_edit_names, vars_to_edit):
        new_var = np.resize(var_to_edit, (3, 3, num_input_channels, 32))
        new_vars.append(tf.Variable(new_var, name=name, dtype=tf.float32))

        print("a", name, "\nb", np.shape(var_to_edit), "c", np.shape(new_var))

    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    saver.save(sess, FLAGS.output_path)
