from tabulate import tabulate
from dataclasses import dataclass
import numpy as np
import re

mAP_str = "DetectionBoxes_Precision/mAP"
l_str = "DetectionBoxes_Precision/mAP (large)"
m_str = "DetectionBoxes_Precision/mAP (medium)"
s_str = "DetectionBoxes_Precision/mAP (small)"


def print_eval_results(results):
    def round_field(field):
        if isinstance(field, np.floating):
            return round(field * 100, 1)
        else:
            return field

    def format_result(result):
        (name, result) = result
        result = (
            name,
            result[mAP_str],
            result[l_str],
            result[m_str],
            result[s_str],
        )
        return list(map(round_field, result))

    print(
        tabulate(
            map(format_result, results),
            headers=["model_name", "mAP", "L", "M", "S"],
            tablefmt="github",
        )
    )


def read_eval_markdown_table(file_path: str, target: str, target2: str) -> list[dict]:
    with open(file_path, "r") as file:
        # Seek to first target.
        while True:
            line = file.readline()
            if not line:
                raise Exception(f"failed to find target '{target}'")
            elif line.startswith(target):
                break

        # Seek to second target.
        while True:
            line = file.readline()
            if not line:
                raise Exception(f"failed to find target2 '{target2}'")
            elif line.startswith(target2):
                break

        # Seek to first table.
        while True:
            line = file.readline()
            if not line:
                raise Exception("failed to find table")
            elif line.startswith("|----"):
                break

        # Parse table.
        m_name = " (\\S*)\\s*\\|"
        m_color = " (\\S*)\\s{1,2}\\|"
        m_float = " (\\d{2}\\.\\d) \\|"
        m_float2 = "\\s{1,2}(\\d{1,2}\\.\\d) \\|"
        m_floatx = "\\s*(\\S*) \\|"
        m_group = " (\\S) \\|"
        pattern = re.compile(
            f"^\\|{m_name}{m_color}{m_float}{m_float}{m_float2}{m_float2} \\|{m_floatx} \\|{m_group}$",
        )

        name_column = 0
        color_column = 1
        mAP_column = 2
        l_column = 3
        m_column = 4
        s_column = 5
        rpi5_column = 6
        group_column = 7

        out = []
        while True:
            line = file.readline()
            if len(line) == 1:
                # Table end.
                break
            if line.startswith("| -"):
                continue
            v = re.match(pattern, line)
            if not v:
                continue

            g = v.groups()
            out.append(
                {
                    "model_name": g[name_column],
                    "color": g[color_column],
                    "mAP": float(g[mAP_column]),
                    "large": float(g[l_column]),
                    "medium": float(g[m_column]),
                    "small": float(g[s_column]),
                    "rpi5": float(g[rpi5_column]),
                    "group": g[group_column],
                }
            )

        return out
