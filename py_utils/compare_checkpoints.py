import sys
import json

from tensorflow.python import pywrap_tensorflow


def main():
    if len(sys.argv) != 3:
        print("Usage: compare_checkpoints ./model.ckpt-400000 ./model.ckpt-400000")
        exit(1)

    ckpt1 = sys.argv[1]
    ckpt2 = sys.argv[2]

    reader1 = pywrap_tensorflow.NewCheckpointReader(ckpt1)
    var_to_shape_map1 = reader1.get_variable_to_shape_map()

    reader2 = pywrap_tensorflow.NewCheckpointReader(ckpt2)
    var_to_shape_map2 = reader2.get_variable_to_shape_map()

    keys1 = []
    for key in var_to_shape_map1:
        keys1.append(clean(key))

    keys2 = []
    for key in var_to_shape_map2:
        keys2.append(clean(key))

    with open("tmp1.json", "w") as f:
        json.dump(keys1, f, indent=2)

    with open("tmp2.json", "w") as f:
        json.dump(keys2, f, indent=2)


def clean(key):
    key = key.replace("FeatureExtractor/spaghettinet_edgetpu_l/spaghetti_net", "")
    key = key.replace("FeatureExtractor/spaghetti_net/spaghetti_net", "")
    key = key.replace("FeatureExtractor/spaghettinet_edgetpu_l", "")
    key = key.replace("FeatureExtractor/spaghetti_net", "")
    return key


if __name__ == "__main__":
    main()
