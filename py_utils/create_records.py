import os
import io
import logging
import hashlib
import PIL.Image
import contextlib2

import tensorflow.compat.v1 as tf


def create_records(instance, output_path, num_shards):
    """Loads COCO annotation json files and converts to tf.Record format.

    Args:
      instance: object containing bounding box annotations.
      output_path: Path to output tf.Record file.
      num_shards: number of output file shards.
    """
    output_path = os.path.join(output_path, "record")

    with contextlib2.ExitStack() as tf_record_close_stack:
        output_tfrecords = open_sharded_output_tfrecords(
            tf_record_close_stack, output_path, num_shards
        )

        images = instance["images"]
        category_index = create_category_index(instance["categories"])

        annotations_index = {}
        if "annotations" in instance:
            logging.info("Found groundtruth annotations. Building annotations index.")
            for annotation in instance["annotations"]:
                image_id = annotation["image_id"]
                if image_id not in annotations_index:
                    annotations_index[image_id] = []
                annotations_index[image_id].append(annotation)
        missing_annotation_count = 0
        for image in images:
            image_id = image["id"]
            if image_id not in annotations_index:
                missing_annotation_count += 1
                annotations_index[image_id] = []
        logging.info("%d images are missing annotations.", missing_annotation_count)

        total_num_annotations_skipped = 0
        for idx, image in enumerate(images):
            if idx % 100 == 0:
                logging.info("On image %d of %d", idx, len(images))
            annotations_list = annotations_index[image["id"]]
            (_, tf_example, num_annotations_skipped) = create_tf_example(
                image, annotations_list, category_index
            )

            total_num_annotations_skipped += num_annotations_skipped
            shard_idx = idx % num_shards
            if tf_example:
                output_tfrecords[shard_idx].write(tf_example.SerializeToString())
        logging.info(
            "Finished writing, skipped %d annotations.", total_num_annotations_skipped
        )


def create_tf_example(image, annotations_list, category_index):
    """Converts image and annotations to a tf.Example proto.

    Args:
      image: dict with keys: [u'license', u'file_name', u'coco_url', u'height',
        u'width', u'date_captured', u'flickr_url', u'id']
      annotations_list:
        list of dicts with keys: [u'segmentation', u'area', u'iscrowd',
          u'image_id', u'bbox', u'category_id', u'id'] Notice that bounding box
          coordinates in the official COCO dataset are given as [x, y, width,
          height] tuples using absolute coordinates where x, y represent the
          top-left (0-indexed) corner.  This function converts to the format
          expected by the Tensorflow Object Detection API (which is which is
          [ymin, xmin, ymax, xmax] with coordinates normalized relative to image
          size).
      category_index: a dict containing COCO category information keyed by the
        'id' field of each category.  See the label_map_util.create_category_index
        function.

    Returns:
      key: SHA256 hash of the image.
      example: The converted tf.Example
      num_annotations_skipped: Number of (invalid) annotations that were ignored.

    Raises:
      ValueError: if the image pointed to by data['filename'] is not a valid JPEG
    """

    image_height = image["height"]
    image_width = image["width"]
    filename = image["file_name"]
    image_id = image["id"]

    with tf.gfile.GFile(filename, "rb") as fid:
        encoded_jpg = fid.read()
    encoded_jpg_io = io.BytesIO(encoded_jpg)
    image = PIL.Image.open(encoded_jpg_io)
    key = hashlib.sha256(encoded_jpg).hexdigest()

    xmin = []
    xmax = []
    ymin = []
    ymax = []
    category_names = []
    category_ids = []
    num_annotations_skipped = 0
    for object_annotations in annotations_list:
        (x, y, width, height) = tuple(object_annotations["bbox"])
        if width <= 0 or height <= 0:
            num_annotations_skipped += 1
            continue
        if x + width > image_width or y + height > image_height:
            num_annotations_skipped += 1
            continue
        category_id = int(object_annotations["category_id"])
        category_name = category_index[category_id]["name"].encode("utf8")
        xmin.append(float(x) / image_width)
        xmax.append(float(x + width) / image_width)
        ymin.append(float(y) / image_height)
        ymax.append(float(y + height) / image_height)
        category_ids.append(category_id)
        category_names.append(category_name)

        feature_dict = {
            "image/height": int64_feature(image_height),
            "image/width": int64_feature(image_width),
            "image/filename": bytes_feature(filename.encode("utf8")),
            "image/source_id": bytes_feature(str(image_id).encode("utf8")),
            "image/key/sha256": bytes_feature(key.encode("utf8")),
            "image/encoded": bytes_feature(encoded_jpg),
            "image/format": bytes_feature("jpeg".encode("utf8")),
            "image/object/bbox/xmin": float_list_feature(xmin),
            "image/object/bbox/xmax": float_list_feature(xmax),
            "image/object/bbox/ymin": float_list_feature(ymin),
            "image/object/bbox/ymax": float_list_feature(ymax),
            "image/object/class/text": bytes_list_feature(category_names),
        }

    example = tf.train.Example(features=tf.train.Features(feature=feature_dict))
    return (key, example, num_annotations_skipped)


def open_sharded_output_tfrecords(exit_stack, base_path, num_shards):
    """Opens all TFRecord shards for writing and adds them to an exit stack.

    Args:
      exit_stack: A context2.ExitStack used to automatically closed the TFRecords
        opened in this function.
      base_path: The base path for all shards
      num_shards: The number of shards

    Returns:
      The list of opened TFRecords. Position k in the list corresponds to shard k.
    """
    tf_record_output_filenames = [
        "{}-{:05d}-of-{:05d}".format(base_path, idx, num_shards)
        for idx in range(num_shards)
    ]

    tfrecords = [
        exit_stack.enter_context(tf.python_io.TFRecordWriter(file_name))
        for file_name in tf_record_output_filenames
    ]

    return tfrecords


def create_category_index(categories):
    """Creates dictionary of COCO compatible categories keyed by category id.

    Args:
      categories: a list of dicts, each of which has the following keys:
        'id': (required) an integer id uniquely identifying this category.
        'name': (required) string representing category name
          e.g., 'cat', 'dog', 'pizza'.

    Returns:
      category_index: a dict containing the same entries as categories, but keyed
        by the 'id' field of each category.
    """
    category_index = {}
    for cat in categories:
        category_index[cat["id"]] = cat
    return category_index


def int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def int64_list_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=value))


def bytes_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def bytes_list_feature(value):
    return tf.train.Feature(bytes_list=tf.train.BytesList(value=value))


def float_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


def float_list_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=value))
