import os
import json
import hashlib
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dir", help="Directory", type=str)
args = parser.parse_args()

path = os.path.abspath(args.dir)
files = os.listdir(path)
files.sort()

hashes = {}

for file_name in files:
    if "record-" not in file_name:
        continue

    print(file_name)
    file_path = os.path.join(path, file_name)
    file_hash = hashlib.md5(open(file_path, "rb").read()).hexdigest()
    hashes[file_name] = [file_hash]

output_data = json.dumps(hashes, indent=2)
output_file = os.path.join(path, "sum.json")
with open(output_file, "w") as outfile:
    outfile.write(output_data)
