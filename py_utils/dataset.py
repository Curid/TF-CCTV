import os
import json

from itertools import chain

from py_utils.utils import parse_xml


class parser:
    datasets_path = ""
    minival_ids = []
    coco_classes = {}
    coco_class_ids = []
    coco_class_names = []
    coco_blacklist_ids = []
    coco_gray_ids = []
    voc_gray_names = []
    train_json = {}
    val_json = {}

    def __init__(
        self,
        datasets_path,
        labels_path,
    ):
        minival_ids_path = os.path.join(datasets_path, "mscoco_minival_ids.txt")
        minival_ids = read_list_file(minival_ids_path)

        self.datasets_path = datasets_path
        self.minival_ids = minival_ids

        coco_blacklist_path = os.path.join(datasets_path, "mscoco_blacklist_ids.txt")
        self.coco_blacklist_ids = read_list_file(coco_blacklist_path)

        coco_gray_path = os.path.join(datasets_path, "mscoco_gray_ids.txt")
        self.coco_gray_ids = read_list_file(coco_gray_path)

        voc_gray_path = os.path.join(datasets_path, "voc_gray_names.txt")
        self.voc_gray_names = read_list_file(voc_gray_path)

        self.coco_classes = read_class_ids(labels_path)
        self.coco_class_ids = list(self.coco_classes.values())
        self.coco_class_names = self.coco_classes.keys()

        coco_path = os.path.join(datasets_path, "coco2017")

        print("reading instances_train2017.json")
        train_file = os.path.join(coco_path, "instances_train2017.json")
        self.train_json = json.load(open(train_file, "r"))

        print("reading instances_val2017.json")
        val_file = os.path.join(coco_path, "instances_val2017.json")
        self.val_json = json.load(open(val_file, "r"))

    def gen_coco(self, dataset_id, minival=False, gray=False):
        print(f"gen_coco gray={gray}")
        image_ids = set()
        annotations = []
        for ann in chain(self.train_json["annotations"], self.val_json["annotations"]):
            if ann["image_id"] in self.coco_blacklist_ids:
                continue
            if ann["iscrowd"]:
                continue
            if not gray and ann["image_id"] in self.coco_gray_ids:
                continue
            if ann["category_id"] in self.coco_class_ids:
                ann["image_id"] += 1000000000 * dataset_id
                annotations.append(ann)
                image_ids.add(ann["image_id"])

        if gray:
            coco_path = os.path.join(self.datasets_path, "gray-coco2017")
        else:
            coco_path = os.path.join(self.datasets_path, "coco2017")

        images = []
        for img in chain(self.train_json["images"], self.val_json["images"]):
            if img["id"] in self.coco_blacklist_ids:
                continue
            if minival:
                if img["id"] not in self.minival_ids:
                    continue
            else:
                if img["id"] in self.minival_ids:
                    continue
            img["id"] += 1000000000 * dataset_id
            if img["id"] in image_ids:
                img["file_name"] = os.path.join(coco_path, "images", img["file_name"])
                images.append(clean_img(img))

        return {
            "images": images,
            "annotations": annotations,
            "categories": self.train_json["categories"],
        }

    def gen_voc(self, dataset_id, gray=False):
        from lxml import etree

        print(f"gen_voc gray={gray}")
        if gray:
            voc_path = os.path.join(self.datasets_path, "gray-VOC2012")
        else:
            voc_path = os.path.join(self.datasets_path, "VOC2012")

        ann_path = os.path.join(voc_path, "Annotations")
        ann_files = os.listdir(ann_path)

        images = []
        annotations = []
        for index, ann_file in enumerate(ann_files):
            path = os.path.join(ann_path, ann_file)

            xml_file = open(path, "r")
            xml_str = xml_file.read()
            xml_file.close()

            xml = etree.fromstring(xml_str)
            data = parse_xml(xml)["annotation"]

            file_name = data["filename"]
            name = os.path.splitext(file_name)[0]
            if not gray and name in self.voc_gray_names:
                continue

            objects = []
            for obj in data["object"]:
                if obj["name"] in self.coco_class_names:
                    new_obj = {"bndbox": obj["bndbox"], "name": obj["name"]}
                    objects.append(new_obj)
            if not objects:
                continue

            img_id = index + 1000000000 * dataset_id

            file_path = os.path.join(voc_path, "JPEGImages", file_name)
            image = {
                "id": img_id,
                "file_name": file_path,
                "width": int(data["size"]["width"]),
                "height": int(data["size"]["height"]),
            }
            images.append(image)
            for obj in objects:
                box = obj["bndbox"]
                xmin = int(float(box["xmin"]))
                ymin = int(float(box["ymin"]))
                xmax = int(float(box["xmax"]))
                ymax = int(float(box["ymax"]))

                width = xmax - xmin
                height = ymax - ymin
                ann = {
                    "image_id": img_id,
                    "category_id": self.coco_classes[obj["name"]],
                    "bbox": [xmin, ymin, width, height],
                    "area": width * height,
                }
                annotations.append(ann)

        return {
            "images": images,
            "annotations": annotations,
            # "categories": [{'supercategory': 'person', 'id': 1, 'name': 'person'}]
        }


def gen_objects365(datasets_path, labels_path, dataset_id, val=False, gray=False):
    print(f"gen_coco gray={gray}")
    val_cctv_v1_path = os.path.join(datasets_path, "objects365_val-cctv_v1.txt")
    val_cctv_v1_ids = read_list_file(val_cctv_v1_path)

    val_ids_path = os.path.join(datasets_path, "objects365_val_ids.txt")
    val_ids = read_list_file(val_ids_path)

    classes = read_class_ids(labels_path)
    class_ids = list(classes.values())
    class_names = classes.keys()

    val_path = os.path.join(datasets_path, "val")

    print("reading zhiyuan_objv2_val.json")
    val_file = os.path.join(val_path, "zhiyuan_objv2_val.json")
    val_json = json.load(open(val_file, "r"))

    image_ids = set()
    annotations = []
    for ann in val_json["annotations"]:
        category_id = objects365_to_coco_class_id(ann["category_id"] - 1) + 1
        if category_id == 0:
            continue
        ann["category_id"] = category_id
        if ann["iscrowd"] and category_id == 0:
            continue
        img_id = ann["image_id"]
        if val:
            if img_id not in val_cctv_v1_ids:
                continue
        else:
            if img_id in val_cctv_v1_ids:
                continue
        if category_id in class_ids:
            ann["image_id"] += 1000000000 * dataset_id
            annotations.append(ann)
            image_ids.add(ann["image_id"])

    if gray:
        images_path = os.path.join(datasets_path, "images-gray")
    else:
        images_path = os.path.join(datasets_path, "images")

    images = []
    for img in val_json["images"]:
        img_id = img["id"]
        if val:
            if img_id not in val_cctv_v1_ids:
                continue
        else:
            if img_id in val_cctv_v1_ids:
                continue
        img["id"] += 1000000000 * dataset_id
        if img["id"] in image_ids:
            img["file_name"] = os.path.join(images_path, str(img_id) + ".jpg")
            images.append(clean_img(img))

    fix_bbox_overflow(images, annotations)
    return {
        "images": images,
        "annotations": annotations,
        "categories": [{"name": "person", "id": 1}],
    }


def fix_bbox_overflow(images, anns):
    id_to_img = {}
    for img in images:
        id_to_img[img["id"]] = img

    for ann in anns:
        (x, y, width, height) = tuple(ann["bbox"])
        image = id_to_img[ann["image_id"]]
        image_height = image["height"]
        image_width = image["width"]

        overflow_x = (x + width) - image_width
        overflow_y = (y + height) - image_height
        if overflow_x > 0:
            ann["bbox"][2] -= overflow_x
        if overflow_y > 0:
            ann["bbox"][3] -= overflow_y

        (x, y, width, height) = tuple(ann["bbox"])
        if x + width > image_width or y + height > image_height:
            raise Exception("bbox still overflows after fix", ann["image_id"])


# Reads .pbtxt labels file.
def read_class_ids(path):
    item_id = None
    item_name = None
    items = {}

    with open(path, "r") as labels_file:
        for line in labels_file:
            line.replace(" ", "")
            if line == "item{":
                pass
            elif line == "}":
                pass
            elif "id" in line:
                item_id = int(line.split(":", 1)[1].strip())
            elif "display_name" in line:
                item_name = (
                    line.split(":", 1)[1].replace("'", "").replace('"', "").strip()
                )

            if item_id is not None and item_name is not None:
                items[item_name] = item_id
                item_id = None
                item_name = None

    return items


# Reads -map.txt labels file.
def read_class_ids2(path):
    items = {}

    with open(path, "r") as labels_file:
        for line in labels_file:
            [class_id, label] = line.strip().split("  ")
            class_id = int(class_id)
            if class_id in items:
                raise ValueError("duplicate id", class_id)
            items[class_id] = label
    return items


def objects365_to_coco_class_id(v):
    m = {
        0: 0,
        5: 2,
        21: 8,
        34: 2,
        46: 1,
        49: 2,
        56: 15,
        58: 3,
        65: 2,
        72: 8,
        77: -1,
        78: 18,
        84: -1,
        87: 2,
        92: 17,
        96: 20,
        99: 19,
        103: -1,
        109: 2,
        114: 4,
        115: -1,
        116: 6,
        124: -1,
        126: 2,
        129: 3,
        130: -1,
        136: 15,
        139: 16,
        144: 21,
        147: -1,
        178: 23,
        180: 24,
        188: 7,
        199: 7,
        208: -1,
        211: 8,
        215: -1,
        221: 15,
        228: -1,
        240: -1,
        241: 15,
        248: 2,
        262: 15,
        263: -1,
        272: -1,
        300: -1,
        307: -1,
        308: 2,
        311: -1,
        318: -1,
        319: -1,
        320: -1,
        321: -1,
        323: -1,
        324: -1,
        326: -1,
        330: -1,
        341: -1,
        342: -1,
        344: -1,
        358: -1,
        360: -1,
    }
    if v in m:
        return m[v]
    else:
        return -1


def combine_instances(instances):
    output = instances.pop()
    for inn in instances:
        """
    for img in inn["images"]:
      output["images"].append(img)

    for ann in inn["annotations"]:
      output["annotations"].append(ann)
    """
        output["images"].extend(inn["images"])
        output["annotations"].extend(inn["annotations"])
        if "categories" in inn:
            output["categories"] = inn["categories"]

    return output


def clean_img(img):
    del img["license"]
    if "flickr_url" in img:
        del img["flickr_url"]
    if "coco_url" in img:
        del img["coco_url"]
    if "date_captured" in img:
        del img["date_captured"]
    return img


def read_list_file(path: str) -> set:
    raw_file = open(path, "r")
    lines = raw_file.readlines()
    return set([int(i) for i in lines])
