from py_utils.eval import read_eval_markdown_table
import os
from pathlib import Path
import matplotlib as mpl
import matplotlib.pyplot as plt
from collections import defaultdict

# Deterministic svg output.
mpl.rcParams["svg.hashsalt"] = "fixed-salt"

script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
home_dir = script_dir.parent.parent
os.chdir(home_dir)

readme_path = os.path.join(home_dir, "README.md")
table = read_eval_markdown_table(
    readme_path, "### objects365-val-cctv-v1_person", "#### EDGETPU"
)

plt.rcParams["figure.figsize"] = (12, 7)
plt.axis((0, 55, 30, 60))

plt.title("EDGETPU", fontsize=22)
plt.gcf().text(0.6, 0.89, "COLOR", fontsize=16)
plt.gcf().text(0.73, 0.045, "https://codeberg.org/Curid/TF-CCTV", fontsize=10)
plt.gcf().text(
    0.125,
    0.892,
    "Evaluated on 684 handpicked person images from objects365",
    fontsize=8,
)
plt.xlabel("Latency RPI5 + USB accelerator (ms/img)", fontsize=16)
plt.ylabel("mAP", fontsize=16)
plt.gcf().text(0.085, 0.54, "50-95 val", fontsize=9, rotation=90)

plt.axvline(x=15, lw=0.6)

groups = defaultdict(list)
for row in table:
    if row["color"] != "color":
        continue
    group = row["group"]
    name = row["model_name"]
    x = row["rpi5"]
    y = row["mAP"]
    groups[group].append((name, x, y))


for group in groups.values():
    # Points and lines.
    _, x, y = list(zip(*group))
    plt.plot(x, y, "o-")

    # Annotations.
    for name, x, y in group:
        plt.annotate(name, (x, y), (x + 0.5, y - 0.15), fontsize=8)


svg_path = os.path.join(script_dir, "objects365-val-cctv-v1_person_edgetpu_color.svg")
with open(svg_path, "w+") as file:
    plt.savefig(file, bbox_inches="tight", pad_inches=0.2, format="svg")


plt.clf()

plt.rcParams["figure.figsize"] = (12, 7)
plt.axis((0, 45, 25, 50))

plt.title("EDGETPU", fontsize=22)
plt.gcf().text(0.6, 0.89, "GRAY", fontsize=16)
plt.gcf().text(0.73, 0.045, "https://codeberg.org/Curid/TF-CCTV", fontsize=10)
plt.gcf().text(
    0.125,
    0.892,
    "Evaluated on 684 handpicked person images from objects365",
    fontsize=8,
)
plt.xlabel("Latency RPI5 + USB accelerator (ms/img)", fontsize=16)
plt.ylabel("mAP", fontsize=16)
plt.gcf().text(0.085, 0.54, "50-95 val", fontsize=9, rotation=90)

plt.axvline(x=15, lw=0.6)

groups = defaultdict(list)
for row in table:
    if row["color"] != "gray":
        continue
    group = row["group"]
    name = row["model_name"]
    x = row["rpi5"]
    y = row["mAP"]
    groups[group].append((name, x, y))


for group in groups.values():
    # Points and lines.
    _, x, y = list(zip(*group))
    plt.plot(x, y, "o-")

    # Annotations.
    for name, x, y in group:
        plt.annotate(name, (x, y), (x + 0.3, y - 0.15), fontsize=8)


svg_path = os.path.join(script_dir, "objects365-val-cctv-v1_person_edgetpu_gray.svg")
with open(svg_path, "w+") as file:
    plt.savefig(file, bbox_inches="tight", pad_inches=0.2, format="svg")
