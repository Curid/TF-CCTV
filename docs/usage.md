

## USAGE

Instruction for how to use the included scrips.

Its recommended to run all commands from inside the included Docker.

* [Downloading the datasets](#downloading_the_datasets)
* [Generating gray datasets](#generating_gray_datasets)
* [Generating tfrecords](#generating_tfrecords)
* [Running evaluation](#running_evaluation)
* [Training a model](#training_a_model)

<br>

## Downloading the datasets

Download COCO2017 (20GB)

`./datasets/coco2017.sh`

Download VOC2012 (2GB)

`./datasets/voc2012.sh`

The datasets directory should look like this afterwards.

```
datasets
├── coco2017
│   ├── annotations_trainval2017.zip
│   ├── images                        // 123287 images in a single directory.
│   ├── instances_train2017.json
│   └── instances_val2017.json
└── VOC2012
    ├── Annotations  // 17125 files.
    ├── ImageSets
    │   ├── Action
    │   ├── Layout
    │   ├── Main
    │   └── Segmentation
    ├── JPEGImages         // 17125 images.
    ├── SegmentationClass
    └── SegmentationObject
```

## Environments

The `docker` folder containers environments for stable reproduction and dependencies.

**run-docker.sh:**

* Generating gray dataset variants
* Generating records

**run-train-docker.sh:**

* Running `train.sh` scripts

**eval.nix:**

* Running evaluations in `evals` folder
* Running bench in `bench` folder

**yolo.nix:**

* Export yolo model

**export_tf1.nix:**

* Export post training with `./utils/export_tflite.sh`
* Run `tflite_convert.sh` after exporting

**export_tf2.nix:**

* Compile with `./utils/compile_edgetpu.sh` post converting

## Generating gray datasets

```
./docker/run-docker.sh
./datasets/gen_gray-coco.sh
./datasets/gen_gray-voc.sh
```


The datasets directory should look like this afterwards.

```
datasets
├── coco2017
│   ├── annotations_trainval2017.zip
│   ├── images                        // 123287 images in a single directory.
│   ├── instances_train2017.json
│   └── instances_val2017.json
├── gray-coco2017
│   ├── images                    // 123287 images in a single directory.
│   ├── instances_train2017.json
│   └── instances_val2017.json
├── gray-VOC2012
│   ├── Annotations // 17125 files.
│   └── JPEGImages  // 17125 images.
└── VOC2012
    ├── Annotations  // 17125 files.
    ├── ImageSets
    │   ├── Action
    │   ├── Layout
    │   ├── Main
    │   └── Segmentation
    ├── JPEGImages         // 17125 images.
    ├── SegmentationClass
    └── SegmentationObject
```

<br>


## Generating tfrecords

Generating tfrecords from the datasets.

Each records directory has a `gen_records.py` file.

```python3
./docker/run-docker.sh
python3 ./records/cctv3.3/gen_records.py
```

<br>


## Running evaluation

Assuming you've generated tfrecords for the testset you wish to use. Evals in the `tflite` folder run on CPU while evals in the `edgetpu` folder run on an edgetpu.

You can evaluate the cctv3.3 model with the `objects365` testset on cpu using.

```python3
nix-shell ./docker/eval.nix
python ./eval/objects365-val-cctv-v1/tflite/cctv3.3--color_tflite.py
```

You can evaluate the mobiledet model with the `objects365` testset on edgetpu using.

```python3
nix-shell ./docker/eval.nix
python ./eval/objects365-val-cctv-v1/edgetpu/mobiledet_edgetpu--color_tflite.py
```
<br>


## Training a model

If you're familiar with the Tensorflow 1 object detection api, you can find the pipeline.config in the models directory and the training Dockerfile in the docker directory.

Feel free to open an issue or join the Matrix room if you have any questions.
