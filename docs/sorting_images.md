# Sorting images

## Setup

Download the repository.

```
git clone https://codeberg.org/Curid/TF-CCTV.git
cd TF-CCTV
```

Download the COCO2017 dataset.

```
./datasets/coco2017.sh
```

Install [Fiftyone](https://github.com/voxel51/fiftyone) or use the [included Docker](../docker)

```
pip install fiftyone
```

<br>

## Using Fiftyone

Show images with IDs in the range of 20000-30000

```
python3 ./datasets/fiftyone/coco.py --min 20000 --max 30000
```

Point your web browser to http://localhost:6006

<img src="./assets/fiftyone_landing.png">

Toggle bounding boxes and image IDs

<img src="./assets/fiftyone_toggle.png">


<br>

## Identifying bad images

Before starting, please check which images need to be sorted [here](https://codeberg.org/Curid/TF-CCTV/issues/4)

When you find a bad image, add the ID in either `./datasets/mscoco_blacklist_ids.txt` or a new text file. When you're finished, open a merge request or upload the file [here](https://codeberg.org/Curid/TF-CCTV/issues/5)

If you have any questions, feel free to ask [here](https://codeberg.org/Curid/TF-CCTV/issues/12)

<br> 

### Irrelevant images

Images that are unlikely to ever show up in surveillance footage, mostly close-up images.

<img src="./assets/irrelevant.png">

### Missing annotations
Cases where a unannotated object can clearly be identified as a `person`.

<img src="./assets/missing.png">


### Unidentifiable objects
Images with objects that are impossible to identify as `person`.

<img src="./assets/unidentifiable.png">

### Edited/Distorted images
Includes objects in paintings, posters, computer screens, mirrors and window reflections.

<img src="./assets/edited.png">
