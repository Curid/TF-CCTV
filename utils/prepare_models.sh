#!/bin/sh

script_dir=$1
home_dir="$script_dir"/..
models_dir="$script_dir"/models

cd "$script_dir" || exit

mkdir -p "$models_dir"/mobiledet
cd "$models_dir"/mobiledet || exit
cp "$home_dir"/labels/mscoco-map.txt ./labels.txt
cp "$home_dir"/pretrained_models/mobiledet_edgetpu/fp32/mobiledet_320x320.tflite ./
cp "$home_dir"/pretrained_models/mobiledet_edgetpu/fp32/mobiledet_320x320_edgetpu.tflite ./

mkdir -p "$models_dir"/cctv1
cd "$models_dir"/cctv1 || exit
cp "$home_dir"/labels/person-map.txt ./labels.txt
cp "$home_dir"/models/cctv1/cctv1_420x280.tflite ./
cp "$home_dir"/models/cctv1/cctv1_420x280_edgetpu.tflite ./

mkdir "$models_dir"/cctv3
cd "$models_dir"/cctv3 || exit
cp "$home_dir"/labels/person-map.txt ./labels.txt
cp "$home_dir"/models/cctv3.3/gray_cctv3_340x340.tflite ./
cp "$home_dir"/models/cctv3.3/gray_cctv3_340x340_edgetpu.tflite ./
