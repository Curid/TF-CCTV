# TF-CCTV

Custom [Coral](https://coral.ai) compatible [Tensorflow](https://www.tensorflow.org) models designed to process CCTV footage.

[Matrix chat](https://matrix.to/#/#tf-cctv:matrix.org)

<br>

## Accuracy

### objects365-val-cctv-v1_person


#### EDGETPU

![objects365-val-cctv-v1_person_edgetpu_color](./docs/plot/objects365-val-cctv-v1_person_edgetpu_color.svg)
![objects365-val-cctv-v1_person_edgetpu_gray](./docs/plot/objects365-val-cctv-v1_person_edgetpu_gray.svg)

<details>
  <summary>raw data</summary>

| MODEL                   |       | mAP  |  L   |  M   |   S  | | RPI5 | |   |
|-------------------------|-------|------|------|------|------|-|------|-|---|
| mobilenetV1             | color | 33.5 | 50.5 | 12.4 |  0.4 | |  6.9 | | a |
| mobilenetV1             | gray  | 29.6 | 45.8 |  9.1 |  0.2 | |  6.9 | |   |
| -                       |       |      |      |      |      | |      | |   |
| mobilenetV2             | color | 38.2 | 57.9 | 13.8 |  0.9 | |  7.6 | | b |
| mobilenetV2             | gray  | 32.7 | 51.4 |  9.4 |  0.7 | |  7.6 | |   |
| -                       |       |      |      |      |      | |      | |   |
| mobiledet_official      | color | 46.4 | 64.0 | 24.7 |  4.5 | | 10.5 | | c |
| mobiledet_official      | gray  | 41.4 | 59.3 | 18.3 |  3.0 | | 10.5 | |   |
| -                       |       |      |      |      |      | |      | |   |
| mobiledet               | color | 45.3 | 62.7 | 23.6 |  4.4 | |  8.1 | | d |
| mobiledet               | gray  | 40.5 | 58.2 | 17.4 |  2.9 | |  8.1 | | a |
| -                       |       |      |      |      |      | |      | |   |
| spaghettinet_m          | color | 45.5 | 62.5 | 25.0 |  3.3 | |  8.4 | | e |
| spaghettinet_m          | gray  | 40.6 | 57.5 | 19.7 |  2.8 | |  8.4 | |   |
| spaghettinet_l          | color | 46.9 | 63.8 | 26.5 |  5.2 | |  8.9 | | f |
| spaghettinet_l          | gray  | 41.7 | 58.0 | 21.7 |  2.8 | |  8.9 | | b |
| spaghettinet_l_official | color | 45.9 | 63.4 | 24.4 |  3.8 | |  8.5 | | g |
| spaghettinet_l_official | gray  | 41.1 | 58.2 | 19.9 |  2.0 | |  8.5 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolov5nu_320x320        | color | 44.6 | 62.4 | 22.8 |  0.7 | | 13.4 | | h |
| yolov5nu_320x320        | gray  | 33.9 | 50.1 | 15.2 |  1.3 | | 13.4 | | c |
| yolov5nu_480x480        | color | 51.9 | 68.3 | 31.3 |  7.3 | | 28.8 | | i |
| yolov5nu_480x480        | gray  | 38.8 | 54.5 | 21.5 |  3.3 | | 28.8 | | d |
| yolov5nu_576x576        | color | 54.2 | 69.6 | 36.6 | 11.0 | | 45.1 | | j |
| yolov5nu_576x576        | gray  | 42.7 | 58.0 | 26.7 |  4.6 | | 45.1 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolov5n6u_320x320       | color | 42.0 | 59.7 | 20.2 |  1.0 | | 14.2 | | k |
| yolov5n6u_320x320       | gray  | 31.4 | 46.7 | 14.2 |  0.7 | | 14.2 | | e |
| -                       |       |      |      |      |      | |      | |   |
| yolov5su_320x320        | color | 50.6 | 67.0 | 30.9 |  5.5 | | 20.5 | | h |
| yolov5su_320x320        | gray  | 39.4 | 55.6 | 21.7 |  2.6 | | 20.5 | | c |
| yolov5su_480x480        | color | 58.4 | 73.0 | 41.9 | 14.7 | | 57.0 | |   |
| yolov5su_480x480        | gray  | 47.1 | 62.5 | 31.0 |  8.2 | | 57.0 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolov5s6u_320x320       | color | 47.8 | 65.8 | 25.8 |  3.0 | | 36.7 | | k |
| yolov5s6u_320x320       | gray  | 36.8 | 53.6 | 17.1 |  1.4 | | 36.7 | | e |
| -                       |       |      |      |      |      | |      | |   |
| yolov5mu_320x320        | color | 59.5 | 75.3 | 41.1 | 10.4 | | 71.2 | |   |
| yolov5mu_320x320        | gray  | 51.4 | 69.1 | 29.9 |  4.9 | | 71.2 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolov8n_320x320         | color | 43.8 | 60.5 | 23.4 |  2.1 | | 13.3 | | l |
| yolov8n_320x320         | gray  | 29.4 | 43.9 | 13.4 |  1.1 | | 13.3 | | f |
| yolov8n_480x480         | color | 50.3 | 65.6 | 32.3 |  8.5 | | 29.0 | | m |
| yolov8n_480x480         | gray  | 36.4 | 51.3 | 19.6 |  4.3 | | 29.0 | | g |
| yolov8n_576x576         | color | 52.0 | 67.2 | 34.7 |  8.9 | | 45.0 | | n |
| yolov8n_576x576         | gray  | 37.1 | 51.9 | 20.1 |  6.4 | | 45.0 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolov8s_320x320         | color | 53.6 | 70.7 | 33.2 |  5.6 | | 25.5 | | l |
| yolov8s_320x320         | gray  | 43.7 | 60.6 | 24.1 |  2.9 | | 25.5 | | f |
| yolov8s_480x480         | color | 61.3 | 75.4 | 45.3 | 13.2 | | 60.1 | |   |
| yolov8s_480x480         | gray  | 52.4 | 67.9 | 34.9 |  7.5 | | 60.1 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolov8m_320x320         | color | 60.6 | 76.5 | 41.8 |  8.8 | | 75.3 | |   |
| yolov8m_320x320         | gray  | 52.8 | 69.8 | 32.3 |  5.1 | | 75.3 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolov9t_320x320         | color | 48.7 | 67.5 | 25.4 |  2.7 | | 16.6 | | o |
| yolov9t_320x320         | gray  | 37.6 | 55.0 | 17.8 |  1.6 | | 16.6 | | h |
| yolov9t_480x480         | color | 56.0 | 72.1 | 37.7 |  9.2 | | 37.8 | | p |
| yolov9t_480x480         | gray  | 42.4 | 59.0 | 24.2 |  4.6 | | 37.8 | | i |
| yolov9t_576x576         | color | 57.8 | 72.4 | 41.1 | 12.1 | | 57.6 | |   |
| yolov9t_576x576         | gray  | 43.6 | 59.3 | 28.0 |  7.0 | | 57.6 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolov9s_320x320         | color | 55.5 | 73.1 | 34.6 |  6.2 | | 25.5 | | o |
| yolov9s_320x320         | gray  | 45.4 | 63.6 | 23.9 |  3.7 | | 25.5 | | h |
| yolov9s_480x480         | color | 62.4 | 76.4 | 46.3 | 18.1 | | 71.9 | |   |
| yolov9s_480x480         | gray  | 53.7 | 69.9 | 34.6 |  9.2 | | 71.9 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolov9m_320x320         | color | 60.2 | 76.4 | 41.4 | 10.2 | | 86.2 | |   |
| yolov9m_320x320         | gray  | 49.8 | 66.3 | 30.9 |  5.0 | | 86.2 | |   |
| -                       |       |      |      |      |      | |      | |   |
| yolo11n_320x320         | color | 43.2 | 60.3 | 22.8 |  3.8 | | 27.0 | | r |
| yolo11n_320x320         | gray  | 31.7 | 46.8 | 14.3 |  0.6 | | 27.0 | |   |
| -                       |       |      |      |      |      | |      | |   |
| cctv1(mobiledet)        | color | 46.6 | 64.9 | 24.6 |  1.8 | |  6.6 | | s |
| cctv1(mobiledet)        | gray  | 44.1 | 62.9 | 21.7 |  1.2 | |  6.6 | | j |
| -                       |       |      |      |      |      | |      | |   |
| cctv3.3(mobiledet)      | color | 45.6 | 63.5 | 22.5 |  1.8 | |  6.5 | | t |
| cctv3.3(mobiledet)      | gray  | 45.5 | 63.6 | 22.3 |  2.0 | |  6.5 | | k |
| -                       |       |      |      |      |      | |      | |   |
| cctv4(mobiledet)        | color | 45.5 | 63.3 | 23.5 |  2.2 | |  9.3 | | u |
| cctv4(mobiledet)        | gray  | 45.7 | 63.3 | 23.6 |  2.4 | |  9.3 | | l |


##### RPI5 4GB with USB accelerator

</details>


<br>
<br>

#### TFLITE

![objects365-val-cctv-v1_person_tflite_color](./docs/plot/objects365-val-cctv-v1_person_tflite_color.svg)

<details>
  <summary>raw data</summary>

| MODEL              |       | mAP  |  L   |  M   |   S  | | RPI5 | |   |
|--------------------|-------|------|------|------|------|-|------|-|---|
| mobiledet          | color | 45.2 | 62.4 | 23.9 |  4.7 | |   48 | | a |
| mobiledet          | gray  | 40.2 | 57.7 | 17.5 |  3.0 | |      | |   |
| -                  |       |      |      |      |      | |      | |   |
| spaghettinet_l     | color | 46.9 | 63.8 | 26.4 |  5.2 | |   46 | | b |
| spaghettinet_l     | gray  | 41.7 | 58.0 | 21.7 |  2.8 | |      | |   |
| -                  |       |      |      |      |      | |      | |   |
| yolov5nu_320x320   | color | 44.5 | 61.9 | 23.3 |  2.0 | |   36 | | c |
| yolov5nu_480x480   | color | 52.0 | 68.4 | 32.0 |  7.0 | |   94 | | d |
| -                  |       |      |      |      |      | |      | |   |
| yolov5n6u_320x320  | color | 41.9 | 59.8 | 19.9 |  0.9 | |   37 | | e |
| -                  |       |      |      |      |      | |      | |   |
| yolov5su_320x320   | color | 50.5 | 67.2 | 30.6 |  5.5 | |   89 | | c |
| -                  |       |      |      |      |      | |      | |   |
| yolov5s6u_320x320  | color | 48.1 | 66.1 | 26.2 |  2.7 | |   93 | | e |
| -                  |       |      |      |      |      | |      | |   |
| yolov5mu_320x320   | color | 59.1 | 75.2 | 40.0 |  9.8 | |  210 | | c |
| -                  |       |      |      |      |      | |      | |   |
| yolov8n_320x320    | color | 43.9 | 60.6 | 23.5 |  1.9 | |   36 | | f |
| yolov8n_480x480    | color | 50.2 | 65.9 | 31.5 |  8.2 | |   93 | | g |
| -                  |       |      |      |      |      | |      | |   |
| yolov8s_320x320    | color | 53.4 | 70.2 | 33.2 |  5.2 | |   97 | | f |
| -                  |       |      |      |      |      | |      | |   |
| yolov8m_320x320    | color | 60.8 | 76.8 | 41.9 |  9.2 | |  236 | | f |
| -                  |       |      |      |      |      | |      | |   |
| yolov9t_320x320    | color | 48.6 | 67.6 | 25.0 |  2.3 | |   42 | | h |
| yolov9t_480x480    | color | 55.9 | 72.5 | 36.7 |  8.1 | |  109 | | i |
| -                  |       |      |      |      |      | |      | |   |
| yolov9s_320x320    | color | 55.4 | 72.7 | 34.4 |  6.1 | |  106 | | h |
| -                  |       |      |      |      |      | |      | |   |
| cctv1(mobiledet)   | color | 46.7 | 64.9 | 24.6 |  1.9 | |   56 | | j |
| cctv1(mobiledet)   | gray  | 44.1 | 62.8 | 21.7 |  1.3 | |      | |   |
| -                  |       |      |      |      |      | |      | |   |
| cctv3.3(mobiledet) | color | 45.5 | 63.4 | 22.3 |  1.8 | |   56 | | k |
| cctv3.3(mobiledet) | gray  | 45.6 | 63.6 | 22.4 |  2.0 | |      | |   |
| -                  |       |      |      |      |      | |      | |   |
| cctv4(mobiledet)   | color | 45.5 | 63.6 | 23.5 |  2.2 | |  119 | | l |
| cctv4(mobiledet)   | gray  | 45.6 | 63.2 | 23.5 |  2.4 | |      | |   |


</details>

<br>


## Models

The models are based on MobileDet with additional training data added.


### CCTV1

Proof of concept model. Can only detect `person`. Resolution changed from `320x320` to `420x280`. Achieves an improvement of 1.7mAP on gray images. Though at the cost of approximately 18% slower inference time.

<details>
<summary>CCTV2 failed run</summary>
Frigate compatible model. Can only detect `person`. Resolution changed from `320x320` to `340x340`. Approximately 18% slower inference time. Failed run, accuracy is worse than original model.
</details>

### CCTV3

Frigate compatible model. Only trained on gray images. Can only detect `person`. Resolution changed from `320x320` to `340x340`. Approximately 18% slower inference speed compared to mobiledet.

### CCTV4

Frigate compatible model. Only trained on gray images. Can only detect `person`. Resolution changed from `320x320` to `340x340`. Higher accuracy compared to mobiledet, but much slower inference speed. First model trained without transfer learning.

<br>

##### mAP is the average accuracy. L, M, S is the accuracy for Large, medium, and small boxes.

<br>

### BASE MODELS

| SIZE    | NAME           | FULL NAME                         |
| ------- | -------------- | --------------------------------- |
| 320x320 | mobiledet      | ssdlite\_mobiledet\_edgetpu/fp32  |

<br>

### EVAL SETS

#### COCO Minival
Blacklisted images are removed from all testsets.

| IMAGES | NAME                        | DESCRIPTION                                         |
| ------ | ----------------------------|-----------------------------------------------------|
| 720    | coco-minival_person         | Minival color and gray, person only.                |
| 349    | color\_coco-minival_person  | Minival original, cctv1 labels. Gray images removed.|
| 371    | gray\_coco-minival_person   | Minival coverted to black and white, person only.   |

<br>

#### objects365-val-cctv-v1

All images are fairly easy with minimum ambiguity. Count represent the number of images with one or more of the object class.

| CLASS      | COUNT |
| ---------- | ----- |
| total      | 1269  |
| person     | 684   |
| car        | 186   |
| elephant   | 118   |
| giraffe    | 105   |
| bird       | 101   |
| dog        | 101   |
| bicycle    | 99    |
| cat        | 93    |
| zebra      | 80    |
| horse      | 77    |
| motorcycle | 77    |
| train      | 70    |
| boat       | 65    |
| cow        | 21    |
| airplane   | 19    |
| sheep      | 16    |

<br>

### TRAINSETS

| SIZE | NAME    | DESCRIPTION                                                             |
| ---- | ------- | ------------------------------------------------------------------------|
| 144k | cctv1   | person only. Combination of coco2017 and voc2012. Blacklist not applied |
| 144k | cctv2   | Same as cctv1.                                                          |
| 59k  | cctv3   | person only. coco2017 and voc2012 images coverted to gray.              |
| 54k  | cctv3.1 | The first 100k person IDs sorted.                                       |
| 50k  | cctv3.2 | The first 200k person IDs sorted.                                       |
| 44k  | cctv3.3 | The first 300k person IDs sorted.                                       |
| 21k  | cctv4   | All COCO person images sorted.


<br>

### Naming convention.

\<TYPE>\_\<DATASET>\_\<LABELS>

| type  | description                            |
|-------|----------------------------------------|
| color | Original images.                       |
| gray  | Images converted to black and white.   |
| empty | Combined color and gray.               |

<br>

### DATASETS

#### COCO2017

https://cocodataset.org

#### VOC2012

http://host.robots.ox.ac.uk/pascal/VOC/voc2012/

<br>

### Resources

[https://coral.ai/docs/edgetpu/retrain-detection](https://coral.ai/docs/edgetpu/retrain-detection)

[Custom object detection from scratch](https://towardsdatascience.com/custom-object-detection-using-tensorflow-from-scratch-e61da2e10087)

[Effect of batch size on training dynamics](https://medium.com/mini-distill/effect-of-batch-size-on-training-dynamics-21c14f7a716e)

[MobileNetV2: Inverted Residuals and Linear Bottlenecks](https://arxiv.org/pdf/1801.04381.pdf)

[https://elitedatascience.com/category/explainers](https://elitedatascience.com/category/explainers)

https://stackoverflow.com/questions/64980436/early-stopping-in-tensorflow-object-detection-api/67261510#67261510

https://github.com/minhnhat93/tf_object_detection_multi_channels

https://stackoverflow.com/questions/60537788/how-to-modify-ssd-mobilenet-config-to-detect-small-objects-using-tensorflow-obje

https://stackoverflow.com/questions/52047638/tuning-first-stage-anchor-generator-in-faster-rcnn-model

https://developers.arcgis.com/python/guide/how-ssd-works

https://towardsdatascience.com/understanding-ssd-multibox-real-time-object-detection-in-deep-learning-495ef744fab


<br>


Copyright (C) 2021-2025 Curid, <Curid@protonmail.com>
