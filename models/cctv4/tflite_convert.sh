#!/bin/sh

script_dir=$(dirname "$(readlink -f "$0")")

tflite_convert \
	--output_file="$script_dir/gray_cctv4_340x340.tflite" \
	--graph_def_file="$script_dir/tflite_graph.pb" \
	--inference_type=QUANTIZED_UINT8 \
	--input_arrays="normalized_input_image_tensor" \
	--output_arrays="TFLite_Detection_PostProcess,TFLite_Detection_PostProcess:1,TFLite_Detection_PostProcess:2,TFLite_Detection_PostProcess:3" \
	--mean_values=128 \
	--std_dev_values=128 \
	--input_shapes=1,340,340,3 \
	--allow_custom_ops
