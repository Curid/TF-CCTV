import os
from pathlib import Path

import functools
import tensorflow.compat.v1 as tf
import tf_slim as slim

from object_detection.builders import graph_rewriter_builder
from object_detection.models import feature_map_generators
from object_detection.utils import (
    ops,
    shape_utils,
)

from od import (
    ssd_meta_arch,
    exporter,
    model_builder,
    post_processing_builder,
)
from od.config import mobiledet_model, coco_eval_config, default_graph_rewriter
from od.export_tflite_ssd_graph_lib import (
    get_const_center_size_encoded_anchors,
    append_postprocessing_op,
)

# Future models should instead use: pretrained_models/mobiledet/export_tflite.py


def main():
    script_dir = Path(os.path.dirname(os.path.realpath(__file__)))
    os.chdir(script_dir)

    num_classes = 1
    trained_checkpoint_prefix = "./model.ckpt-157129"
    add_postprocessing_op = True
    output_dir = "."

    model = mobiledet_model(
        num_classes=1,
        width=340,
        height=340,
    )
    eval_config = coco_eval_config(num_examples=371)

    if model.WhichOneof("model") != "ssd":
        raise ValueError(
            "Only ssd models are supported in tflite. " "Found {} in config".format(
                model.WhichOneof("model")
            )
        )

    nms_score_threshold = {
        model.ssd.post_processing.batch_non_max_suppression.score_threshold
    }
    nms_iou_threshold = {
        model.ssd.post_processing.batch_non_max_suppression.iou_threshold
    }
    scale_values = {}
    scale_values["y_scale"] = {model.ssd.box_coder.faster_rcnn_box_coder.y_scale}
    scale_values["x_scale"] = {model.ssd.box_coder.faster_rcnn_box_coder.x_scale}
    scale_values["h_scale"] = {model.ssd.box_coder.faster_rcnn_box_coder.height_scale}
    scale_values["w_scale"] = {model.ssd.box_coder.faster_rcnn_box_coder.width_scale}

    _DEFAULT_NUM_CHANNELS = 3

    image_resizer_config = model.ssd.image_resizer
    image_resizer = image_resizer_config.WhichOneof("image_resizer_oneof")
    num_channels = _DEFAULT_NUM_CHANNELS
    if image_resizer == "fixed_shape_resizer":
        height = image_resizer_config.fixed_shape_resizer.height
        width = image_resizer_config.fixed_shape_resizer.width
        if image_resizer_config.fixed_shape_resizer.convert_to_grayscale:
            num_channels = 1
        shape = [1, height, width, num_channels]
    else:
        raise ValueError(
            "Only fixed_shape_resizer" "is supported with tflite. Found {}".format(
                image_resizer_config.WhichOneof("image_resizer_oneof")
            )
        )

    image = tf.placeholder(
        tf.float32, shape=shape, name="normalized_input_image_tensor"
    )

    detection_model = model_builder.build(
        model,
        is_training=False,
        feature_extractor_class=SSDMobileDetEdgeTPUFeatureExtractor,
    )
    predicted_tensors = detection_model.predict(image, true_image_shapes=None)
    # The score conversion occurs before the post-processing custom op
    _, score_conversion_fn = post_processing_builder.build(model.ssd.post_processing)
    class_predictions = score_conversion_fn(
        predicted_tensors["class_predictions_with_background"]
    )

    with tf.name_scope("raw_outputs"):
        # 'raw_outputs/box_encodings': a float32 tensor of shape [1, num_anchors, 4]
        #  containing the encoded box predictions. Note that these are raw
        #  predictions and no Non-Max suppression is applied on them and
        #  no decode center size boxes is applied to them.
        tf.identity(predicted_tensors["box_encodings"], name="box_encodings")
        # 'raw_outputs/class_predictions': a float32 tensor of shape
        #  [1, num_anchors, num_classes] containing the class scores for each anchor
        #  after applying score conversion.
        tf.identity(class_predictions, name="class_predictions")
    # 'anchors': a float32 tensor of shape
    #   [4, num_anchors] containing the anchors as a constant node.
    tf.identity(
        get_const_center_size_encoded_anchors(predicted_tensors["anchors"]),
        name="anchors",
    )

    # Add global step to the graph, so we know the training step number when we
    # evaluate the model.
    tf.train.get_or_create_global_step()

    # graph rewriter

    graph_rewriter_fn = graph_rewriter_builder.build(
        default_graph_rewriter(), is_training=False
    )
    graph_rewriter_fn()

    if model.ssd.feature_extractor.HasField("fpn"):
        exporter.rewrite_nn_resize_op(is_quantized)

    # freeze the graph
    saver_kwargs = {}
    if eval_config.use_moving_averages:
        saver_kwargs["write_version"] = saver_pb2.SaverDef.V1
        moving_average_checkpoint = tempfile.NamedTemporaryFile()
        exporter.replace_variable_values_with_moving_averages(
            tf.get_default_graph(),
            trained_checkpoint_prefix,
            moving_average_checkpoint.name,
        )
        checkpoint_to_use = moving_average_checkpoint.name
    else:
        checkpoint_to_use = trained_checkpoint_prefix

    saver = tf.train.Saver(**saver_kwargs)
    input_saver_def = saver.as_saver_def()
    frozen_graph_def = exporter.freeze_graph_with_def_protos(
        input_graph_def=tf.get_default_graph().as_graph_def(),
        input_saver_def=input_saver_def,
        input_checkpoint=checkpoint_to_use,
        output_node_names=",".join(
            ["raw_outputs/box_encodings", "raw_outputs/class_predictions", "anchors"]
            + list(())
        ),
        restore_op_name="save/restore_all",
        filename_tensor_name="save/Const:0",
        clear_devices=True,
        output_graph="",
        initializer_nodes="",
    )

    # Add new operation to do post processing in a custom op (TF Lite only)
    if add_postprocessing_op:
        transformed_graph_def = append_postprocessing_op(
            frozen_graph_def,
            nms_score_threshold,
            nms_iou_threshold,
            num_classes,
            scale_values,
        )
    else:
        # Return frozen without adding post-processing custom op
        transformed_graph_def = frozen_graph_def

    binary_graph_name = "tflite_graph.pb"
    binary_graph = os.path.join(output_dir, binary_graph_name)
    with tf.gfile.GFile(binary_graph, "wb") as f:
        f.write(transformed_graph_def.SerializeToString())

    txt_graph_name = "tflite_graph.pbtxt"
    txt_graph = os.path.join(output_dir, txt_graph_name)
    with tf.gfile.GFile(txt_graph, "w") as f:
        f.write(str(transformed_graph_def))


# Copyright 2020 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

BACKBONE_WEIGHT_DECAY = 4e-5


def _scale_filters(filters, multiplier, base=8):
    """Scale the filters accordingly to (multiplier, base)."""
    round_half_up = int(int(filters) * multiplier / base + 0.5)
    result = int(round_half_up * base)
    return max(result, base)


def _conv(
    h,
    filters,
    kernel_size,
    strides=1,
    normalizer_fn=slim.batch_norm,
    activation_fn=tf.nn.relu6,
):
    if activation_fn is None:
        raise ValueError(
            "Activation function cannot be None. Use tf.identity instead to better support quantized training."
        )
    return slim.conv2d(
        h,
        filters,
        kernel_size,
        stride=strides,
        activation_fn=activation_fn,
        normalizer_fn=normalizer_fn,
        weights_initializer=tf.initializers.he_normal(),
        weights_regularizer=slim.l2_regularizer(BACKBONE_WEIGHT_DECAY),
        padding="SAME",
    )


def _separable_conv(h, filters, kernel_size, strides=1, activation_fn=tf.nn.relu6):
    """Separable convolution layer."""
    if activation_fn is None:
        raise ValueError(
            "Activation function cannot be None. Use tf.identity instead to better support quantized training."
        )
    # Depthwise variant of He initialization derived under the principle proposed
    # in the original paper. Note the original He normalization was designed for
    # full convolutions and calling tf.initializers.he_normal() can over-estimate
    # the fan-in of a depthwise kernel by orders of magnitude.
    stddev = (2.0 / kernel_size**2) ** 0.5 / 0.87962566103423978
    depthwise_initializer = tf.initializers.truncated_normal(stddev=stddev)
    return slim.separable_conv2d(
        h,
        filters,
        kernel_size,
        stride=strides,
        activation_fn=activation_fn,
        normalizer_fn=slim.batch_norm,
        weights_initializer=depthwise_initializer,
        pointwise_initializer=tf.initializers.he_normal(),
        weights_regularizer=slim.l2_regularizer(BACKBONE_WEIGHT_DECAY),
        padding="SAME",
    )


def _squeeze_and_excite(h, hidden_dim, activation_fn=tf.nn.relu6):
    with tf.variable_scope(None, default_name="SqueezeExcite"):
        height, width = h.shape[1], h.shape[2]
        u = slim.avg_pool2d(h, [height, width], stride=1, padding="VALID")
        u = _conv(u, hidden_dim, 1, normalizer_fn=None, activation_fn=activation_fn)
        u = _conv(u, h.shape[-1], 1, normalizer_fn=None, activation_fn=tf.nn.sigmoid)
        return u * h


def _inverted_bottleneck(
    h,
    filters,
    activation_fn=tf.nn.relu6,
    kernel_size=3,
    expansion=8,
    strides=1,
    use_se=False,
    residual=True,
):
    """Inverted bottleneck layer."""
    with tf.variable_scope(None, default_name="IBN"):
        shortcut = h
        expanded_filters = int(h.shape[-1]) * expansion
        if expansion <= 1:
            raise ValueError("Expansion factor must be greater than 1.")
        h = _conv(h, expanded_filters, 1, activation_fn=activation_fn)
        # Setting filters to None will make _separable_conv a depthwise conv.
        h = _separable_conv(
            h, None, kernel_size, strides=strides, activation_fn=activation_fn
        )
        if use_se:
            hidden_dim = _scale_filters(expanded_filters, 0.25)
            h = _squeeze_and_excite(h, hidden_dim, activation_fn=activation_fn)
        h = _conv(h, filters, 1, activation_fn=tf.identity)
        if residual:
            h = h + shortcut
        return h


def _fused_conv(
    h,
    filters,
    activation_fn=tf.nn.relu6,
    kernel_size=3,
    expansion=8,
    strides=1,
    use_se=False,
    residual=True,
):
    """Fused convolution layer."""
    with tf.variable_scope(None, default_name="FusedConv"):
        shortcut = h
        expanded_filters = int(h.shape[-1]) * expansion
        if expansion <= 1:
            raise ValueError("Expansion factor must be greater than 1.")
        h = _conv(
            h,
            expanded_filters,
            kernel_size,
            strides=strides,
            activation_fn=activation_fn,
        )
        if use_se:
            hidden_dim = _scale_filters(expanded_filters, 0.25)
            h = _squeeze_and_excite(h, hidden_dim, activation_fn=activation_fn)
        h = _conv(h, filters, 1, activation_fn=tf.identity)
        if residual:
            h = h + shortcut
        return h


def _tucker_conv(
    h,
    filters,
    activation_fn=tf.nn.relu6,
    kernel_size=3,
    input_rank_ratio=0.25,
    output_rank_ratio=0.25,
    strides=1,
    residual=True,
):
    """Tucker convolution layer (generalized bottleneck)."""
    with tf.variable_scope(None, default_name="TuckerConv"):
        shortcut = h
        input_rank = _scale_filters(h.shape[-1], input_rank_ratio)
        h = _conv(h, input_rank, 1, activation_fn=activation_fn)
        output_rank = _scale_filters(filters, output_rank_ratio)
        h = _conv(
            h, output_rank, kernel_size, strides=strides, activation_fn=activation_fn
        )
        h = _conv(h, filters, 1, activation_fn=tf.identity)
        if residual:
            h = h + shortcut
        return h


def mobiledet_edgetpu_backbone(h, multiplier=1.0):
    """Build a MobileDet EdgeTPU backbone."""

    def _scale(filters):
        return _scale_filters(filters, multiplier)

    ibn = functools.partial(_inverted_bottleneck, activation_fn=tf.nn.relu6)
    fused = functools.partial(_fused_conv, activation_fn=tf.nn.relu6)
    tucker = functools.partial(_tucker_conv, activation_fn=tf.nn.relu6)

    endpoints = {}
    h = _conv(h, _scale(32), 3, strides=2, activation_fn=tf.nn.relu6)
    h = tucker(
        h, _scale(24), input_rank_ratio=0.25, output_rank_ratio=0.75, residual=False
    )
    endpoints["C1"] = h
    h = fused(h, _scale(32), expansion=8, kernel_size=3, strides=2, residual=False)
    h = fused(h, _scale(32), expansion=4, kernel_size=3)
    h = fused(h, _scale(32), expansion=8, kernel_size=3)
    h = fused(h, _scale(32), expansion=4, kernel_size=3)
    endpoints["C2"] = h
    h = fused(h, _scale(64), expansion=8, kernel_size=5, strides=2, residual=False)
    h = fused(h, _scale(64), expansion=4, kernel_size=3)
    h = fused(h, _scale(64), expansion=4, kernel_size=3)
    h = fused(h, _scale(64), expansion=4, kernel_size=3)
    endpoints["C3"] = h
    h = ibn(h, _scale(120), expansion=8, kernel_size=3, strides=2, residual=False)
    h = ibn(h, _scale(120), expansion=8, kernel_size=3)
    h = fused(h, _scale(120), expansion=4, kernel_size=3)
    h = fused(h, _scale(120), expansion=4, kernel_size=3)

    h = ibn(h, _scale(144), expansion=8, kernel_size=5, residual=False)
    h = ibn(h, _scale(144), expansion=8, kernel_size=5)
    h = ibn(h, _scale(144), expansion=8, kernel_size=3)
    h = ibn(h, _scale(144), expansion=8, kernel_size=3)
    endpoints["C4"] = h
    h = ibn(h, _scale(160), expansion=8, kernel_size=5, strides=2, residual=False)
    h = ibn(h, _scale(160), expansion=8, kernel_size=3)
    h = ibn(h, _scale(160), expansion=4, kernel_size=5)
    h = ibn(h, _scale(160), expansion=8, kernel_size=3)

    h = ibn(h, _scale(384), expansion=8, kernel_size=5, residual=False)
    endpoints["C5"] = h
    return endpoints


class SSDMobileDetFeatureExtractorBase(ssd_meta_arch.SSDFeatureExtractor):
    """Base class of SSD feature extractor using MobileDet features."""

    def __init__(
        self,
        backbone_fn,
        is_training,
        depth_multiplier,
        min_depth,
        pad_to_multiple,
        conv_hyperparams_fn,
        reuse_weights=None,
        use_explicit_padding=False,
        use_depthwise=False,
        override_base_feature_extractor_hyperparams=False,
        scope_name="MobileDet",
    ):
        """MobileDet Feature Extractor for SSD Models. https://arxiv.org/abs/2004.14525"""

        if use_explicit_padding:
            raise NotImplementedError(
                "Explicit padding is not yet supported in MobileDet backbones."
            )

        super(SSDMobileDetFeatureExtractorBase, self).__init__(
            is_training=is_training,
            depth_multiplier=depth_multiplier,
            min_depth=min_depth,
            pad_to_multiple=pad_to_multiple,
            conv_hyperparams_fn=conv_hyperparams_fn,
            reuse_weights=reuse_weights,
            use_explicit_padding=use_explicit_padding,
            use_depthwise=use_depthwise,
            override_base_feature_extractor_hyperparams=override_base_feature_extractor_hyperparams,
        )
        self._backbone_fn = backbone_fn
        self._scope_name = scope_name

    def preprocess(self, resized_inputs):
        return (2.0 / 255.0) * resized_inputs - 1.0

    def extract_features(self, preprocessed_inputs):
        preprocessed_inputs = shape_utils.check_min_image_dim(33, preprocessed_inputs)
        padded_inputs = ops.pad_to_multiple(preprocessed_inputs, self._pad_to_multiple)

        feature_map_layout = {
            "from_layer": ["C4", "C5", "", "", "", ""],
            # Do not specify the layer depths (number of filters) for C4 and C5, as
            # their values are determined based on the backbone.
            "layer_depth": [-1, -1, 512, 256, 256, 128],
            "use_depthwise": self._use_depthwise,
            "use_explicit_padding": self._use_explicit_padding,
        }

        with tf.variable_scope(self._scope_name, reuse=self._reuse_weights):
            with slim.arg_scope(
                [slim.batch_norm],
                is_training=self._is_training,
                epsilon=0.01,
                decay=0.99,
                center=True,
                scale=True,
            ):
                endpoints = self._backbone_fn(
                    padded_inputs, multiplier=self._depth_multiplier
                )

            image_features = {"C4": endpoints["C4"], "C5": endpoints["C5"]}
            with slim.arg_scope(self._conv_hyperparams_fn()):
                feature_maps = feature_map_generators.multi_resolution_feature_maps(
                    feature_map_layout=feature_map_layout,
                    depth_multiplier=self._depth_multiplier,
                    min_depth=self._min_depth,
                    insert_1x1_conv=True,
                    image_features=image_features,
                )

        return list(feature_maps.values())


class SSDMobileDetEdgeTPUFeatureExtractor(SSDMobileDetFeatureExtractorBase):
    """MobileDet-EdgeTPU feature extractor."""

    def __init__(
        self,
        is_training,
        depth_multiplier,
        min_depth,
        pad_to_multiple,
        conv_hyperparams_fn,
        reuse_weights=None,
        use_explicit_padding=False,
        use_depthwise=False,
        override_base_feature_extractor_hyperparams=False,
        scope_name="MobileDetEdgeTPU",
    ):
        super(SSDMobileDetEdgeTPUFeatureExtractor, self).__init__(
            backbone_fn=mobiledet_edgetpu_backbone,
            is_training=is_training,
            depth_multiplier=depth_multiplier,
            min_depth=min_depth,
            pad_to_multiple=pad_to_multiple,
            conv_hyperparams_fn=conv_hyperparams_fn,
            reuse_weights=reuse_weights,
            use_explicit_padding=use_explicit_padding,
            use_depthwise=use_depthwise,
            override_base_feature_extractor_hyperparams=override_base_feature_extractor_hyperparams,
            scope_name=scope_name,
        )


if __name__ == "__main__":
    main()
