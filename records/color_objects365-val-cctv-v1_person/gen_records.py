import os

from py_utils.create_records import create_records
from py_utils.dataset import gen_objects365
from py_utils.utils import check_sum


dir_path = os.path.dirname(os.path.realpath(__file__))
home_path = os.path.dirname(os.path.dirname(dir_path))
datasets_path = os.path.join(home_path, "datasets", "objects365")

labels_path = os.path.join(home_path, "labels", "person.pbtxt")

color_objects365 = gen_objects365(datasets_path, labels_path, 1, val=True)

create_records(color_objects365, dir_path, 1)

check_sum(color_objects365, dir_path)

print("images", len(color_objects365["images"]))
