import os

from py_utils.create_records import create_records
from py_utils.dataset import parser
from py_utils.dataset import combine_instances
from py_utils.utils import check_sum


dir_path = os.path.dirname(os.path.realpath(__file__))
home_path = os.path.dirname(os.path.dirname(dir_path))
datasets_path = os.path.join(home_path, "datasets")

labels_path = os.path.join(home_path, "labels", "person.pbtxt")

p = parser(datasets_path, labels_path)

color_coco = p.gen_coco(1, minival=True)
gray_coco = p.gen_coco(2, minival=True, gray=True)

print(len(color_coco), len(gray_coco))

instance = combine_instances(
    [
        color_coco,
        gray_coco,
    ]
)
create_records(instance, dir_path, 1)

check_sum(instance, dir_path)

print("images", len(instance["images"]))
