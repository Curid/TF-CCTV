0  person
1  sneakers
2  chair
3  other shoes
4  hat
5  car
6  lamp
7  glasses
8  bottle
9  desk
10  cup
11  street lights
12  cabinet/shelf
13  handbag/satchel
14  bracelet
15  plate
16  picture/frame
17  helmet
18  book
19  gloves
20  storage box
21  boat
22  leather shoes
23  flower
24  bench
25  potted plant
26  bowl/basin
27  flag
28  pillow
29  boots
30  vase
31  microphone
32  necklace
33  ring
34  suv
35  wine glass
36  belt
37  monitor/tv
38  backpack
39  umbrella
40  traffic light
41  speaker
42  watch
43  tie
44  trash bin can
45  slippers
46  bicycle
47  stool
48  barrel/bucket
49  van
50  couch
51  sandals
52  basket
53  drum
54  pen/pencil
55  bus
56  wild bird
57  high heels
58  motorcycle
59  guitar
60  carpet
61  cell phone
62  bread
63  camera
64  canned
65  truck
66  traffic cone
67  cymbal
68  lifesaver
69  towel
70  stuffed toy
71  candle
72  sailboat
73  laptop
74  awning
75  bed
76  faucet
77  tent
78  horse
79  mirror
80  power outlet
81  sink
82  apple
83  air conditioner
84  knife
85  hockey stick
86  paddle
87  pickup truck
88  fork
89  traffic sign
90  balloon
91  tripod
92  dog
93  spoon
94  clock
95  pot
96  cow
97  cake
98  dinning table
99  sheep
100  hanger
101  blackboard/whiteboard
102  napkin
103  other fish
104  orange/tangerine
105  toiletry
106  keyboard
107  tomato
108  lantern
109  machinery vehicle
110  fan
111  green vegetables
112  banana
113  baseball glove
114  airplane
115  mouse
116  train
117  pumpkin
118  soccer
119  skiboard
120  luggage
121  nightstand
122  tea pot
123  telephone
124  trolley
125  head phone
126  sports car
127  stop sign
128  dessert
129  scooter
130  stroller
131  crane
132  remote
133  refrigerator
134  oven
135  lemon
136  duck
137  baseball bat
138  surveillance camera
139  cat
140  jug
141  broccoli
142  piano
143  pizza
144  elephant
145  skateboard
146  surfboard
147  gun
148  skating and skiing shoes
149  gas stove
150  donut
151  bow tie
152  carrot
153  toilet
154  kite
155  strawberry
156  other balls
157  shovel
158  pepper
159  computer box
160  toilet paper
161  cleaning products
162  chopsticks
163  microwave
164  pigeon
165  baseball
166  cutting/chopping board
167  coffee table
168  side table
169  scissors
170  marker
171  pie
172  ladder
173  snowboard
174  cookies
175  radiator
176  fire hydrant
177  basketball
178  zebra
179  grape
180  giraffe
181  potato
182  sausage
183  tricycle
184  violin
185  egg
186  fire extinguisher
187  candy
188  fire truck
189  billiards
190  converter
191  bathtub
192  wheelchair
193  golf club
194  briefcase
195  cucumber
196  cigar/cigarette
197  paint brush
198  pear
199  heavy truck
200  hamburger
201  extractor
202  extension cord
203  tong
204  tennis racket
205  folder
206  american football
207  earphone
208  mask
209  kettle
210  tennis
211  ship
212  swing
213  coffee machine
214  slide
215  carriage
216  onion
217  green beans
218  projector
219  frisbee
220  washing machine/drying machine
221  chicken
222  printer
223  watermelon
224  saxophone
225  tissue
226  toothbrush
227  ice cream
228  hot-air balloon
229  cello
230  french fries
231  scale
232  trophy
233  cabbage
234  hot dog
235  blender
236  peach
237  rice
238  wallet/purse
239  volleyball
240  deer
241  goose
242  tape
243  tablet
244  cosmetics
245  trumpet
246  pineapple
247  golf ball
248  ambulance
249  parking meter
250  mango
251  key
252  hurdle
253  fishing rod
254  medal
255  flute
256  brush
257  penguin
258  megaphone
259  corn
260  lettuce
261  garlic
262  swan
263  helicopter
264  green onion
265  sandwich
266  nuts
267  speed limit sign
268  induction cooker
269  broom
270  trombone
271  plum
272  rickshaw
273  goldfish
274  kiwi fruit
275  router/modem
276  poker card
277  toaster
278  shrimp
279  sushi
280  cheese
281  notepaper
282  cherry
283  pliers
284  cd
285  pasta
286  hammer
287  cue
288  avocado
289  hamimelon
290  flask
291  mushroom
292  screwdriver
293  soap
294  recorder
295  bear
296  eggplant
297  board eraser
298  coconut
299  tape measure/ruler
300  pig
301  showerhead
302  globe
303  chips
304  steak
305  crosswalk sign
306  stapler
307  camel
308  formula 1
309  pomegranate
310  dishwasher
311  crab
312  hoverboard
313  meat ball
314  rice cooker
315  tuba
316  calculator
317  papaya
318  antelope
319  parrot
320  seal
321  butterfly
322  dumbbell
323  donkey
324  lion
325  urinal
326  dolphin
327  electric drill
328  hair dryer
329  egg tart
330  jellyfish
331  treadmill
332  lighter
333  grapefruit
334  game board
335  mop
336  radish
337  baozi
338  target
339  french
340  spring rolls
341  monkey
342  rabbit
343  pencil case
344  yak
345  red cabbage
346  binoculars
347  asparagus
348  barbell
349  scallop
350  noddles
351  comb
352  dumpling
353  oyster
354  table tennis paddle
355  cosmetics brush/eyeliner pencil
356  chainsaw
357  eraser
358  lobster
359  durian
360  okra
361  lipstick
362  cosmetics mirror
363  curling
364  table tennis
