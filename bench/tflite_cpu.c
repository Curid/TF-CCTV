#include "bench.h"
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

int main(int argc, char **argv) {
  char *model_path = NULL;
  size_t width = 0;
  size_t height = 0;
  char *image_path = NULL;

  int ret = 0;
  if ((ret = parseArgs(argc, argv, &model_path, &width, &height, &image_path) !=
             0)) {
    return ret;
  }

  // Load model.
  TfLiteModel *model =
      TfLiteModelCreateFromFileWithErrorReporter(model_path, reporter, NULL);
  if (model == NULL) {
    printf("failed to load model: %s", model_path);
    return 1;
  }

  // Create interpreter.
  TfLiteInterpreterOptions *options = TfLiteInterpreterOptionsCreate();
  TfLiteInterpreterOptionsSetNumThreads(options, 1);
  TfLiteInterpreterOptionsSetErrorReporter(options, reporter, NULL);

  TfLiteInterpreter *interpreter = TfLiteInterpreterCreate(model, options);
  if (interpreter == NULL) {
    printf("failed to create interpreter");
    TfLiteModelDelete(model);
    return 1;
  }
  TfLiteModelDelete(model);
  TfLiteInterpreterOptionsDelete(options);

  // Allocate tensors.
  if ((ret = TfLiteInterpreterAllocateTensors(interpreter)) != 0) {
    printf("failed to allocate tensors");
    goto cleanup;
  }

  TfLiteTensor *input_tensor = TfLiteInterpreterGetInputTensor(interpreter, 0);
  size_t input_tensor_size = TfLiteTensorByteSize(input_tensor);
  size_t buf_size = width * height * 3;

  if (input_tensor_size != buf_size) {
    printf("tensor size doesn't match buffer size: %ld %ld", input_tensor_size,
           buf_size);
    goto cleanup;
  }

  // Read image.
  uint8_t *buf = malloc(sizeof(uint8_t) * buf_size);

  FILE *file = fopen(image_path, "r");
  if (file == NULL) {
    printf("failed to open image");
    goto cleanup;
  }
  size_t n = fread(buf, sizeof(uint8_t), buf_size, file);
  if (n != buf_size) {
    printf("file size doesn't match buffer size: %ld %ld", n, buf_size);
    goto cleanup;
  }
  if ((ret = fclose(file) != 0)) {
    printf("failed to close file");
    goto cleanup;
  }

  int32_t output_tensor_count =
      TfLiteInterpreterGetOutputTensorCount(interpreter);

  // NOLO preprocessing.
  if (output_tensor_count == 1) {
    TfLiteQuantizationParams params =
        TfLiteTensorQuantizationParams(input_tensor);

    float *buf2 = malloc(sizeof(float) * buf_size);
    int i = 0;
    for (i = 0; i < buf_size; i++) {
      buf2[i] = (float)buf[i];
      buf2[i] = buf2[i] / 255;
      buf2[i] = buf2[i] / params.scale + params.zero_point;
      buf[i] = (uint8_t)buf2[i];
    }
  }

  // Evaluate one image.
  char *best = NULL;
  if ((ret = invokeAndGetBestScore(interpreter, input_tensor, buf, buf_size,
                                   width, height, &best) != 0)) {
    goto cleanup;
  }
  // best = "";

  int i = 0;
  int warmup = 500;
  for (i = 0; i <= warmup; i++) {
    // Execute inference.
    if ((ret = invoke(interpreter, input_tensor, buf, buf_size) != 0)) {
      goto cleanup;
    }
  }

  struct timeval start_time;
  gettimeofday(&start_time, NULL);
  unsigned long start_time_us =
      start_time.tv_sec * 1000000 + start_time.tv_usec;

  int num_samples = 500;
  for (i = 0; i <= num_samples; i++) {
    // Execute inference.
    if ((ret = invoke(interpreter, input_tensor, buf, buf_size) != 0)) {
      goto cleanup;
    }
  }

  struct timeval end_time;
  gettimeofday(&end_time, NULL);
  unsigned long end_time_us = end_time.tv_sec * 1000000 + end_time.tv_usec;

  double ms_per_sample =
      ((double)(end_time_us - start_time_us) / 1000) / num_samples;

  printf("%.1fms         %s", ms_per_sample, best);

cleanup:
  TfLiteInterpreterDelete(interpreter);

  return ret;
}
