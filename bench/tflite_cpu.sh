#!/bin/sh

set -e

script_dir=$(dirname "$(readlink -f "$0")")
cd "$script_dir"

# Compile the program.
gcc -O3 -I ./includes/ ./tflite_cpu.c -l tensorflowlite_c -o tflite_cpu

printf "Mean inference time\n"

bench() {
	name=$1
	width=$2
	height=$3
	path=$4

	if [ ! -e "$path" ]; then
		printf "\n path does not exist: %s" "$path"
		return
	fi

	printf "\n%s: " "$name"
	image_path=./images/"$width"x"$height".rgb24
	./tflite_cpu "$path" "$width" "$height" "$image_path" || true
}

bench "mobiledet"      "320" "320" "../pretrained_models/mobiledet/mobiledet_cpu.tflite"
bench "spaghettinet_l" "320" "320" "../pretrained_models/spaghettinet_l/spaghettinet_l_cpu.tflite"


bench "yolo5nu_320x320"  "320" "320" "../yolo/models_official/yolov5nu/yolov5nu_320x320.tflite"
bench "yolo5nu_480x480"  "480" "480" "../yolo/models_official/yolov5nu/yolov5nu_480x480.tflite"

bench "yolo5su_320x320"  "320" "320" "../yolo/models_official/yolov5su/yolov5su_320x320.tflite"
bench "yolo5mu_320x320"  "320" "320" "../yolo/models_official/yolov5mu/yolov5mu_320x320.tflite"

bench "yolo5n6u_320x320" "320" "320" "../yolo/models_official/yolov5n6u/yolov5n6u_320x320.tflite"
bench "yolo5s6u_320x320" "320" "320" "../yolo/models_official/yolov5s6u/yolov5s6u_320x320.tflite"


bench "yolo8n_320x320" "320" "320" "../yolo/models_official/yolov8n/yolov8n_320x320.tflite"
bench "yolo8n_480x480" "480" "480" "../yolo/models_official/yolov8n/yolov8n_480x480.tflite"

bench "yolo8s_320x320" "320" "320" "../yolo/models_official/yolov8s/yolov8s_320x320.tflite"
bench "yolo8m_320x320" "320" "320" "../yolo/models_official/yolov8m/yolov8m_320x320.tflite"


bench "yolo9t_320x320" "320" "320" "../yolo/models_official/yolov9t/yolov9t_320x320.tflite"
bench "yolo9t_480x480" "480" "480" "../yolo/models_official/yolov9t/yolov9t_480x480.tflite"

bench "yolo9s_320x320" "320" "320" "../yolo/models_official/yolov9s/yolov9s_320x320.tflite"


bench "cctv1"          "420" "280" "../models/cctv1/cctv1_420x280.tflite"
bench "cctv3"          "340" "340" "../models/cctv3.3/gray_cctv3_340x340.tflite"
bench "cctv4"          "340" "340" "../models/cctv4/gray_cctv4_340x340.tflite"
