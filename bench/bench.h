#include <stdio.h>
#include <string.h>
#include <tensorflowlite_c.h>

void reporter(void *user_data, const char *format, va_list args) {
  (void)user_data;
  const char *prefix = "TFLITE ERROR: ";
  char *f;
  f = malloc(strlen(prefix) + strlen(format) + 1);
  strcpy(f, prefix);
  strcat(f, format);
  fprintf(stderr, f, args);
  free(f);
}

int parseArgs(int argc, char **argv, char **model_path, size_t *width,
              size_t *height, char **image_path) {
  if (argc != 5) {
    printf("usage:\n    %s <MODEL_PATH> <WIDTH> <HEIGHT> <IMAGE_PATH>\n",
           argv[0]);
    return 1;
  }
  *model_path = argv[1];
  *width = atoi(argv[2]);
  *height = atoi(argv[3]);
  *image_path = argv[4];
  return 0;
}

void parseODAPI(TfLiteInterpreter *interpreter, char **best) {
  const TfLiteTensor *t0_tensor =
      TfLiteInterpreterGetOutputTensor(interpreter, 0);
  const TfLiteTensor *t1_tensor =
      TfLiteInterpreterGetOutputTensor(interpreter, 1);
  const TfLiteTensor *t2_tensor =
      TfLiteInterpreterGetOutputTensor(interpreter, 2);
  const TfLiteTensor *t3_tensor =
      TfLiteInterpreterGetOutputTensor(interpreter, 3);

  float *t0 = TfLiteTensorData(t0_tensor);
  float *t1 = TfLiteTensorData(t1_tensor);
  float *t2 = TfLiteTensorData(t2_tensor);
  float *t3 = TfLiteTensorData(t3_tensor);

  float count = t3[0];

  float best_score = 0;
  *best = malloc(3000 * sizeof(char));

  int i = 0;
  for (i = 0; i < count; i++) {
    float score = t2[i];
    float class = t1[i];

    float top = t0[4 * i];
    float left = t0[4 * i + 1];
    float bottom = t0[4 * i + 2];
    float right = t0[4 * i + 3];

    if (score > best_score) {
      best_score = score;
      sprintf(*best, "%.0f%% %.0f [%.2f, %.2f, %.2f, %.2f]", score * 100, class,
              top, left, bottom, right);
    }
  }
}

void parseNOLO(TfLiteInterpreter *interpreter, size_t width, size_t height,
               char **best) {
  const TfLiteTensor *tensor = TfLiteInterpreterGetOutputTensor(interpreter, 0);
  TfLiteQuantizationParams params = TfLiteTensorQuantizationParams(tensor);

  int8_t *data = TfLiteTensorData(tensor);
  size_t size = TfLiteTensorByteSize(tensor);
  int32_t num_classes4 = TfLiteTensorDim(tensor, 1);
  size_t n = size / num_classes4;

  float *data2 = malloc(sizeof(float) * size);
  for (int i = 0; i < size; i++) {
    data2[i] = (float)data[i];
    data2[i] = (data2[i] - params.zero_point) * params.scale;
  }

  *best = malloc(3000 * sizeof(char));
  float best_score = 0;
  for (int class4 = 4; class4 < num_classes4; class4++) {
    for (int j = 0; j < n; j++) {
      float score = data2[n * class4 + j];
      if (score > best_score) {
        float x = data2[j];
        float y = data2[n + j];
        float w2 = data2[n * 2 + j] / 2;
        float h2 = data2[n * 3 + j] / 2;
        float top = y - h2;
        float left = x - w2;
        float bottom = y + h2;
        float right = x + w2;
        best_score = score;
        sprintf(*best, "%.0f%% %d [%.2f, %.2f, %.2f, %.2f]", score * 100,
                class4 - 4, top, left, bottom, right);
      }
    }
  }
}

int invokeAndGetBestScore(TfLiteInterpreter *interpreter,
                          TfLiteTensor *input_tensor, uint8_t *image,
                          size_t image_size, size_t width, size_t height,
                          char **best) {
  int32_t input_tensor_count =
      TfLiteInterpreterGetInputTensorCount(interpreter);
  if (input_tensor_count != 1) {
    printf("expected input tensor count to be 1, got: %d", input_tensor_count);
    return 1;
  }

  int ret = 0;
  if ((ret = TfLiteTensorCopyFromBuffer(input_tensor, image, image_size)) !=
      0) {
    printf("failed to copy image to tensor");
    return ret;
  }

  if ((ret = TfLiteInterpreterInvoke(interpreter)) != 0) {
    printf("failed to invoke interpreter");
    return ret;
  }

  int32_t output_tensor_count =
      TfLiteInterpreterGetOutputTensorCount(interpreter);
  if (output_tensor_count == 4) {
    parseODAPI(interpreter, best);
  } else if (output_tensor_count == 1) {
    parseNOLO(interpreter, width, height, best);
  } else {
    printf("unexpected output tensor count: %d", output_tensor_count);
    return 1;
  }

  return 0;
}

int invoke(TfLiteInterpreter *interpreter, TfLiteTensor *input_tensor,
           uint8_t *image, size_t image_size) {
  int ret = 0;
  if ((ret = TfLiteTensorCopyFromBuffer(input_tensor, image, image_size)) !=
      0) {
    printf("failed to copy image to tensor");
    return ret;
  }

  if ((ret = TfLiteInterpreterInvoke(interpreter)) != 0) {
    printf("failed to invoke interpreter");
    return ret;
  }

  int32_t output_tensor_count =
      TfLiteInterpreterGetOutputTensorCount(interpreter);
  int i = 0;
  for (i = 0; i < output_tensor_count; i++) {
    const TfLiteTensor *tensor =
        TfLiteInterpreterGetOutputTensor(interpreter, i);
    uint8_t *data = TfLiteTensorData(tensor);
    // Prevent optimization.
    ((uint8_t volatile *)data)[0] = data[0];
  }

  return 0;
}
